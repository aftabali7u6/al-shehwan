-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2019 at 01:44 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `al-shahwan`
--

-- --------------------------------------------------------

--
-- Table structure for table `payables`
--

CREATE TABLE `payables` (
  `payable_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `received_from` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `delete_status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payables`
--

INSERT INTO `payables` (`payable_id`, `name`, `date`, `contact`, `address`, `amount`, `received_from`, `image`, `status`, `created_by`, `updated_by`, `delete_status`, `created_at`, `updated_at`) VALUES
(1, 'Anika Crosby', '2014-01-07', 'Est itaque qui nesci', 'Nisi voluptate ex ex', 77, 'Veniam doloribus ac', NULL, 0, 'abobakar', 'abobakar', 0, '2019-04-25 15:46:14', '2019-04-26 23:22:32'),
(2, 'Random number', '2014-01-07', 'mudassir', 'garden town', 55, 'received', NULL, 1, 'abobakar', 'abobakar', 1, '0000-00-00 00:00:00', '2019-04-26 22:01:17'),
(3, 'Madeline Dunlap', '1979-03-28', 'Sit et fugit sed re', 'Ad veritatis ullam e', 80, 'Ipsum ut ex accusant', NULL, 0, 'abobakar', 'abobakar', 0, '2019-04-26 18:17:50', '2019-04-26 23:42:33'),
(4, 'mudassir ali', '1979-03-28', 'djdjkj', 'Ad veritatis ullam e', 80, 'Ipsum ut ex accusant', NULL, 1, 'abobakar', 'abobakar', 1, '2019-04-26 18:19:31', '2019-04-26 21:33:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `payables`
--
ALTER TABLE `payables`
  ADD PRIMARY KEY (`payable_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `payables`
--
ALTER TABLE `payables`
  MODIFY `payable_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
