-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2019 at 01:43 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `al-shahwan`
--

-- --------------------------------------------------------

--
-- Table structure for table `receivables`
--

CREATE TABLE `receivables` (
  `receivable_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `paid_by` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `delete_status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receivables`
--

INSERT INTO `receivables` (`receivable_id`, `name`, `date`, `contact`, `address`, `amount`, `paid_by`, `image`, `status`, `created_by`, `updated_by`, `delete_status`, `created_at`, `updated_at`) VALUES
(1, 'ali', '1993-06-11', 'mudassir', 'garden town', 77, 'jdsldf', NULL, 0, 'abobakar', 'abobakar', 0, '2019-04-26 18:09:44', '2019-04-26 18:09:44'),
(2, 'Phelan Bruce', '1993-06-11', 'Rerum delectus blan', 'Nihil et amet sit ', 70, 'Aliqua Possimus et', NULL, 1, 'abobakar', 'abobakar', 1, '2019-04-26 17:51:44', '2019-04-26 23:42:51'),
(3, 'Shelby Graves', '2006-05-26', 'Dolorum qui officia ', 'Blanditiis reprehend', 78, 'Minima expedita est ', NULL, 0, 'abobakar', 'abobakar', 0, '2019-04-26 17:53:22', '0000-00-00 00:00:00'),
(4, 'Teagan Thomas', '1975-09-15', 'At aut non deleniti ', 'Incididunt molestias', 42, 'Natus elit quia inv', NULL, 1, 'abobakar', 'abobakar', 1, '2019-04-26 18:11:40', '2019-04-26 23:43:11'),
(5, 'hlakjhdksfh', '2019-04-11', 'jkhk', 'hkjhk', 1212, 'hkjh', NULL, 0, 'abobakar', 'abobakar', 0, '2019-04-26 18:38:30', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `receivables`
--
ALTER TABLE `receivables`
  ADD PRIMARY KEY (`receivable_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `receivables`
--
ALTER TABLE `receivables`
  MODIFY `receivable_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
