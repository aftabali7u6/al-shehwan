<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Expense extends CI_Controller {

	function __construct() {
		parent::__construct();
		check_user_session();
		$this->load->model('Expense_model');
		$this->load->model('User_model', 'user');
		$this->load->model('Products_model', 'products');

	}

	public function index(){
		if ($this->session->userdata('name')) {

		$expense = $this->Expense_model->select_join('expense','exp_id','categories','cat_id');
		// echo "<pre>"; print_r($expense);exit;
		$this->load->view('expenses/index',compact('expense'));
		}
	}

	public function create() {
		if ($this->session->userdata('name')) {
			$this->load->view('employees/create');
		}
	}

	public function store() {
	if ($this->session->userdata('name')) {
		if (isset($_FILES['photo']['name']) && $_FILES['photo']['name'] != '') {
			$config= array();
			$config['upload_path'] = FCPATH.'images/employees';
			$config['allowed_types'] = 'gif|jpg|png|mp4';
			$this->load->library('upload',$config);
			$this->upload->do_upload('photo');
			$data = $this->upload->data();
			if($data) {
				$image = $data['file_name']; 
			} } else {
				$image = "oldphoto";
			}

			$data = array(
				'name' => $this->input->post('name'),
				'date' => $this->input->post('date'),
				'contact' => $this->input->post('contact'),
				'salary' => $this->input->post('salary'),
				'created_by' => $this->session->userdata('name'),
				'image' => $image
			);
			$store = $this->Employees_model->insert('employees',$data);
			if(!empty($store)) {
				$this->session->set_flashdata('pro_success','Employee added successfully.');
				redirect('employee_index');
			}
			else{
				$this->session->set_flashdata('pro_msg','Something went wrong...!');
				redirect('add_employee');
			}
		}
	}
	public function update(){
	if ($this->session->userdata('name')) {
		$employee_id = $this->input->post('update_id');
		if (isset($_FILES['photo']['name']) && $_FILES['photo']['name'] != '') {
			$config= array();
			$config['upload_path'] = FCPATH.'images/';
			$config['allowed_types'] = 'gif|jpg|png|mp4';
			$this->load->library('upload',$config);
			$this->upload->do_upload('photo');
			$data = $this->upload->data();
			if($data) {
				$image = $data['file_name']; 
			} } else{
				$image = "File not selected !!!!";
			}
		} 
		$data = array(
			'name' => $this->input->post('name'),
			'date' => $this->input->post('date'),
			'contact' => $this->input->post('contact'),
			'salary' => $this->input->post('salary'),
			'image' => $image,
			'created_by' => $this->session->userdata('name'),
			'updated_by' => $this->session->userdata('name'),
			'created_at' => date('d-m-Y H-i:s'),
			'updated_at' => date('Y-m-d H-i:s')
		);
		$result = $this->Employees_model->update('employees',$data,'employee_id',$employee_id);
		if($result) {
			$this->session->set_flashdata("update_msg","Record is updated successfully.");
			redirect('employee_index',compact($result));
		} else {
			$this->session->set_flashdata("update_msg","Something went wrong!");
			redirect('employee_index',compact($result));
		}
	}

	public function change_status(){		
		$this->layout = '';
		$data = array();
		if($this->session->userdata('name')){
			$id=$this->input->post('id');
			$status=$this->input->post('status');
			if($status==0) {
				$data['status'] = 1;
			} else {
				$data['status'] = 0;
			}			
			$this->Receivable_model->update_by('employee_id',$id,$data); 
			$data['id'] =$id;
			echo json_encode($data);
		}
	}

	public function delete($id){
		if($this->session->userdata('name')){		
			$data=$this->Employees_model->delete_by('employee_id',$id);
			if($data){
				$this->session->set_flashdata("delete_msg","Record delete successfully.");
				redirect('employee_index',compact($data));
			}

		}else{
			redirect('employee_index');
		}
	}

	/// autocomplete for categories
	public function cat_info()
	{
		$response = array();
		$cat_nam = $this->input->get('term');
		$products = $this->Expense_model->client_info('categories','cat_name',$cat_nam);			
			echo json_encode($products);
			exit;
	}



	public function cat_info_get()
	{
		$cat_id = $this->input->post('cat_ex_id');
		$products = $this->Expense_model->client_info_get('categories','cat_id',$cat_id);
		// echo "<pre>";
		// print_r($products);exit;
			if(isset($products))
			{
				echo json_encode($products);
			}
			exit;
	}	

	/// insert new expenses

	public function new_expense_exe()
	{
			$cat_id = $this->input->post('cat_exp_id');
			$category = $this->products->get_product_by_column('categories','cat_id',$cat_id);
			if($category)
			{
				$this->expense_without_cat();
			}
			else
			{

				$this->expense_with_cat();
			}	
	}

	public function expense_without_cat()
	{
		$data = array(
		'cat_id' => $this->input->post('cat_exp_id'),
		'exp_date' => $this->input->post('exp_date'),
		'amount' => $this->input->post('amount'),
		'exp_description' => $this->input->post('desc'),
		'created_by' => $this->session->userdata('id'),
		'created_at' => date('Y-m-d H-i:s')
		);

		$exp_res = $this->Expense_model->insert('expense',$data);
		if($exp_res)
		{
			$this->session->set_flashdata('add_success','Expense is added successfully.');
			redirect('expense');
		}
		else
		{
			$this->session->set_flashdata('add_fail','Expense can not be addedd...');
			redirect('expense');
		
		}


	} 

	public function expense_with_cat()
	{
		$cat_data = array(
				'cat_name' => $this->input->post('cat_name'),
				'created_by' => $this->session->userdata('id'),
				'created_at' => date('Y-m-d H-i:s')
		);
		$cat_res = $this->Expense_model->insert('categories',$cat_data);
		if($cat_res)
		{
				$data = array(
				'cat_id' => $cat_res,
				'exp_date' => $this->input->post('exp_date'),
				'amount' => $this->input->post('amount'),
				'exp_description' => $this->input->post('desc'),
				'created_by' => $this->session->userdata('id'),
				'created_at' => date('Y-m-d H-i:s')
				);

				$exp_res = $this->Expense_model->insert('expense',$data);
				if($exp_res)
				{
					$this->session->set_flashdata('add_success','Expense is added successfully.');
					redirect('expense');
				}
				else
				{
					$this->session->set_flashdata('add_fail','Expense can not be addedd...');
					redirect('expense');
				
				}
		}
		else
		{
			$this->session->set_flashdata('add_fail','Something went wrong in categories insertion...');
					redirect('expense');
		}
	} 
}	