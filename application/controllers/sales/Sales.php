<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales extends CI_Controller {

	function __construct() {
		parent::__construct();
		check_user_session();
		$this->load->model('Sales_model','sale');
		$this->load->model('User_model', 'user');
		$this->load->model('Products_model', 'products');
	}

	public function index(){
		}

	public function new_sale_view()
	{

		$product_list = $this->sale->select_products('product');
		$invoice_number = $this->sale->select_invo_number();
		$invoice_number =isset($invoice_number->invo_id)?$invoice_number->invo_id:1;
		// echo "$invoice_number";exit();
		$this->load->view('sales/new_sale_invoice',compact('product_list','invoice_number'));
	}
	public function sale_invoice_with_user()
	{

		$payment_method = $this->input->post('payment_method');
		$cash = $this->input->post('cash');
		$credit = $this->input->post('credit');
		$status = '';
			if(!empty($credit))
			{
				$status = 0;
			}
			else
			{
				$status = 1;
			}
			$invo_num = $this->input->post('invo_num');
			if($invo_num < 10)
			{
				$invo_num="00"."".$invo_num;
			}
			else if($invo_num < 100)
			{
				$invo_num="0"."".$invo_num;
			}
			else
			{
				$invo_num = $invo_num;
			}
		$desc = $this->input->post('desc');
		$comp_name = $this->input->post('comp_name');
		$contact = $this->input->post('contact');
		$address = $this->input->post('address');
		$vat_num = $this->input->post('vat_num');
		$user_data = array(
			'company_name' => $comp_name,
			'phone' => $contact,
			'address' => $address,
			'vat_no' => $vat_num,
			'type' => 'Client',
			'status' => 1,
			'created_by' => $this->session->userdata('id'),
			'created_at' => date('Y-m-d H-i:s')
		);
		$user_res = $this->products->insert_product('users',$user_data);
		if(!empty($user_res))
		{

			$invo_date = formated_date($this->input->post('invo_date'));	
			$login_user_id = $this->session->userdata('id');
			$invo_data = array(

					'invoice_no' => $invo_num,
					'user_id' => $user_res,
					'invoice_date' => $invo_date,
					'payment_method' => $payment_method,
					'cash' => $cash,
					'credit' => $credit,
					'description' => $desc,
					'type' => 'Sale',
					'invo_status' => $status,
					'created_by' => $login_user_id,
					'created_at' => date('Y-m-d H-i:s')
			);

			$invo_res = $this->products->insert_product('invoice',$invo_data);
			if($invo_res)
			{	
							//$res = $this->porducts->insert_product();
							$products = $this->input->post('product');
							$vendor_id = $this->get_vendor_id();
							$i = 0;
				foreach ($products as $index =>$product) 
				{ 

					$product = isset($this->input->post('product')[$index])?$this->input->post('product')[$index]:'';
					$weight = isset($this->input->post('weight')[$index])?$this->input->post('weight')[$index]:'';
					$quantity = isset($this->input->post('quantity')[$index])?$this->input->post('quantity')[$index]:'';
					$rate = isset($this->input->post('rate')[$index])?$this->input->post('rate')[$index]:'';
					$discount = isset($this->input->post('discount')[$index])?$this->input->post('discount')[$index]:'';
					$total_exc_vat = isset($this->input->post('total_exc_vat')[$index])?$this->input->post('total_exc_vat')[$index]:'';
					$vat_perc = isset($this->input->post('vat_perc')[$index])?$this->input->post('vat_perc')[$index]:'';
					$vat_sar = isset($this->input->post('vat_sar')[$index])?$this->input->post('vat_sar')[$index]:'';
					$total_amt = isset($this->input->post('total_amt')[$index])?$this->input->post('total_amt')[$index]:'';
					
							$sale_data = array(

									'client_id' => $vendor_id,
									'invo_id' => $invo_res,
									'product_id' => $product,
									'weight_unit' => $weight,
									'qty' => $quantity,
									'rate' => $rate,
									'discount' => $discount,
									'total_exec_vat' => $total_exc_vat,
									'vat_percent' => $vat_perc,
									'vat_sar' => $vat_sar,
									'total_amount' => $total_amt,
									'status' => 1,
									'created_by' => $login_user_id,
									'created_at' => date('Y-m-d H-i:s')
								);
							$sale_res = $this->sale->insert_product('sales',$sale_data);
							$i++;
				}
							if($i!=0)
							{
								$receivables = $this->db->select("* 
								,(select sum(total_amount) FROM sales where sales.invo_id=invoice.invo_id) as total_amount")
								->from('invoice')->where('invoice.invo_id',$invo_res)
								->join('users', 'users.user_id=invoice.user_id')->join('sales','sales.invo_id=invoice.invo_id')->join('product','sales.product_id=product.pro_id')->get()->result();
									$response['flag'] = true;
									$response['data'] = $receivables;
								echo json_encode($response);
								exit;
							}
							else
							{
								if(!empty($invoices)){
									$response['flag'] = false;
								}
								echo json_encode($response);

								exit;
							}
			}
			else
			{
				if(!empty($invoices))
				{
					$response['flag'] = false;
				}
				echo json_encode($response);
				exit;
			}
		}
		else
		{
			if(!empty($invoices))
			{
				$response['flag'] = false;
			}
			echo json_encode($response);
			exit;
		}
	}
	public function sale_invoice_without_user()
	{
			$payment_method = $this->input->post('payment_method');
			$cash = $this->input->post('cash');
			$credit = $this->input->post('credit');
			$status = '';
			if(!empty($credit))
			{
				$status = 0;
			}
			else
			{
				$status = 1;
			}
			$invo_num = $this->input->post('invo_num');
			if($invo_num < 10)
			{
				$invo_num="00"."".$invo_num;
			}
			else if($invo_num < 100)
			{
				$invo_num="0"."".$invo_num;
			}
			else
			{
				$invo_num = $invo_num;
			}
			$desc = $this->input->post('desc');
			$invo_date = formated_date($this->input->post('invo_date'));
			$login_user_id = $this->session->userdata('id');
			$vat_num = $this->input->post('vat_num');
			$user_id = $this->products->get_product_by_column('users','vat_no',$vat_num);
			$adv_avl_amt = $this->input->post('adv_avl');
			$grd_total = $this->input->post('grand_total');
			$adv_deduction ='';
			$adv_flag = false;
			if(isset($adv_avl_amt) && !empty($adv_avl_amt))
			{
				
				if($adv_avl_amt > $grd_total)
				{
					$adv_deduction = $grd_total;
				}
				else
				{
					$adv_deduction = $adv_avl_amt;
				}
				$adv_flag = true;
			}
			else
			{
				$adv_flag = false;
			}
			
			
			$invo_data = array(

					'invoice_no' => $invo_num,
					'user_id' => $user_id->user_id,
					'invoice_date' => $invo_date,
					'payment_method' => $payment_method,
					'advance_deduction' => $adv_deduction,
					'cash' => $cash,
					'credit' => $credit,
					'description' => $desc,
					'type' => 'Sale',
					'invo_status' => $status,
					'created_by' => $login_user_id,
					'created_at' => date('Y-m-d H-i:s')
			);

			$invo_res = $this->products->insert_product('invoice',$invo_data);
			if($invo_res)
			{
				$products = $this->input->post('product');
				$vendor_id = $this->get_vendor_id();
				$i = 0;
				foreach ($products as $index =>$product) 
				{ 

					$product = isset($this->input->post('product')[$index])?$this->input->post('product')[$index]:'';
					$weight = isset($this->input->post('weight')[$index])?$this->input->post('weight')[$index]:'';
					$quantity = isset($this->input->post('quantity')[$index])?$this->input->post('quantity')[$index]:'';
					$rate = isset($this->input->post('rate')[$index])?$this->input->post('rate')[$index]:'';
					$discount = isset($this->input->post('discount')[$index])?$this->input->post('discount')[$index]:'';
					$total_exc_vat = isset($this->input->post('total_exc_vat')[$index])?$this->input->post('total_exc_vat')[$index]:'';
					$vat_perc = isset($this->input->post('vat_perc')[$index])?$this->input->post('vat_perc')[$index]:'';
					$vat_sar = isset($this->input->post('vat_sar')[$index])?$this->input->post('vat_sar')[$index]:'';
					$total_amt = isset($this->input->post('total_amt')[$index])?$this->input->post('total_amt')[$index]:'';
					
							$sale_data = array(

									'client_id' => $vendor_id,
									'invo_id' => $invo_res,
									'product_id' => $product,
									'weight_unit' => $weight,
									'qty' => $quantity,
									'rate' => $rate,
									'discount' => $discount,
									'total_exec_vat' => $total_exc_vat,
									'vat_percent' => $vat_perc,
									'vat_sar' => $vat_sar,
									'total_amount' => $total_amt,
									'status' => 1,
									'created_by' => $login_user_id,
									'created_at' => date('Y-m-d H-i:s')
								);
							$sale_res = $this->sale->insert_product('sales',$sale_data);
							$i++;
						}

							if($i!=0)
							{
								if($adv_flag == true)
								{
									$adv_avl_result = ''; 
									
									if($adv_avl_amt > $grd_total)
									{
										$db_adv_avl = $adv_avl_amt - $grd_total;
										$data = array(
											'advance_amount' => $db_adv_avl,
										);
										$adv_avl_result = $this->sale->product_update('client_advance_deposit',$data,'user_id',$user_id->user_id);
										if($adv_avl_result)
										{
											$receivables = $this->db->select("* 
											,(select sum(total_amount) FROM sales where sales.invo_id=invoice.invo_id) as total_amount")
											->from('invoice')->where('invoice.invo_id',$invo_res)
											->join('users', 'users.user_id=invoice.user_id')->join('sales','sales.invo_id=invoice.invo_id')->join('product','sales.product_id=product.pro_id')->get()->result();
												$response['flag'] = true;
												$response['data'] = $receivables;
											echo json_encode($response);
											exit;
										}
										else
										{

												$response['flag'] = false;
											echo json_encode($response);
											exit;
										}
									}
									else
									{
									
										$db_adv_not_avl = '';
											$data = array(
											'advance_amount' => $db_adv_not_avl,
										);
										$db_adv_not_avl = $this->sale->product_update('client_advance_deposit',$data,'user_id',$user_id->user_id);
										if($db_adv_not_avl)
										{
											$receivables = $this->db->select("* 
											,(select sum(total_amount) FROM sales where sales.invo_id=invoice.invo_id) as total_amount")
											->from('invoice')->where('invoice.invo_id',$invo_res)
											->join('users', 'users.user_id=invoice.user_id')->join('sales','sales.invo_id=invoice.invo_id')->join('product','sales.product_id=product.pro_id')->get()->result();
												$response['flag'] = true;
												$response['data'] = $receivables;
											echo json_encode($response);
											exit;
										}
										else
										{
												$response['flag'] = false;
											echo json_encode($response);
											exit;
										}
									}
								}
								else
								{
									$receivables = $this->db->select("* 
											,(select sum(total_amount) FROM sales where sales.invo_id=invoice.invo_id) as total_amount")
											->from('invoice')->where('invoice.invo_id',$invo_res)
											->join('users', 'users.user_id=invoice.user_id')->join('sales','sales.invo_id=invoice.invo_id')->join('product','sales.product_id=product.pro_id')->get()->result();
												$response['flag'] = true;
												$response['data'] = $receivables;
											echo json_encode($response);
											exit;
								}
									
									
								
							}
							else
							{
								if(!empty($invoices))
								{
									$response['flag'] = false;
								}
								echo json_encode($response);
								exit;
							}
			}
			else
			{
				if(!empty($invoices))
				{
					$response['flag'] = false;
				}
				echo json_encode($response);
				exit;
			}
	}
	public function new_sale_exe()
	{
		$vat_num = $this->input->post('vat_num');

		$user_vat_num = $this->sale->get_product_by_column('users','vat_no',$vat_num);
		if($user_vat_num)
		{
			$this->sale_invoice_without_user();
		}
		else
		{
			$this->sale_invoice_with_user();
		}	
	}
	
	//ajax client get 
	public function client_info()
	{
		$response = array();
		$vat_no = $this->input->get('term');
		$products = $this->sale->client_info('users','vat_no',$vat_no);

			if(!empty($products)){
				$response = $products;
				/*foreach ($products as $product) {
				}*/
			}

			echo json_encode($response);
			exit;
	}
	public function client_info_get()
	{
		
		$vat_no = $this->input->post('vat_no');
		$products = $this->sale->client_info_get('users','vat_no',$vat_no);
			if(isset($products))
			{
				$adv_user_id = $products->user_id;
				$advance_result = $this->sale->get_product_by_column('client_advance_deposit','user_id',$adv_user_id);
				if($advance_result)
				{
					echo json_encode(array($products,$advance_result));
				}
				else
				{
					echo json_encode(array($products,false));
				}
				
			}
			exit;
	}


	///// advance functions 

	public function adv_client_info_get()
	{
		
		$vat_no = $this->input->post('vat_no');
		$products = $this->sale->client_info_get('users','vat_no',$vat_no);
			if(isset($products))
			{
				
					echo json_encode($products);
			}
			exit;
	}
	public function get_vendor_id()
	{
		$vendor_vat_no = $this->input->post('vat_num');
		$vendor_res = $this->products->get_product_by_column('users','vat_no',$vendor_vat_no);
		return $vendor_res->user_id;
	}
/// sale Register
	public function sale_register()
	{
		$invoices = $this->sale->show_sale();
		// echo "<pre>";
		// echo print_r($invoices);
		// exit;
		$this->load->view('sales/sales_list', compact('invoices'));
	}
	public function edit_sales()
	{
		$edit_id = $this->input->get('edit_id');
		$invoices=$this->db->select('*')
		->where('invo_id',$edit_id)
		->from('invoice')
		->join('users','users.user_id=invoice.user_id')->order_by('invoice.created_at','desc')->get()->row();
		// echo "<pre>";
		// echo print_r($invoices);
		// exit;
		$sales = $this->products->select_all_puchases('sales','invo_id',$edit_id);
		// echo "<pre>";
		// echo print_r($sales);
		// exit;
		// // $single_purchase = $this->products->get_product_by_column('purchase','invo_id',$edit_id);
		$product_list = $this->products->select_products('product');
		$this->load->view('sales/sale_edit_view',compact('invoices','sales','product_list'));
	}

	public function sale_edit_exe()
	{
			$old_img = $this->input->post('old_img');
			if (isset($_FILES['invoice_img']['name']) && $_FILES['invoice_img']['name'] != '') 
			{

				$config= array();
				$config['upload_path'] = FCPATH.'assets/images/purchase';
				$config['allowed_types'] = 'gif|jpg|png|mp4';
				$this->load->library('upload',$config);
				$this->upload->do_upload('invoice_img');
				$data = $this->upload->data();
				if($data) {
					$image = $data['file_name']; 
				} 
			} 
			else
			{
				$image = $old_img;
			}
				$user_id=$this->input->post('user_id');
				$invo_id=$this->input->post('invo_id');

				$user_data = array(
					'company_name' => $this->input->post('comp_name'),
					'phone'=>$this->input->post('contact'),
					'vat_no'=>$this->input->post('vat_num'),
					'address'=> $this->input->post('address'),
					'updated_by'=> $this->session->userdata('id'),
					'updated_at'=> date('Y-m-d H-i:s')
				);
					$update_user = $this->products->product_update('users',$user_data,'user_id',$user_id);
					if($update_user != 0)
					{
						$cash = $this->input->post('cash');
						$credit = $this->input->post('credit');
						$gtotal = $this->input->post('grand_total');
						$paid_amt = $cash+$credit;
						$status = '';
						if($paid_amt == $gtotal)
						{
							$status = 1;
						}
						else
						{
							$status = 0;
						}
						$invo_date = formated_date($this->input->post('invo_date'));
						$sal_ord_dat = invo_date($this->input->post('sale_ord_date'));
						$invo_data = array(
						'user_id' => $user_id,
						'invoice_no' => $this->input->post('invo_num'),
						'invoice_date'=>$invo_date,
						'sale_ord_no'=>$this->input->post('sale_ord_num'),
						'sale_ord_date'=> $sal_ord_dat,
						'deliv_note_no'=>$this->input->post('deliv_note_num'),
						'cust_tckt_no'=> $this->input->post('cust_tck_num'),
						'payment_method'=> $this->input->post('payment_method'),
						'description'=> $this->input->post('desc'),
						'cash'=> $cash,
						'img'=> $image,
						'credit'=> $credit,
						'invo_status'=> $status,
						'updated_by'=> $this->session->userdata('id'),
						'updated_at'=> date('Y-m-d H-i:s')
						);
						$update_invo = $this->products->product_update('invoice',$invo_data,'invo_id',$invo_id);
						if($update_invo)
						{
							$del_product = $this->products->delete_products('purchase','invo_id',$invo_id);
							if($del_product)
							{
								$updat_product='';
								for ($i=0; $i <count($this->input->post('product')) ; $i++) 
								{ 
								$product_data = array(
									'vendor_id' => $this->input->post('user_id'),
									'invo_id' => $this->input->post('invo_id'),
									'product_id' => $this->input->post('product')[$i],
									'weight_unit'=>$this->input->post('weight')[$i],
									'qty'=>$this->input->post('quantity')[$i],
									'rate'=> $this->input->post('rate')[$i],
									'discount'=>$this->input->post('discount')[$i],
									'total_exec_vat'=> $this->input->post('total_exc_vat')[$i],
									'vat_percent'=>$this->input->post('vat_perc')[$i],
									'vat_sar'=>$this->input->post('vat_sar')[$i],
									'total_amount'=>$this->input->post('total_amt')[$i],		
									);
									$updat_product = $this->products->insert_product('purchase',$product_data);
								}
									if($updat_product)
									{
										$this->session->set_flashdata('update_msg_success','Invoice updated successfully.');
										redirect('purchase_list');
									}
									else
									{
										$this->session->set_flashdata('update_msg_error','Something went Wrong updating products data ...');
									redirect('purchase_list');
									}
							}
							else
							{
								$this->session->set_flashdata('update_msg_error','Something went Wrong deleting products data ...');
								redirect('purchase_list');
							}
							
						}
						else
						{
							$this->session->set_flashdata('update_msg_error','Something went Wrong updating invoice purchase data ...');
							redirect('purchase_list');
						}
						
					}
					else
					{
						echo 'user not updated';exit;
						$this->session->set_flashdata('update_msg_error','Something went Wrong updating purchase data ...');
						redirect('purchase_list');
					}

	}

	public function delete_sale()
	{
		$id =  $this->input->post('del');
		
		$del_data = $this->products->get_product_by_column('sales','invo_id',$id);
		// echo "<pre>"; print_r($del_data); exit;
		$client_id =  $del_data->client_id;
		$sale_id =  $del_data->sale_id;
		// echo $client_id; exit;
		$vendor_res = $this->products->delete_products('users','user_id',$client_id);
		if($vendor_res)
		{
				$invo_res = $this->products->delete_products('invoice','invo_id',$id);

			if($invo_res)
			{

			$sale_res = $this->products->delete_products('sales','invo_id',$id);
				if($sale_res)
				{
					$this->session->set_flashdata('update_msg','Sales invoice deleted successfully!');
					return  redirect('sale_list');
				}
				else
				{
					$this->session->set_flashdata('update_msg','Sales Invoice can not be deleted!');
					return  redirect('sale_list');
				}
			}
			else
			{
				$this->session->set_flashdata('update_msg','Invoice can not be deleted!');
					return  redirect('sale_list');
			}
		}
		else
		{
			$this->session->set_flashdata('update_msg','Vendor can not be deleted!');
					return  redirect('sale_list');
		}
		
	}

	/// sale receivable amounts
	public function sale_reveivable()
	{
		$receivables = $this->db->select("* 
	,(select sum(vat_sar) FROM sales where sales.invo_id=invoice.invo_id) as vat_sar")
	->from('invoice')->where('invoice.type','Sale')
	->join('users', 'users.user_id=invoice.user_id')->order_by('invoice.created_at','desc')->get()->result();
	// echo $receivable[0]->invoice_no;exit;
	// echo "<pre>";
	// print_r($receivable);exit;
		$this->load->view('sales/sale_receivable_view', compact('receivables'));
	
	}

	public function update_receive_exe()
	{
		$amt_cash = $this->input->post('amt_cash');
		$receivable_amount = $this->input->post('receivable_amount');
		$received_amount = $this->input->post('received_amount');
		$credit = $receivable_amount-$received_amount;
		$cash = $amt_cash+$received_amount;
		$invo_id = $this->input->post('invo_id');
		$data = array(
				'cash' => $cash,
				'credit' => $credit,
		);
		$update_invo = $this->sale->product_update('invoice',$data,'invo_id',$invo_id);
		if($update_invo)
		{
			echo json_encode($data);
			exit;
		}
		else
		{
			echo json_encode('');
			exit;
		}
		
	}

	/// advance payments 

	public function Advance_payments_view()
	{
		 $advance_amounts = $this->db->select('*')->from('client_advance_deposit cad')->join("users","users.user_id=cad.user_id")->where('users.type','Client')->get()->result();
		$this->load->view('sales/client_advance_paymnet', compact('advance_amounts'));
	}

	public function Advance_payments_exe()
	{
		$vat_num = $this->input->post('vat_num');
		$user_vat_num = $this->sale->get_product_by_column('users','vat_no',$vat_num);
		if($user_vat_num)
		{

			$this->advance_without_user();
		}
		else
		{
			$this->advance_with_user();
		}	
	
	}
/// insert user with advance payment
		public function advance_without_user()
	{
		$advance_amt = $this->input->post('adv_amt');
		$adv_date = formated_date($this->input->post('invo_date'));
		$adv_desc = $this->input->post('advance_desc');
		$vat_num = $this->input->post('vat_num');
		$user_vat_num = $this->sale->get_product_by_column('users','vat_no',$vat_num);
		$check_adv_user = $this->sale->get_product_by_column('client_advance_deposit','user_id',$user_vat_num->user_id);

		/// validate user 
		if($check_adv_user->user_id)
		{
			$updated_amt = $check_adv_user->advance_amount + $advance_amt;
			$adv_data = array(
			'advance_amount' => $updated_amt,
			'advance_date' => $adv_date,
			'description' => $adv_desc,
			'status' => 1,
			'updated_by' => $this->session->userdata('id'),
			'updated_at' => date('Y-m-d H-i:s')
		);

			$user_res = $this->sale->sale_update('client_advance_deposit',$adv_data,'user_id',$user_vat_num->user_id);

			if(!empty($user_res))
			{

				$this->session->set_flashdata('adv_msg','Advance transaction is completed successfully!');
				redirect('Advance_payments_view');
			}
			else
			{
				$this->session->set_flashdata('adv_msg_error','Advance transaction can not be inserted!');
				redirect('Advance_payments_view');
			}
		}
		else
		{
			$adv_data = array(
			'user_id' => $user_vat_num->user_id,
			'advance_amount' => $advance_amt,
			'advance_date' => $adv_date,
			'description' => $adv_desc,
			'status' => 1,
			'created_by' => $this->session->userdata('id'),
			'created_at' => date('Y-m-d H-i:s')
			);
			$user_res = $this->sale->insert_product('client_advance_deposit',$adv_data);
			if(!empty($user_res))
			{

				$this->session->set_flashdata('adv_msg','Advance transaction is completed successfully!');
				redirect('Advance_payments_view');
			}
			else
			{
				$this->session->set_flashdata('adv_msg_error','Advance transaction can not be inserted!');
				redirect('Advance_payments_view');
			}
		}

		// echo "client does exist ".$user_vat_num->user_id;exit;
		
		
			
		
	
	}
/// advance amount without user
	public function advance_with_user()
	{
		$vat_num = $this->input->post('vat_num');
		$comp_name = $this->input->post('comp_name');
		$contact = $this->input->post('contact');
		$address = $this->input->post('address');
		$user_data = array(
			'company_name' => $comp_name,
			'phone' => $contact,
			'address' => $address,
			'vat_no' => $vat_num,
			'type' => 'Client',
			'status' => 1,
			'created_by' => $this->session->userdata('id'),
			'created_at' => date('Y-m-d H-i:s')
		);
		$user_res = $this->sale->insert_product('users',$user_data);
		if(!empty($user_res))
		{
				$user_vat_num = $this->sale->get_product_by_column('users','vat_no',$vat_num);
				$advance_amt = $this->input->post('adv_amt');
				$adv_date = formated_date($this->input->post('invo_date'));
				$adv_desc = $this->input->post('advance_desc');
				$adv_data = array(
					'user_id' => $user_vat_num->user_id,
					'advance_amount' => $advance_amt,
					'advance_date' => $adv_date,
					'description' => $adv_desc,
					'status' => 1,
					'created_by' => $this->session->userdata('id'),
					'created_at' => date('Y-m-d H-i:s')
				);
				$adv_data = $this->sale->insert_product('client_advance_deposit',$adv_data);
				if(!empty($adv_data))
				{
					$this->session->set_flashdata('adv_msg','Advance transaction is completed successfully!');
					redirect('Advance_payments_view');
				}
				else
				{
					$this->session->set_flashdata('adv_msg_error','Advance transaction is not completed...!');
					redirect('Advance_payments_view');
				}

			
		}
		else
		{
			$this->session->set_flashdata('adv_msg_error','Client does not inserted...');
			redirect('Advance_payments_view');
		}
	}

	public function update_advance_amt()
	{
		$advanc_id = $this->input->post('advance_id');
		$advance_amt = $this->input->post('update_amt');
				$adv_date = formated_date($this->input->post('update_date'));
				$adv_desc = $this->input->post('adv_desc');
				$adv_data = array(
					'advance_amount' => $advance_amt,
					'advance_date' => $adv_date,
					'description' => $adv_desc,
					'updated_by' => $this->session->userdata('id'),
					'updated_at' => date('Y-m-d H-i:s')
				);
				$adv_data = $this->sale->product_update('client_advance_deposit',$adv_data,'adv_id',$advanc_id);
				if(!empty($adv_data))
				{
					$this->session->set_flashdata('adv_msg','Advance transaction is updated successfully!');
					redirect('Advance_payments_view');
				}
				else
				{
					$this->session->set_flashdata('adv_msg_error','Advance transaction is not updated...!');
					redirect('Advance_payments_view');
				}
	}


		public function delete_advance_amt()
		{
			$delete_id = $this->input->post('del_id');
			$del_result = $this->sale->delete_advance('client_advance_deposit','adv_id',$delete_id);
				if(!empty($del_result))
				{
					$this->session->set_flashdata('adv_msg','Advance transaction is deleted successfully!');
					redirect('Advance_payments_view');
				}
				else
				{
					$this->session->set_flashdata('adv_msg_error','Advance transaction is not delted...!');
					redirect('Advance_payments_view');
				}
		}

			public function update_sales_exe()
	{

		   $old_img = $this->input->post('old_img');
			if (isset($_FILES['invoice_img']['name']) && $_FILES['invoice_img']['name'] != '') 
			{

				$config= array();
				$config['upload_path'] = FCPATH.'assets/images/sale';
				$config['allowed_types'] = 'gif|jpg|png|mp4';
				$this->load->library('upload',$config);
				$this->upload->do_upload('invoice_img');
				$data = $this->upload->data();
				if($data) {
					$image = $data['file_name']; 
				} 
			} 
			else
			{
				$image = $old_img;
			}
				$user_id=$this->input->post('user_id');

				$invo_id=$this->input->post('invo_id');
				$user_data = array(
					'company_name' => $this->input->post('comp_name'),
					'phone'=>$this->input->post('contact'),
					'vat_no'=>$this->input->post('vat_num'),
					'address'=> $this->input->post('address'),
					'updated_by'=> $this->session->userdata('id'),
					'updated_at'=> date('Y-m-d H-i:s')
				);
				$update_user = $this->products->product_update('users',$user_data,'user_id',$user_id);

					if($update_user>0)
					{

						$cash = $this->input->post('cash');
						$credit = $this->input->post('credit');
						$gtotal = $this->input->post('grand_total');
						$paid_amt = $cash+$credit;
         
						if(abs(($gtotal-$paid_amt)/$paid_amt) < 0.00001)
						{
							$status=1;
						}
						else
						{
							$status=0;
						}
						$invo_date = formated_date($this->input->post('invo_date'));
						$sal_ord_dat = formated_date($this->input->post('sale_ord_date'));
						$invo_data = array(
						'user_id' => $user_id,
						'invoice_no' => $this->input->post('invo_num'),
						'invoice_date'=>$invo_date,
						'sale_ord_no'=>$this->input->post('sale_ord_num'),
						'sale_ord_date'=> $sal_ord_dat,
						'deliv_note_no'=>$this->input->post('deliv_note_num'),
						'cust_tckt_no'=> $this->input->post('cust_tck_num'),
						'payment_method'=> $this->input->post('payment_method'),
						'description'=> $this->input->post('desc'),
						'cash'=> $cash,
						'img'=> $image,
						'credit'=> $credit,
						'invo_status'=> $status,
						'updated_by'=> $this->session->userdata('id'),
						'updated_at'=> date('Y-m-d H-i:s')
						);
						$update_invo = $this->products->product_update('invoice',$invo_data,'invo_id',$invo_id);
						if($update_invo)
						{
							$del_product = $this->products->delete_products('sales','invo_id',$invo_id);

							if($del_product)
							{
								$updat_product='';
								for ($i=0; $i <count($this->input->post('product')) ; $i++) 
								{ 
								$product_data = array(
									'client_id' => $this->input->post('user_id'),
									'invo_id' => $this->input->post('invo_id'),
									'product_id' => $this->input->post('product')[$i],
									'weight_unit'=>$this->input->post('weight')[$i],
									'qty'=>$this->input->post('quantity')[$i],
									'rate'=> $this->input->post('rate')[$i],
									'discount'=>$this->input->post('discount')[$i],
									'total_exec_vat'=> $this->input->post('total_exc_vat')[$i],
									'vat_percent'=>$this->input->post('vat_perc')[$i],
									'vat_sar'=>$this->input->post('vat_sar')[$i],
									'total_amount'=>$this->input->post('total_amt')[$i],
									'status'=>1		
									);
									$updat_product = $this->products->insert_product('sales',$product_data);
								}
									if($updat_product)
									{
										$this->session->set_flashdata('update_msg_success','Invoice updated successfully.');
										redirect('sale_list');
									}
									else
									{
										$this->session->set_flashdata('update_msg_error','Something went Wrong updating products data ...');
									redirect('sale_list');
									}
							}
							else
							{
								$this->session->set_flashdata('update_msg_error','Something went Wrong deleting products data ...');
								redirect('sale_list');
							}
							
						}
						else
						{
							$this->session->set_flashdata('update_msg_error','Something went Wrong updating invoice purchase data ...');
							redirect('sale_list');
						}
						
					}
					else
					{
						echo 'user not updated';exit;
						$this->session->set_flashdata('update_msg_error','Something went Wrong updating purchase data ...');
						redirect('purchase_list');
					}				
	}

		/// print sale ajax call

		public function saleBetween(){
		$fromDate =  $this->input->post('start_date');
		$dueDate = $this->input->post('due_date');
		$vat_no = $this->input->post('vat_no');
		// echo json_encode($dueDate);exit;

		$invoices = $this->db->select("*,(select sum(total_amount) FROM sales where sales.invo_id=invoice.invo_id) as total_amount
			,(select sum(qty) FROM sales where sales.invo_id=invoice.invo_id) as qty 
			,(select sum(total_exec_vat) FROM sales where sales.invo_id=invoice.invo_id) as total_exec_vat 
			,(select sum(vat_sar) FROM sales where sales.invo_id=invoice.invo_id) as vat_sar")
			->from('invoice')->where('invoice.type','Sale')->join('users', 'users.user_id=invoice.user_id')->where('users.type','Client')->order_by('invoice.invoice_no','asc');

			if(isset($vat_no) && !empty($vat_no) ){
				$invoices->where('users.vat_no',$vat_no);
			}

			if(isset($dueDate) && !empty($dueDate) ){
				$invoices->where('invoice.invoice_date <=', $dueDate);
			}
			if(isset($fromDate) && !empty($fromDate) ){
				$invoices->where('invoice.invoice_date >=',$fromDate);
			}
			$record=$invoices->get()->result_array();
		if($record){
			$response['flag'] = true;
			$response['data'] = $record;
		}else{
			$response['flag'] = false;
		}
		echo json_encode($response);

		exit;
	}

}	