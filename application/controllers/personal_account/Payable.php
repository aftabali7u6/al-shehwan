<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payable extends CI_Controller {

	function __construct(){
		parent::__construct();
		check_user_session();
		$this->load->model('Payable_model');
		$this->load->model('User_model', 'user');
		$this->load->model('Products_model', 'products');
	}

	public function index() {
			$payables = $this->Payable_model->select_products('payables');
			$this->load->view('personal_account/list_payable', compact('payables'));
	}

	public function create() {
		if ($this->session->userdata('name')) {
			$this->load->view('personal_account/create');
		}
	}

	public function store() {
			if (isset($_FILES['photo']['name']) && $_FILES['photo']['name'] != '') {
				$config= array();
				$config['upload_path'] = FCPATH.'assets/images/payable';
				$config['allowed_types'] = 'gif|jpg|png|mp4';
				$this->load->library('upload',$config);
				$this->upload->do_upload('photo');
				$data = $this->upload->data();
				if($data) 
				{
					$image = $data['file_name']; 
				} 
			} 
			else 
			{
					$image = "File not selected !!!!";
			}
				$pay_date = strtotime($this->input->post('date'));
     		$payable_date=date("Y-m-d", $pay_date);
				$data = array(
					'name' => $this->input->post('name'),
					'date' => $payable_date,
					'contact' => $this->input->post('contact'),
					'address' => $this->input->post('address'),
					'amount' => $this->input->post('amount'),
					'received_from' => $this->input->post('recivedfrom'),
					'image' => $image,
					'description' => $this->input->post('desc'),
					'status' => 0,
					'created_by' => $this->session->userdata('id'),
					'created_at' => date('Y-m-d H-i:s'),
				);
				$res = $this->Payable_model->insert_product('payables',$data);
				if(!empty($res)) {
					$this->session->set_flashdata('add_success','Record is added successfully.');
					redirect('payable_index');
				} else {
					$this->session->set_flashdata('add_fail','Something went wrong...!');
					redirect('payable_index');
				}
			}

	public function upadate_payables_exe() {
		$status =0;
		$rem_amt = 0;
		$payable_amt = $this->input->post('payable_amt');
		$pay_amt = $this->input->post('pay_amt');
		if($pay_amt == $payable_amt)
		{
			$status = 1;
			$rem_amt = 0;
		}
		else
		{
			$status = 0;
			$rem_amt = $payable_amt - $pay_amt;	
		}
		
		// pay_amt
		$payable_id = $this->input->post('payableId');
		$old_img = $this->input->post('old_img');	
		if (isset($_FILES['photo']['name']) && $_FILES['photo']['name'] != '') {
			$config= array();
			$config['upload_path'] = FCPATH.'assets/images/payable';
			$config['allowed_types'] = 'gif|jpg|png|mp4';
			$this->load->library('upload',$config);
			$this->upload->do_upload('photo');
			$data = $this->upload->data();
			if($data) {
				$image = $data['file_name']; 
			} } else{
				$image = $old_img;
			}

			$data = array(
				'name' => $this->input->post('name'),
				'contact' => $this->input->post('contact'),
				'address' => $this->input->post('address'),
				'amount' => $rem_amt,
				'image' => $image,
				'status' => $status,
				'received_from' => $this->input->post('recivedfrom'),
				'description' => $this->input->post('desc'),
				'created_by' => $this->session->userdata('name'),
				'updated_at' => date('Y-m-d H-i:s')
			);

			$result = $this->Payable_model->product_update('payables',$data,'payable_id',$payable_id);

			if($result) {
				$this->session->set_flashdata("add_success","Record is updated successfully.");
				redirect('payable_index',compact($result));
			} else {
				$this->session->set_flashdata("add_fail","Something went wrong!");
				redirect('payable_index');
			}
	}
	public function get_personal_payables()
	{
		$payable_id = $this->input->post('invo_id');
		$update_invo = $this->products->get_product_by_column('payables','payable_id',$payable_id);
		if($update_invo)
		{
			echo json_encode($update_invo);
			exit;
		}
		else
		{
			echo json_encode('');
			exit;
		}
		
	}

}	