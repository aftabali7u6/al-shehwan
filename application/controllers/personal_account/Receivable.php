<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Receivable extends CI_Controller {
	function __construct(){
		parent::__construct();
		check_user_session();
		$this->load->model('Receivable_model');
		$this->load->model('User_model', 'user');
		$this->load->model('Products_model', 'products');
		$this->load->model('Payable_model');
	}

	public function index() {
		if ($this->session->userdata('name')) {
			$receivables = $this->Receivable_model->select_receivables('receivables');
			$this->load->view('personal_account/list_receivable', compact('receivables'));
		}
	}

	public function create() {
		if ($this->session->userdata('name')) {
			$this->load->view('personal_account/create_receivable');
		}
	}

	public function store() {
			if (isset($_FILES['photo']['name']) && $_FILES['photo']['name'] != '') {
				$config= array();
				$config['upload_path'] = FCPATH.'assets/images/receivables';
				$config['allowed_types'] = 'gif|jpg|png|mp4';
				$this->load->library('upload',$config);
				$this->upload->do_upload('photo');
				$data = $this->upload->data();
				if($data) {
					$image = $data['file_name']; 
				} } else {
					$image = "File not selected !!!!";
				}
				$data = array(
					'name' => $this->input->post('name'),
					'date' => $this->input->post('date'),
					'contact' => $this->input->post('contact'),
					'address' => $this->input->post('address'),
					'amount' => $this->input->post('amount'),
					'image' => $image,
					'paid_by' => $this->input->post('pai_by'),
					'description' => $this->input->post('desc'),
					'created_by' => $this->session->userdata('id'),
					'created_at' => date('Y-m-d H-i:s'),
				);
	// echo "<pre>";print_r($data);exit();
				$res = $this->Receivable_model->insert_receivable('receivables',$data);
				if(!empty($res)) {
				// echo "string";exit();
					$this->session->set_flashdata('add_success','Record is added successfully.');
					redirect('receivable_index');
				} else {
					$this->session->set_flashdata('pro_msg','Something went wrong...!');
					redirect('add_fail');
				}
			}

	public function update(){

			$status =0;
		$rem_amt = 0;
		$receivable_am = $this->input->post('receivable_am');
		$receiving_amt = $this->input->post('receiving_amt');
		if($receiving_amt == $receivable_am)
		{
			$status = 1;
			$rem_amt = 0;
		}
		else
		{
			$status = 0;
			$rem_amt = $receivable_am - $receiving_amt;	
		}


		$receivable_id = $this->input->post('receivable_id');
		$old_img = $this->input->post('old_img');
		if (isset($_FILES['photo']['name']) && $_FILES['photo']['name'] != '') {
			$config= array();
			$config['upload_path'] = FCPATH.'assets/images/receivables';
			$config['allowed_types'] = 'gif|jpg|png|mp4';
			$this->load->library('upload',$config);
			$this->upload->do_upload('photo');
			$data = $this->upload->data();
			if($data) {
				$image = $data['file_name']; 
			} } else {
				$image = $old_img;
			}
			$data = array(
				'name' => $this->input->post('name'),
				'contact' => $this->input->post('contact'),
				'address' => $this->input->post('address'),
				'amount' => $rem_amt,
				'image' => $image,
				'status' => $status,
				'paid_by' => $this->input->post('paid_by'),
				'date' => $this->input->post('date'),
				'description' => $this->input->post('desc'),
				'created_by' => $this->session->userdata('id'),
				'created_at' => date('Y-m-d H-i:s'),
			);

			$result = $this->Receivable_model->receivable_update('receivables',$data,'receivable_id',$receivable_id);
			if($result) {
				$this->session->set_flashdata("add_success","Record is updated successfully.");
				redirect('receivable_index');
			} else {
				$this->session->set_flashdata("add_fail","Something went wrong!");
				redirect('receivable_index');
			}
		}
	public function change_status(){		
		$this->layout = '';
		$data = array();
		if($this->session->userdata('name')){
			$id=$this->input->post('id');
			$status=$this->input->post('status');
			if($status==0) {
				$data['status'] = 1;
			} else {
				$data['status'] = 0;
			}			
			$this->Receivable_model->update_by('receivable_id',$id,$data); 
			$data['id'] =$id;
			echo json_encode($data);
		}
	}
	public function hide_record(){
		$this->layout = '';
		$data = array();
		if($this->session->userdata('name')) {
			$id=$this->input->post('id');
			$status=$this->input->post('status');
			if($status==0) {
				$data['delete_status'] = 1;
			} else {
				$data['delete_status'] = 0;
			}
			$this->Receivable_model->update_by('receivable_id',$id,$data);
			$data['id'] =$id;
			echo json_encode($data);
		}
	}

	/// reveivable get data ajax call 
	public function get_personal_receivables()
	{
		$receivable_id = $this->input->post('invo_id');
		$update_invo = $this->products->get_product_by_column('receivables','receivable_id',$receivable_id);
		if($update_invo)
		{
			echo json_encode($update_invo);
			exit;
		}
		else
		{
			echo json_encode('');
			exit;
		}
		
	}

	/// Personal accounts dashboard
		public function personal_account_index()
	{
		$payables = $this->Payable_model->select_products('payables');
		$amount_payable = $this->Payable_model->select_payable_amount('payables');
		

		$receivables = $this->Receivable_model->select_receivables('receivables');
		$amount_receivable = $this->Receivable_model->select_receivable_amount('receivables');
		$this->load->view('personal_account/personal_account_dashboard',compact('amount_payable','amount_receivable','payables','receivables'));

	}
}	