<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form extends CI_Controller {

	function __construct() {
		parent::__construct();
		check_user_session();
		$this->load->model('Employees_model');
		$this->load->model('User_model', 'user');
	}

	public function index(){
		$this->load->view('form/form');
		}
	
}	