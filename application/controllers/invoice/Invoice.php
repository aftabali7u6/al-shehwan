<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Invoice_model');
		check_user_session();
		
	}

	public function index($id)
	{
    
		$this->layout='';
		$invoice_data = $this->Invoice_model->select('invoice',$id);
		$user_data = $this->Invoice_model->user_data('users',$invoice_data->user_id);
		$purchase_data = $this->Invoice_model->purchase_data('purchase',$id);
		$this->load->view('invoice/invoice',compact('user_data','invoice_data','purchase_data'));
		
	}


	public function sales_invoice($id)
	{
    
		$this->layout='';
		$invoice_data = $this->Invoice_model->select('invoice',$id);
		$user_data = $this->Invoice_model->user_data('users',$invoice_data->user_id);
		$sales_data = $this->Invoice_model->purchase_data('sales',$id);
		$this->load->view('sales/sales_invoice',compact('user_data','invoice_data','sales_data'));
		
	}

	public function testSaleInvoice(){
		$this->load->view('invoice/testing_invoice');
	}


	
}	