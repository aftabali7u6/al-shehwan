<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

function __construct(){
		parent::__construct();
		check_user_session();
		$this->load->model('Receivable_model');
		$this->load->model('Payable_model');
		$this->load->model('User_model', 'user');
		$this->layout = "default";
	}
	public function index()
	{
		$purchases = $this->db->select("*,(select sum(total_amount) FROM purchase where purchase.invo_id=invoice.invo_id) as total_amount 
			,(select sum(total_exec_vat) FROM purchase where purchase.invo_id=invoice.invo_id) as total_exec_vat 
			,(select sum(qty) FROM purchase where purchase.invo_id=invoice.invo_id) as qty 
			,(select sum(vat_sar) FROM purchase where purchase.invo_id=invoice.invo_id) as vat_sar")
			->from('invoice')->where('invoice.type','Purchase') 
			->join('users', 'users.user_id=invoice.user_id')->where('users.type','Vendor')->order_by('invoice.created_at','desc')->limit(20)->get()->result();
		$sales=	$this->db->select("*,(select sum(total_amount) FROM sales where sales.invo_id=invoice.invo_id) as total_amount ,
				,(select sum(total_exec_vat) FROM sales where sales.invo_id=invoice.invo_id) as total_exec_vat 
				,(select sum(vat_sar) FROM sales where sales.invo_id=invoice.invo_id) as vat_sar")
				->from('invoice')->where('invoice.type','Sale')
				->join('users', 'users.user_id=invoice.user_id')->where('users.type','Client')->order_by('invoice.created_at','desc')->limit(20)->get()->result();
				
		$expenses= $this->db->select("*,(select sum(amount) FROM expense) as total_expense")->from('expense')->join('categories', 'categories.cat_id=expense.cat_id')
		->limit(20)->get()->result();
	
		
		// 		echo "<pre>";
		// print_r($expenses);exit();




		$sumPurchase = 0;
		$sumSales = 0;

		foreach ($purchases as $subArray) {
		 	$sumPurchase+=$subArray->total_amount;
		}
		foreach ($sales as $subSal) {
		 	$sumSales+=$subSal->total_amount;
		}

		$this->load->view('dashboard',compact('purchases','sales','sumPurchase','sumSales','expenses'));
	}
}
