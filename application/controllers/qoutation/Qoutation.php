<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qoutation extends CI_Controller {

	function __construct() {
		parent::__construct();
		check_user_session();
		$this->load->model('Invoice_model');
		$this->load->model('Sales_model','sale');
		$this->load->model('Qoutation_model','qoute');
		$this->load->model('Products_model', 'products');
		
	}

	public function index()
	{

		$qoutes =$this->db->select("*,(select sum(total_amount) FROM qoutation where qoutation.invo_id=invoice.invo_id) as total_amount ,
				,(select sum(total_exec_vat) FROM qoutation where qoutation.invo_id=invoice.invo_id) as total_exec_vat 
				,(select sum(vat_sar) FROM qoutation where qoutation.invo_id=invoice.invo_id) as vat_sar")
				->from('invoice')->where('invoice.type','Qoute')
				->join('users', 'users.user_id=invoice.user_id')->where('users.type','Client')->order_by('invoice.created_at','desc')->limit(20)->get()->result();
				$this->load->view('quotation/quotation',compact('qoutes'));	
	}
	public function new_quotation()
	{

			$product_list = $this->sale->select_products('product');
		$invoice_number = $this->sale->select_invo_number();
		$invoice_number =isset($invoice_number->invo_id)?$invoice_number->invo_id:1;
		$this->load->view('quotation/new_quotation',compact('product_list','invoice_number'));
	}



	/// insert qoutation 

	public function qoute_invoice_with_user()
	{
		$payment_method = $this->input->post('payment_method');
		$cash = $this->input->post('cash');
		$credit = $this->input->post('credit');
		$status = '';
			if(!empty($credit))
			{
				$status = 0;
			}
			else
			{
				$status = 1;
			}
		$desc = $this->input->post('desc');
		$comp_name = $this->input->post('comp_name');
		$contact = $this->input->post('contact');
		$address = $this->input->post('address');
		$vat_num = $this->input->post('vat_num');
		$user_data = array(
			'company_name' => $comp_name,
			'phone' => $contact,
			'address' => $address,
			'vat_no' => $vat_num,
			'type' => 'Client',
			'status' => 1,
			'created_by' => $this->session->userdata('id'),
			'created_at' => date('Y-m-d H-i:s')
		);
		$user_res = $this->products->insert_product('users',$user_data);
		if(!empty($user_res))
		{
			$invo_num = $this->input->post('invo_num');
			$invoice_date = strtotime($this->input->post('invo_date'));
     		$invo_date=date("Y-m-d", $invoice_date);
			
			$login_user_id = $this->session->userdata('id');
			$invo_data = array(

					'invoice_no' => $invo_num,
					'user_id' => $user_res,
					'invoice_date' => $invo_date,
					'payment_method' => $payment_method,
					'cash' => $cash,
					'credit' => $credit,
					'description' => $desc,
					'type' => 'Qoute',
					'invo_status' => $status,
					'created_by' => $login_user_id,
					'created_at' => date('Y-m-d H-i:s')
			);

			$invo_res = $this->products->insert_product('invoice',$invo_data);
			if($invo_res)
			{	
							//$res = $this->porducts->insert_product();
							$products = $this->input->post('product');
							$vendor_id = $this->get_vendor_id();
							$i = 0;
				foreach ($products as $index =>$product) 
				{ 

					$product = isset($this->input->post('product')[$index])?$this->input->post('product')[$index]:'';
					$weight = isset($this->input->post('weight')[$index])?$this->input->post('weight')[$index]:'';
					$quantity = isset($this->input->post('quantity')[$index])?$this->input->post('quantity')[$index]:'';
					$rate = isset($this->input->post('rate')[$index])?$this->input->post('rate')[$index]:'';
					$discount = isset($this->input->post('discount')[$index])?$this->input->post('discount')[$index]:'';
					$total_exc_vat = isset($this->input->post('total_exc_vat')[$index])?$this->input->post('total_exc_vat')[$index]:'';
					$vat_perc = isset($this->input->post('vat_perc')[$index])?$this->input->post('vat_perc')[$index]:'';
					$vat_sar = isset($this->input->post('vat_sar')[$index])?$this->input->post('vat_sar')[$index]:'';
					$total_amt = isset($this->input->post('total_amt')[$index])?$this->input->post('total_amt')[$index]:'';
					
							$sale_data = array(

									'client_id' => $vendor_id,
									'invo_id' => $invo_res,
									'product_id' => $product,
									'weight_unit' => $weight,
									'qty' => $quantity,
									'rate' => $rate,
									'discount' => $discount,
									'total_exec_vat' => $total_exc_vat,
									'vat_percent' => $vat_perc,
									'vat_sar' => $vat_sar,
									'total_amount' => $total_amt,
									'status' => 1,
									'created_by' => $login_user_id,
									'created_at' => date('Y-m-d H-i:s')
								);
							$sale_res = $this->qoute->insert_product('qoutation',$sale_data);
							$i++;
				}
							if($i!=0)
							{
								$receivables = $this->db->select("* 
								,(select sum(total_amount) FROM qoutation where qoutation.invo_id=invoice.invo_id) as total_amount")
								->from('invoice')->where('invoice.invo_id',$invo_res)
								->join('users', 'users.user_id=invoice.user_id')->join('qoutation','qoutation.invo_id=invoice.invo_id')->join('product','qoutation.product_id=product.pro_id')->get()->result();
									$response['flag'] = true;
									$response['data'] = $receivables;
								echo json_encode($response);
								exit;
							}
							else
							{
								if(!empty($invoices)){
									$response['flag'] = false;
								}
								echo json_encode($response);

								exit;
							}
			}
			else
			{
				if(!empty($invoices))
				{
					$response['flag'] = false;
				}
				echo json_encode($response);
				exit;
			}
		}
		else
		{
			if(!empty($invoices))
			{
				$response['flag'] = false;
			}
			echo json_encode($response);
			exit;
		}
	}
	public function qoute_invoice_without_user()
	{
			$payment_method = $this->input->post('payment_method');
			$cash = $this->input->post('cash');
			$credit = $this->input->post('credit');
			$status = '';
			if(!empty($credit))
			{
				$status = 0;
			}
			else
			{
				$status = 1;
			}
			$desc = $this->input->post('desc');
			$invo_num = $this->input->post('invo_num');
			$invo_date = $this->input->post('invo_date');
			$login_user_id = $this->session->userdata('id');
			$vat_num = $this->input->post('vat_num');
			$user_id = $this->products->get_product_by_column('users','vat_no',$vat_num);
			$adv_avl_amt = $this->input->post('adv_avl');
			$grd_total = $this->input->post('grand_total');
			$adv_deduction ='';
			$adv_flag = false;
			if(isset($adv_avl_amt) && !empty($adv_avl_amt))
			{
				
				if($adv_avl_amt > $grd_total)
				{
					$adv_deduction = $adv_avl_amt - $grd_total;
				}
				else
				{
					$adv_deduction = $adv_avl_amt;
				}
				$adv_flag = true;
			}
			else
			{
				$adv_flag = false;
			}
			
			
			$invo_data = array(

					'invoice_no' => $invo_date,
					'user_id' => $user_id->user_id,
					'invoice_date' => date('Y-m-d', strtotime($invo_date)),
					'payment_method' => $payment_method,
					'advance_deduction' => $adv_deduction,
					'cash' => $cash,
					'credit' => $credit,
					'description' => $desc,
					'type' => 'Qoute',
					'invo_status' => $status,
					'created_by' => $login_user_id,
					'created_at' => date('Y-m-d H-i:s')
			);

			$invo_res = $this->products->insert_product('invoice',$invo_data);
			if($invo_res)
			{
				$products = $this->input->post('product');
				$vendor_id = $this->get_vendor_id();
				$i = 0;
				foreach ($products as $index =>$product) 
				{ 

					$product = isset($this->input->post('product')[$index])?$this->input->post('product')[$index]:'';
					$weight = isset($this->input->post('weight')[$index])?$this->input->post('weight')[$index]:'';
					$quantity = isset($this->input->post('quantity')[$index])?$this->input->post('quantity')[$index]:'';
					$rate = isset($this->input->post('rate')[$index])?$this->input->post('rate')[$index]:'';
					$discount = isset($this->input->post('discount')[$index])?$this->input->post('discount')[$index]:'';
					$total_exc_vat = isset($this->input->post('total_exc_vat')[$index])?$this->input->post('total_exc_vat')[$index]:'';
					$vat_perc = isset($this->input->post('vat_perc')[$index])?$this->input->post('vat_perc')[$index]:'';
					$vat_sar = isset($this->input->post('vat_sar')[$index])?$this->input->post('vat_sar')[$index]:'';
					$total_amt = isset($this->input->post('total_amt')[$index])?$this->input->post('total_amt')[$index]:'';
					
							$sale_data = array(

									'client_id' => $vendor_id,
									'invo_id' => $invo_res,
									'product_id' => $product,
									'weight_unit' => $weight,
									'qty' => $quantity,
									'rate' => $rate,
									'discount' => $discount,
									'total_exec_vat' => $total_exc_vat,
									'vat_percent' => $vat_perc,
									'vat_sar' => $vat_sar,
									'total_amount' => $total_amt,
									'status' => 1,
									'created_by' => $login_user_id,
									'created_at' => date('Y-m-d H-i:s')
								);
							$sale_res = $this->qoute->insert_product('qoutation',$sale_data);
							$i++;
						}

							if($i!=0)
							{
								if($adv_flag == true)
								{
									$adv_avl_result = ''; 
									
									if($adv_avl_amt > $grd_total)
									{
										$db_adv_avl = $adv_avl_amt - $grd_total;
										$data = array(
											'advance_amount' => $db_adv_avl,
										);
										$adv_avl_result = $this->sale->product_update('client_advance_deposit',$data,'user_id',$user_id->user_id);
										if($adv_avl_result)
										{
											$receivables = $this->db->select("* 
											,(select sum(total_amount) FROM qoutation where qoutation.invo_id=invoice.invo_id) as total_amount")
											->from('invoice')->where('invoice.invo_id',$invo_res)
											->join('users', 'users.user_id=invoice.user_id')->join('qoutation','qoutation.invo_id=invoice.invo_id')->join('product','qoutation.product_id=product.pro_id')->get()->result();
												$response['flag'] = true;
												$response['data'] = $receivables;
											echo json_encode($response);
											exit;
										}
										else
										{

												$response['flag'] = false;
											echo json_encode($response);
											exit;
										}
									}
									else
									{
									
										$db_adv_not_avl = '';
											$data = array(
											'advance_amount' => $db_adv_not_avl,
										);
										$db_adv_not_avl = $this->sale->product_update('client_advance_deposit',$data,'user_id',$user_id->user_id);
										if($db_adv_not_avl)
										{
											$receivables = $this->db->select("* 
											,(select sum(total_amount) FROM qoutation where qoutation.invo_id=invoice.invo_id) as total_amount")
											->from('invoice')->where('invoice.invo_id',$invo_res)
											->join('users', 'users.user_id=invoice.user_id')->join('qoutation','qoutation.invo_id=invoice.invo_id')->join('product','qoutation.product_id=product.pro_id')->get()->result();
												$response['flag'] = true;
												$response['data'] = $receivables;
											echo json_encode($response);
											exit;
										}
										else
										{
												$response['flag'] = false;
											echo json_encode($response);
											exit;
										}
									}
								}
								else
								{
									$receivables = $this->db->select("* 
											,(select sum(total_amount) FROM qoutation where qoutation.invo_id=invoice.invo_id) as total_amount")
											->from('invoice')->where('invoice.invo_id',$invo_res)
											->join('users', 'users.user_id=invoice.user_id')->join('qoutation','qoutation.invo_id=invoice.invo_id')->join('product','qoutation.product_id=product.pro_id')->get()->result();
												$response['flag'] = true;
												$response['data'] = $receivables;
											echo json_encode($response);
											exit;
								}
									
									
								
							}
							else
							{
								if(!empty($invoices))
								{
									$response['flag'] = false;
								}
								echo json_encode($response);
								exit;
							}
			}
			else
			{
				if(!empty($invoices))
				{
					$response['flag'] = false;
				}
				echo json_encode($response);
				exit;
			}
	}
	public function new_quotation_exe()
	{
		$vat_num = $this->input->post('vat_num');

		$user_vat_num = $this->qoute->get_product_by_column('users','vat_no',$vat_num);
		if($user_vat_num)
		{
			$this->qoute_invoice_without_user();
		}
		else
		{
			$this->qoute_invoice_with_user();
		}	
	}
	
	public function get_vendor_id()
	{
		$vendor_vat_no = $this->input->post('vat_num');
		$vendor_res = $this->products->get_product_by_column('users','vat_no',$vendor_vat_no);
		return $vendor_res->user_id;
	}

	public function client_info()
	{
		$response = array();
		$vat_no = $this->input->get('term');
		$products = $this->sale->client_info('users','vat_no',$vat_no);

			if(!empty($products)){
				$response = $products;
				/*foreach ($products as $product) {
				}*/
			}

			echo json_encode($response);
			exit;
	}
	public function client_info_get()
	{
		
		$vat_no = $this->input->post('vat_no');
		$products = $this->sale->client_info_get('users','vat_no',$vat_no);
			if(isset($products))
			{
				echo json_encode($products);	
			}
			else
			{ 
				$data = ['error'];
				echo json_encode($data);
			}
			exit;
	}
}	