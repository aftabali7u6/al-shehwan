<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase extends CI_Controller {

	function __construct(){
		parent::__construct();
		check_user_session();
		$this->load->model('Products_model', 'products');
		$this->load->library('session');
	}
	
	public function index()
	{
		$product_list = $this->products->select_products('product');
		$this->load->view('purchase/new_purchase',compact('product_list'));
	}


	
	public function purchase_list()
	{
		$invoices = $this->db->select("*,(select sum(total_amount) FROM purchase where purchase.invo_id=invoice.invo_id) as total_amount 
,(select sum(total_exec_vat) FROM purchase where purchase.invo_id=invoice.invo_id) as total_exec_vat 
,(select sum(qty) FROM purchase where purchase.invo_id=invoice.invo_id) as qty 
,(select sum(vat_sar) FROM purchase where purchase.invo_id=invoice.invo_id) as vat_sar")
->from('invoice')->where('invoice.type','Purchase') 
->join('users', 'users.user_id=invoice.user_id')->where('users.type','Vendor')->order_by('invoice.created_at','desc')->get()->result();
// =======
// 			,(select sum(total_exec_vat) FROM purchase where purchase.invo_id=invoice.invo_id) as total_exec_vat 
// 			,(select sum(vat_sar) FROM purchase where purchase.invo_id=invoice.invo_id) as vat_sar")
		// ->from('invoice')->where('invoice.type','Purchase') 
		// ->join('users', 'users.user_id=invoice.user_id')->where('users.type','Vendor')->order_by('invoice.created_at','desc')->get()->result();
		// $invoices=$this->db->select('*')
		// ->from('invoice')
		// ->join('users','users.user_id=invoice.user_id')->order_by('invoice.created_at','desc')->get()->result();
		// $invoices = array();
		// foreach ($tmp_invoices as $invoice) {
		// 	$purchases=$this->db->get_where('purchase',array('invo_id'=>$invoice->invo_id))->result();
	 //        $invoice->purchases = $purchases;
	 //        array_push($invoices, $invoice);
		//}
		// echo "<pre>";
		// print_r($invoices);exit;
		$this->load->view('purchase/purchase_list', compact('invoices'));
	}

	public function purchaseBetween(){
		$fromDate =  $this->input->post('start_date');
		$dueDate = $this->input->post('due_date');
		$vat_no = $this->input->post('vat_no');
		// echo json_encode($dueDate);exit;

		$invoices = $this->db->select("*,(select sum(total_amount) FROM purchase where purchase.invo_id=invoice.invo_id) as total_amount
			,(select sum(qty) FROM purchase where purchase.invo_id=invoice.invo_id) as qty
			,(select sum(total_exec_vat) FROM purchase where purchase.invo_id=invoice.invo_id) as total_exec_vat 
			,(select sum(vat_sar) FROM purchase where purchase.invo_id=invoice.invo_id) as vat_sar")

			->from('invoice')->where('invoice.type','Purchase')->join('users', 'users.user_id=invoice.user_id')->where('users.type','Vendor')->order_by('invoice.invoice_date','asc');
		
			

			if(isset($vat_no) && !empty($vat_no) ){
				$invoices->where('users.vat_no',$vat_no);
			}

			if(isset($dueDate) && !empty($dueDate) ){
				$invoices->where('invoice.invoice_date <=', formated_date($dueDate));
			}
			if(isset($fromDate) && !empty($fromDate) ){
				$invoices->where('invoice.invoice_date >=', formated_date($fromDate));
			}

			$record=$invoices->get()->result_array();
			$response = array();
		if(($record)){
			$response['flag'] = true;
			$response['data'] = $record;
		}
		echo json_encode($response);

		exit;
	}

	public function view_purchase_invoice()
	{
		$this->load->view("purchase/purchase_invoice_view");
	}

	public function delete_perchase()
	{
		$id =  $this->input->post('del');
		$del_data = $this->products->get_product_by_column('purchase','invo_id',$id);
		$vendor_id =  $del_data->vendor_id;
		$pur_id =  $del_data->pur_id;
		$vendor_res = $this->products->delete_products('users','user_id',$vendor_id);
		if($vendor_res)
		{
			$invo_res = $this->products->delete_products('invoice','invo_id',$id);

			if($invo_res)
			{

				$pur_res = $this->products->delete_products('purchase','invo_id',$id);
				if($pur_res)
				{
					$this->session->set_flashdata('update_msg','Purchase invoice deleted successfully!');
					return  redirect('purchase_list');
				}
				else
				{
					$this->session->set_flashdata('update_msg','Purchase Invoice products can not be deleted!');
					return  redirect('purchase_list');
				}
			}
			else
			{
				$this->session->set_flashdata('update_msg','Invoice can not be deleted!');
				return  redirect('purchase_list');
			}
		}
		else
		{
			$this->session->set_flashdata('update_msg','Vendor can not be deleted!');
			return  redirect('purchase_list');
		}
		
	}

	public function edit_purchase()
	{
		$edit_id = $this->input->get('edit_id');
		$invoices=$this->db->select('*')
		->where('invo_id',$edit_id)
		->from('invoice')
		->join('users','users.user_id=invoice.user_id')->order_by('invoice.created_at','desc')->get()->row();
		$purchases = $this->products->select_all_puchases('purchase','invo_id',$edit_id);
		// echo "<pre>";
		// print_r($purchases);exit;
		$single_purchase = $this->products->get_product_by_column('purchase','invo_id',$edit_id);
		$product_list = $this->products->select_products('product');
		$this->load->view('purchase/purchase_edit_view',compact('invoices','purchases','single_purchase','product_list'));
	}

	public function update_purchase_exe()
	{
		$old_img = $this->input->post('old_img');
			if (isset($_FILES['invoice_img']['name']) && $_FILES['invoice_img']['name'] != '') 
			{

				$config= array();
				$config['upload_path'] = FCPATH.'assets/images/purchase';
				$config['allowed_types'] = 'gif|jpg|png|mp4';
				$this->load->library('upload',$config);
				$this->upload->do_upload('invoice_img');
				$data = $this->upload->data();
				if($data) {
					$image = $data['file_name']; 
				} 
			} 
			else
			{
				$image = $old_img;
			}
				$user_id=$this->input->post('user_id');
				$invo_id=$this->input->post('invo_id');

				$user_data = array(
					'company_name' => $this->input->post('comp_name'),
					'phone'=>$this->input->post('contact'),
					'vat_no'=>$this->input->post('vat_num'),
					'address'=> $this->input->post('address'),
					'updated_by'=> $this->session->userdata('id'),
					'updated_at'=> date('Y-m-d H-i:s')
				);
					$update_user = $this->products->product_update('users',$user_data,'user_id',$user_id);
					if($update_user != 0)
					{
						$cash = $this->input->post('cash');
						$credit = $this->input->post('credit');
						$gtotal = $this->input->post('grand_total');
						$paid_amt = $cash+$credit;
						$status = '';
						if($paid_amt == $gtotal)
						{
							$status = 1;
						}
						else
						{
							$status = 0;
						}
						$invoice_date = strtotime($this->input->post('invo_date'));
						$invo_date=date("Y-m-d", $invoice_date);
						$sale_ord_dt = strtotime($this->input->post('sale_ord_date'));
						$sal_ord_dat=date("Y-m-d", $sale_ord_dt);
						$invo_data = array(
						'user_id' => $user_id,
						'invoice_no' => $this->input->post('invo_num'),
						'invoice_date'=>$invo_date,
						'sale_ord_no'=>$this->input->post('sale_ord_num'),
						'sale_ord_date'=> $sal_ord_dat,
						'deliv_note_no'=>$this->input->post('deliv_note_num'),
						'cust_tckt_no'=> $this->input->post('cust_tck_num'),
						'payment_method'=> $this->input->post('payment_method'),
						'description'=> $this->input->post('desc'),
						'cash'=> $cash,
						'img'=> $image,
						'credit'=> $credit,
						'invo_status'=> $status,
						'updated_by'=> $this->session->userdata('id'),
						'updated_at'=> date('Y-m-d H-i:s')
						);
						$update_invo = $this->products->product_update('invoice',$invo_data,'invo_id',$invo_id);
						if($update_invo)
						{
							$del_product = $this->products->delete_products('purchase','invo_id',$invo_id);
							if($del_product)
							{
								$updat_product='';
								for ($i=0; $i <count($this->input->post('product')) ; $i++) 
								{ 
								$product_data = array(
									'vendor_id' => $this->input->post('user_id'),
									'invo_id' => $this->input->post('invo_id'),
									'product_id' => $this->input->post('product')[$i],
									'weight_unit'=>$this->input->post('weight')[$i],
									'qty'=>$this->input->post('quantity')[$i],
									'rate'=> $this->input->post('rate')[$i],
									'discount'=>$this->input->post('discount')[$i],
									'total_exec_vat'=> $this->input->post('total_exc_vat')[$i],
									'vat_percent'=>$this->input->post('vat_perc')[$i],
									'vat_sar'=>$this->input->post('vat_sar')[$i],
									'total_amount'=>$this->input->post('total_amt')[$i],		
									);
									$updat_product = $this->products->insert_product('purchase',$product_data);
								}
									if($updat_product)
									{
										$this->session->set_flashdata('update_msg_success','Invoice updated successfully.');
										redirect('purchase_list');
									}
									else
									{
										$this->session->set_flashdata('update_msg_error','Something went Wrong updating products data ...');
									redirect('purchase_list');
									}
							}
							else
							{
								$this->session->set_flashdata('update_msg_error','Something went Wrong deleting products data ...');
								redirect('purchase_list');
							}
							
						}
						else
						{
							$this->session->set_flashdata('update_msg_error','Something went Wrong updating invoice purchase data ...');
							redirect('purchase_list');
						}
						
					}
					else
					{
						echo 'user not updated';exit;
						$this->session->set_flashdata('update_msg_error','Something went Wrong updating purchase data ...');
						redirect('purchase_list');
					}				
	}



	public function vendor_info()
	{
		$response = array();
		$vat_no = $this->input->get('term');
		$products = $this->products->vendor_info('users','vat_no',$vat_no);

		if(!empty($products)){
			$response = $products;
				/*foreach ($products as $product) {
				}*/
			}

			echo json_encode($response);
			exit;
		}
		public function vendor_info_get()
		{

			$vat_no = $this->input->post('vat_no');
			$products = $this->products->vendor_info_get('users','vat_no',$vat_no);
			if(isset($products)){
				echo json_encode($products);
			}
			exit;
		}

	
	public function get_vendor_id()
	{
		$vendor_vat_no = $this->input->post('vat_num');
		$vendor_res = $this->products->get_product_by_column('users','vat_no',$vendor_vat_no);
		return $vendor_res->user_id;
	}
	// public function purchase_invoice_with_user()
	// {

					
	// 	$payment_method = $this->input->post('payment_method');
	// 	$cash = $this->input->post('cash');
	// 	$credit = $this->input->post('credit');
	// 	$status = '';
	// 		if(!empty($credit))
	// 		{
	// 			$status = 0;
	// 		}
	// 		else
	// 		{
	// 			$status = 1;
	// 		}
	// 	$img = $this->input->post('img');
	// 	$desc = $this->input->post('desc');
	// 	$comp_name = $this->input->post('comp_name');
	// 	$contact = $this->input->post('contact');
	// 	$address = $this->input->post('address');
	// 	$vat_num = $this->input->post('vat_num');
	// 	$user_data = array(
	// 		'company_name' => $comp_name,
	// 		'phone' => $contact,
	// 		'address' => $address,
	// 		'vat_no' => $vat_num,
	// 		'type' => 'Vendor',
	// 		'status' => 1,
	// 		'created_by' => $this->session->userdata('id'),
	// 		'created_at' => date('Y-m-d H-i:s')
	// 	);
	// 	$user_res = $this->products->insert_product('users',$user_data);
	// 	if(!empty($user_res))
	// 	{
	// 		$vendor_vat_no = $this->input->post('vat_num');
	// 		$vendor_res = $this->products->get_product_by_column('users','vat_no',$vendor_vat_no);
	// 		return $vendor_res->user_id;
	// 	}
	// }

		public function purchase_invoice_with_user()
		{


			$payment_method = $this->input->post('payment_method');
			$cash = $this->input->post('cash');
				$credit = $this->input->post('credit');
		$status = '';
			if(!empty($credit))
			{
				$status = 0;
			}
			else
			{
				$status = 1;
			}

			$img = $this->input->post('img');
			$desc = $this->input->post('desc');
			$comp_name = $this->input->post('comp_name');
			$contact = $this->input->post('contact');
			$address = $this->input->post('address');
			$vat_num = $this->input->post('vat_num');
			$user_data = array(
				'company_name' => $comp_name,
				'phone' => $contact,
				'address' => $address,
				'vat_no' => $vat_num,
				'type' => 'Vendor',
				'status' => 1,
				'created_by' => $this->session->userdata('id'),
				'created_at' => date('Y-m-d H-i:s')
			);
			$user_res = $this->products->insert_product('users',$user_data);
			if(!empty($user_res))
			{
				$invo_num = $this->input->post('invo_num');
				$invo_date = formated_date($this->input->post('invo_date'));
				$sale_ord_num = $this->input->post('sale_ord_num');
				$sale_ord_date = formated_date($this->input->post('sale_ord_date'));
				$deliv_note_num = $this->input->post('deliv_note_num');
				$cust_tck_num = $this->input->post('cust_tck_num');
				$login_user_id = $this->session->userdata('id');
				$image ='';
				if (isset($_FILES['invoice_img']['name']) && $_FILES['invoice_img']['name'] != '') 
				{

					$config= array();
					$config['upload_path'] = FCPATH.'assets/images/purchase';
					$config['allowed_types'] = 'gif|jpg|png|mp4';
					$this->load->library('upload',$config);
					$this->upload->do_upload('invoice_img');
					$data = $this->upload->data();
					if($data) {
						$image = $data['file_name']; 
					} 
				} 
				else
				{
					$image = "";
				}
				$invo_data = array(

					'invoice_no' => $invo_num,
					'user_id' => $user_res,
					'invoice_date' => $invo_date,
					'sale_ord_no' => $sale_ord_num,
					'sale_ord_date' => $sale_ord_date,
					'deliv_note_no' => $deliv_note_num,
					'cust_tckt_no' => $cust_tck_num,
					'payment_method' => $payment_method,
					'cash' => $cash,
					'credit' => $credit,
					'img' => $image,
					'description' => $desc,
					'type' => 'Purchase',
					'invo_status' => $status,
					'created_by' => $login_user_id,
					'created_at' => date('Y-m-d H-i:s')
				);

				$invo_res = $this->products->insert_product('invoice',$invo_data);
				if($invo_res)
				{



							//$res = $this->porducts->insert_product();
					$products = $this->input->post('product');
					$vendor_id = $this->get_vendor_id();
					$i = 0;
					foreach ($products as $index =>$product) 
					{ 

						$product = isset($this->input->post('product')[$index])?$this->input->post('product')[$index]:'';
						$weight = isset($this->input->post('weight')[$index])?$this->input->post('weight')[$index]:'';
						$quantity = isset($this->input->post('quantity')[$index])?$this->input->post('quantity')[$index]:'';
						$rate = isset($this->input->post('rate')[$index])?$this->input->post('rate')[$index]:'';
						$discount = isset($this->input->post('discount')[$index])?$this->input->post('discount')[$index]:'';
						$total_exc_vat = isset($this->input->post('total_exc_vat')[$index])?$this->input->post('total_exc_vat')[$index]:'';
						$vat_perc = isset($this->input->post('vat_perc')[$index])?$this->input->post('vat_perc')[$index]:'';
						$vat_sar = isset($this->input->post('vat_sar')[$index])?$this->input->post('vat_sar')[$index]:'';
						$total_amt = isset($this->input->post('total_amt')[$index])?$this->input->post('total_amt')[$index]:'';

						$purchas_data = array(

							'vendor_id' => $vendor_id,
							'invo_id' => $invo_res,
							'product_id' => $product,
							'weight_unit' => $weight,
							'qty' => $quantity,
							'rate' => $rate,
							'discount' => $discount,
							'total_exec_vat' => $total_exc_vat,
							'vat_percent' => $vat_perc,
							'vat_sar' => $vat_sar,
							'total_amount' => $total_amt,
							'created_by' => $login_user_id,
							'created_at' => date('Y-m-d H-i:s')
						);
						$purchase_res = $this->products->insert_product('purchase',$purchas_data);
						$i++;

					// echo $vendor_id.'<br> '.$invo_res.'<br> '.$product.'<br> '.$weight.' <br>'.$quantity.' <br>'.$rate.'<br> '.$discount.'<br> '.$total_exc_vat.'<br> '.$vat_perc.'<br> '.$vat_sar.'<br> '.$total_amt.' <br>'.$payment_method.'<br> '.$cash.' <br>'.$credit.'<br> '.$image.'<br> '.$desc;exit;
					// 		echo $weight;
					//$weight = isset($_POST['weight'][$index])?$_POST['weight'][$index]:0;
					// $quantity = $this->input->post('quantity');
					// $rate = $this->input->post('rate');
					// $discount = $this->input->post('discount');
					// $total_exc_vat = $this->input->post('total_exc_vat');
					// $vat_perc = $this->input->post('vat_perc');
					// $vat_sar = $this->input->post('vat_sar');
					// $total_amt = $this->input->post('total_amt');


					}

					if($i!=0)
					{

						$this->session->set_flashdata('pur_msg','Purchase invoice inserted successfully!');
						redirect('new_purchase');
					}
					else
					{
						$this->session->set_flashdata('pur_msg_error','Purchase invoice can not be inserted!');
						redirect('new_purchase');
					}
					//end product details
					// $payment_method = $this->input->post('payment_method');
					// $cash = $this->input->post('cash');
					// $credit = $this->input->post('credit');
					// $img = $this->input->post('img');
					// $desc = $this->input->post('desc');
					//$res = $this->porducts->insert_product();
				}
				else
				{
					$this->session->set_flashdata('pur_msg','Purchase invoice can not be inserted!');
					redirect('new_purchase');
				}
			}
			else
			{
				$this->session->set_flashdata('pur_msg','Purchase invoice can not be inserted!');
				redirect('new_purchase');
			}
		}

		public function purchase_invoice_without_user()
		{

			$payment_method = $this->input->post('payment_method');
			$cash = $this->input->post('cash');
			$credit = $this->input->post('credit');
			$status = '';
			if(!empty($credit))
			{
				$status = 0;
			}
			else
			{
				$status = 1;
			}
			$img = $this->input->post('img');
			$desc = $this->input->post('desc');
			$invo_num = $this->input->post('invo_num');
			$invo_date = formated_date($this->input->post('invo_date'));
			$sale_ord_num = $this->input->post('sale_ord_num');
			$sale_ord_date = formated_date($this->input->post('sale_ord_date'));
			$deliv_note_num = $this->input->post('deliv_note_num');
			$cust_tck_num = $this->input->post('cust_tck_num');
			$login_user_id = $this->session->userdata('id');
			$vat_num = $this->input->post('vat_num');
			$user_id = $this->products->get_product_by_column('users','vat_no',$vat_num);
			$image ='';
			if (isset($_FILES['invoice_img']['name']) && $_FILES['invoice_img']['name'] != '') 
			{

				$config= array();
				$config['upload_path'] = FCPATH.'assets/images/purchase';
				$config['allowed_types'] = 'gif|jpg|png|mp4';
				$this->load->library('upload',$config);
				$this->upload->do_upload('invoice_img');
				$data = $this->upload->data();
				if($data) {
					$image = $data['file_name']; 
				} 
			} 
			else
			{
				$image = "";
			}
			$invo_data = array(


				// 'invoice_no' => $invo_date,
				// 'user_id' => $user_id->user_id,
				// 'invoice_date' => date('Y-m-d', strtotime($invo_date)),
				// 'sale_ord_no' => $sale_ord_num,
				// 'sale_ord_date' => date('Y-m-d', strtotime($sale_ord_date)),
				// 'deliv_note_no' => $deliv_note_num,
				// 'cust_tckt_no' => $cust_tck_num,
				// 'payment_method' => $payment_method,
				// 'cash' => $cash,
				// 'credit' => $credit,
				// 'img' => $image,
				// 'description' => $desc,
				// 'type' => 'Purchase',
				// 'created_by' => $login_user_id,
				// 'created_at' => date('Y-m-d H-i:s')


					'invoice_no' => $invo_num,
					'user_id' => $user_id->user_id,
					'invoice_date' => $invo_date,
					'sale_ord_no' => $sale_ord_num,
					'sale_ord_date' => $sale_ord_date,
					'deliv_note_no' => $deliv_note_num,
					'cust_tckt_no' => $cust_tck_num,
					'payment_method' => $payment_method,
					'cash' => $cash,
					'credit' => $credit,
					'img' => $image,
					'description' => $desc,
					'type' => 'Purchase',
					'invo_status' => $status,
					'created_by' => $login_user_id,
					'created_at' => date('Y-m-d H-i:s')

			);

			$invo_res = $this->products->insert_product('invoice',$invo_data);
			if($invo_res)
			{

				$products = $this->input->post('product');
				$vendor_id = $this->get_vendor_id();
				$i = 0;
				foreach ($products as $index =>$product) 
				{ 

					$product = isset($this->input->post('product')[$index])?$this->input->post('product')[$index]:'';
					$weight = isset($this->input->post('weight')[$index])?$this->input->post('weight')[$index]:'';
					$quantity = isset($this->input->post('quantity')[$index])?$this->input->post('quantity')[$index]:'';
					$rate = isset($this->input->post('rate')[$index])?$this->input->post('rate')[$index]:'';
					$discount = isset($this->input->post('discount')[$index])?$this->input->post('discount')[$index]:'';
					$total_exc_vat = isset($this->input->post('total_exc_vat')[$index])?$this->input->post('total_exc_vat')[$index]:'';
					$vat_perc = isset($this->input->post('vat_perc')[$index])?$this->input->post('vat_perc')[$index]:'';
					$vat_sar = isset($this->input->post('vat_sar')[$index])?$this->input->post('vat_sar')[$index]:'';
					$total_amt = isset($this->input->post('total_amt')[$index])?$this->input->post('total_amt')[$index]:'';
					
					$purchas_data = array(

						'vendor_id' => $vendor_id,
						'invo_id' => $invo_res,
						'product_id' => $product,
						'weight_unit' => $weight,
						'qty' => $quantity,
						'rate' => $rate,
						'discount' => $discount,
						'total_exec_vat' => $total_exc_vat,
						'vat_percent' => $vat_perc,
						'vat_sar' => $vat_sar,
						'total_amount' => $total_amt,
						'created_by' => $login_user_id,
						'created_at' => date('Y-m-d H-i:s')
					);
					$purchase_res = $this->products->insert_product('purchase',$purchas_data);
					$i++;
				}

				if($i!=0)
				{

					$this->session->set_flashdata('pur_msg','Purchase invoice inserted successfully!');
					redirect('new_purchase');
				}
				else
				{
					$this->session->set_flashdata('pur_msg_error','Purchase invoice can not be inserted!');
					redirect('new_purchase');
				}
			}
			else
			{
				$this->session->set_flashdata('pur_msg','Purchase invoice can not be inserted!');
				redirect('new_purchase');
			}
		}
		public function new_purchase_exe()
		{
			$vat_num = $this->input->post('vat_num');
			$user_vat_num = $this->products->get_product_by_column('users','vat_no',$vat_num);
			if($user_vat_num)
			{
				$this->purchase_invoice_without_user();
			}
			else
			{
				$this->purchase_invoice_with_user();
			}	
		}

		public function purchase_payable_view()
		{
			$payables = $this->db->select("* 
				,(select sum(vat_sar) FROM purchase where purchase.invo_id=invoice.invo_id) as vat_sar")
			->from('invoice')->where('invoice.type','Purchase')
			->join('users', 'users.user_id=invoice.user_id')->order_by('invoice.created_at','desc')->get()->result();
	// echo $payables[0]->invoice_no;exit;
	// echo "<pre>";
	// print_r($payables);exit;
			$this->load->view('purchase/purchase_payables_view', compact('payables'));


		}

		public function update_payable_exe()
		{
			$amt_cash = $this->input->post('amt_cash');
			$payable_amount = $this->input->post('payable_amount');
			$paid_amount = $this->input->post('paid_amount');
			$credit = $payable_amount-$paid_amount;
			$cash = $amt_cash+$paid_amount;
			$invo_id = $this->input->post('invo_id');
			$data = array(
				'cash' => $cash,
				'credit' => $credit,
			);
			$ajaxData = array(
				'invo_id' =>$invo_id,
				'cash' => $cash,
				'credit' => $credit,
			);
			$update_invo = $this->products->product_update('invoice',$data,'invo_id',$invo_id);
			if($update_invo)
			{
				echo json_encode($data);
				exit;
			}
			else
			{
				echo json_encode('');
				exit;
			}

		}
	}


