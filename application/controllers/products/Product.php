<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

function __construct(){
		parent::__construct();
		check_user_session();
		$this->load->model('Products_model', 'products');
		$this->load->model('User_model', 'user');
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('products/create');
	}
	public function all_pro()
	{
		$products = $this->products->select_products('product');
		// echo "<pre>";print_r($products);exit();
		$this->load->view('products/all', compact('products'));
	}
	public function edit_product()
	{
		$res =  $this->input->post('id');
		echo json_encode($res);
	}
	public function delete_product()
	{

		$id =  $this->input->post('del');
		$res = $this->products->delete_products('product','pro_id',$id);
		if($res)
		{
			$this->session->set_flashdata("update_msg","Record is deleted successfully.");
			redirect('all_products',compact($res));
		}
		else
		{
			$this->session->set_flashdata("update_msg","Something went wrong!");
			redirect('all_products',compact($res));
		}
	}


	public function get_product_ajax(){
		
		$response = array();
		$response['flag'] = false;
		$pro_id = $this->input->post('pro_id');
		$product = $this->products->get_product_by_column('product','pro_id',$pro_id);

		if(!empty($product)){
			$response['flag'] = true;
			$response['data'] = $product;
		}

		echo json_encode($response);
		exit;

	}
	public function update_product()
	{
		$pro_id = $this->input->post('update_id');
		$pro_name = $this->input->post('pro_name');
		$status = $this->input->post('state');
		$description = $this->input->post('description');
		$user = $this->session->userdata('name');
		$user_id = $this->user->select_user('user','name',$user);
		$data = array(
					'name' => $pro_name,
						'description' => $description,
						'status' => $status,
						'updated_by' => $user_id->id,
						'updated_at' => date('Y-m-d H-i:s')
			);

		$res = $this->products->product_update('product',$data,'pro_id',$pro_id);
		if($res)
		{
			$this->session->set_flashdata("update_msg","Record is updated successfully.");
			redirect('all_products',compact($res));
		}
		else
		{
			$this->session->set_flashdata("update_msg","Something went wrong!");
			redirect('all_products',compact($res));
		}
	}

	public function add_product_exe()
	{
		$this->form_validation->set_rules('pname','Product Name','required');
		$this->form_validation->set_rules('status','Status','required');
		$this->form_validation->set_rules('description','Description','required');
		if($this->form_validation->run()==false)
		{
			$this->session->set_flashdata("pro_msg","All fields are required!");
			redirect('add_product');

		}
		else
		{
		$pro_name = $this->input->post('pname');
		$status = $this->input->post('status');
		$description = $this->input->post('description');
		$user = $this->session->userdata('name');
		$user_id = $this->user->select_user('user','name',$user);
		//echo $this->db->last_query(); exit;
		$data = array(
						'name' => $pro_name,
						'description' => $description,
						'status' => $status,
						'created_by' => $user_id->id,
						'updated_by' => ' ',
						'created_at' => date('Y-m-d H-i:s'),
						'updated_at' => ' '

					);
		$res = $this->products->insert_product('product',$data);
			if(!empty($res))
			{
				$this->session->set_flashdata('pro_success','Product is added successfully.');
				redirect('add_product');
			}
			else
			{
				$this->session->set_flashdata('pro_msg','Something went wrong...!');
				redirect('add_product');
			}
		}
	}
}
