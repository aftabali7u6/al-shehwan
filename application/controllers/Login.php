<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

function __construct(){
		parent::__construct();
		$this->load->model('User_model','login');
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->form_validation->set_rules('name','Name','required');
		$this->form_validation->set_rules('password','Password','required');
		if($this->form_validation->run() == false)
		{
			$this->session->set_flashdata('login_msg','Something went wrong try again...!');
			redirect('login/page');
		}
		$name = $this->input->post('name');
		$password = $this->input->post('password');
		$result = $this->login->user_login($name,$password);
		if($result->name == $name && $result->password)
		{
			$this->session->set_userdata('name',$name);
			redirect('user/dashboard');
		}
		else
		{
			$this->session->set_flashdata('login_msg','Something went wrong try again...');
			redirect('login/page');
		}
	}
}
