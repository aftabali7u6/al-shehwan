<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['user/login'] = 'auth/login/index';
$route['login/page'] = 'welcome';
$route['user/dashboard'] = 'dashboard';
$route['user/logout'] = 'auth/logout/index';
$route['purchase_invoice'] = 'purchase/purchase/purchase_invoice_view';
$route['purchase_invoice_exe'] = 'purchase/purchase/purchase_invoice_exe';
$route['purchase_dashboard'] = 'purchase/purchase/purchase_dashboard';
$route['add_product'] = 'products/product/index';
$route['all_products'] = 'products/product/all_pro';
$route['add_product_exe'] = 'products/product/add_product_exe';
$route['edit_product'] = 'products/product/edit_product';
$route['delete_product'] = 'products/product/delete_product';
$route['get_single_product_ajax'] = 'products/product/get_product_ajax';
$route['update_product'] = 'products/product/update_product';
//Purchase routes
$route['new_purchase'] = 'purchase/purchase/index';
$route['purchase_list'] = 'purchase/purchase/purchase_list';
$route['view_purchase_invoice'] = 'purchase/purchase/view_purchase_invoice';
$route['new_purchase_exe'] = 'purchase/purchase/new_purchase_exe';
$route['delete_perchase'] = 'purchase/purchase/delete_perchase';
$route['edit_purchase'] = 'purchase/purchase/edit_purchase';
$route['update_purchase_exe'] = 'purchase/purchase/update_purchase_exe';
$route['update_sales_exe'] = 'sales/sales/update_sales_exe';
$route['purchase_payable'] = 'purchase/purchase/purchase_payable_view';
$route['update_payable_ajax'] = 'purchase/purchase/update_payable_exe';

$route['vendor_info'] = 'purchase/purchase/vendor_info';
$route['vendor_info_get'] = 'purchase/purchase/vendor_info_get';
//payable account
$route['add_payable'] = 'personal_account/Payable/create';
$route['store_payable'] = 'personal_account/Payable/store';
$route['payable_index'] = 'personal_account/Payable/index';
$route['get_personal_payables'] = 'personal_account/Payable/get_personal_payables';
$route['upadate_payables_exe'] = 'personal_account/Payable/upadate_payables_exe';

//receivable account
$route['receivable_index'] = 'personal_account/Receivable/index';
$route['store_receivable'] = 'personal_account/Receivable/store';
$route['update_receivable'] = 'personal_account/Receivable/update';
$route['get_personal_receivables'] = 'personal_account/Receivable/get_personal_receivables';
$route['personal_account_index'] = 'personal_account/Receivable/personal_account_index';
//employee
$route['employee_index'] = 'employee/Employees/index';
$route['add_employee'] = 'employee/Employees/create';
$route['store_employee'] = 'employee/Employees/store';
$route['update_employee'] = 'employee/Employees/update';
//sales
$route['new_sale_view'] = 'sales/sales/new_sale_view';
$route['new_sale_exe'] = 'sales/sales/new_sale_exe';
$route['sale_list'] = 'sales/sales/sale_register';
$route['edit_sales'] = 'sales/sales/edit_sales';
$route['sale_edit_exe'] = 'sales/sales/sale_edit_exe';
$route['sale_reveivable'] = 'sales/sales/sale_reveivable';
$route['update_receive_ajax'] = 'sales/sales/update_receive_exe';
$route['Advance_payments_view'] = 'sales/sales/Advance_payments_view';
$route['Advance_payments_exe'] = 'sales/sales/Advance_payments_exe';
$route['update_advance_amt'] = 'sales/sales/update_advance_amt';
$route['delete_advance_amt'] = 'sales/sales/delete_advance_amt';
$route['delete_sale'] = 'sales/Sales/delete_sale';
$route['saleBetween'] = 'sales/Sales/saleBetween';



//ajax client get 
$route['client_info'] = 'sales/sales/client_info';
$route['client_info_get'] = 'sales/sales/client_info_get';

///advance amount ajax

$route['adv_client_info_get'] = 'sales/sales/adv_client_info_get';



///////////// ajax calls
$route['betweenPurchase']='purchase/Purchase/purchaseBetween';


/// test invoice route
$route['testSaleInvoice']='invoice/invoice/testSaleInvoice';

///ajax image get



///// qoutation 
$route['qoutation_view']='qoutation/qoutation';
$route['new_quotation']='qoutation/qoutation/new_quotation';
$route['new_quotation_exe']='qoutation/qoutation/new_quotation_exe';


// expense
$route['expense'] = 'expense/Expense/index';
$route['new_expense_exe'] = 'expense/Expense/new_expense_exe';

/// expense controller functions
$route['cat_info_get']='expense/Expense/cat_info_get';
$route['cat_info']='expense/Expense/cat_info';




