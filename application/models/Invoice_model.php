<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Invoice_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->table_name = "invoice";
	}
	public function select($table,$id)
	{
		
		$this->db->where('invo_id',$id);
		$query = $this->db->get($table);
		$data = $query->row();
		return $data;
	}

	public function user_data($table,$id)
	{
		
		$this->db->where('user_id',$id);
		$query = $this->db->get($table);
		$user_data = $query->row();
		return $user_data;
	}

	public function purchase_data($table,$id)
	{
		
		$this->db->where('invo_id',$id);
		$query = $this->db->get($table);
		return $query->result_array();
	}

	public function product_data($table,$id)
	{
		
		$this->db->where('pro_id',$id);
		$query = $this->db->get($table);
		$product_data = $query->row();
		return $product_data;
	}
}