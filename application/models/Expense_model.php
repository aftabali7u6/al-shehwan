<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Expense_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->table_name = "expense";
	}
	public function select_join($table1,$table1_id,$table2,$table2_id)
	{	
		$this->db->join($table2, $table2.".".$table2_id."=".$table1.".".$table2_id);
		return $result = $this->db->order_by('exp_id','DESC')->get($table1)->result();
	}

	public function select($table)
	{
		return $this->db->order_by('exp_id','DESC')->get($table)->result();
	}

	public function insert($table,$data)
	{
		$this->db->insert($table,$data);
		return $this->db->insert_id();
	}
	public function update($table,$data,$column_name,$column_data) 
	{
		$this->db->where($column_name,$column_data);
		$this->db->update($table,$data);
		return $this->db->affected_rows();
	}
	public function update_by($column, $row_id, $data) 
	{
        $this->db->where($column, $row_id);
        return $this->db->update('expense', $data);
    }
    public function delete_by($column, $id) 
	{
        return $this->db->delete($this->table_name, array($column => $id));
    }

    function client_info($table,$column_name,$column_data)
	{
		$this->db->select('cat_id as id,cat_name as label,cat_name as value');
		 $this->db->like($column_name, $column_data,'after');
            return $this->db->get($table)->result();
	}
	function client_info_get($table,$column_name,$column_data)
	{
		$this->db->where($column_name,$column_data);
            return $this->db->get($table)->row();        
	}
}