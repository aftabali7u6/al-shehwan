<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Employees_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->table_name = "employees";
	}
	public function select($table)
	{
		return $this->db->order_by('employee_id','DESC')->get($table)->result();
	}
	public function insert($table,$data)
	{
		$this->db->insert($table,$data);
		return $this->db->insert_id();
	}
	public function update($table,$data,$column_name,$column_data) 
	{
		$this->db->where($column_name,$column_data);
		$this->db->update($table,$data);
		return $this->db->affected_rows();
	}
	public function update_by($column, $row_id, $data) 
	{
        $this->db->where($column, $row_id);
        return $this->db->update('employees', $data);
    }
    public function delete_by($column, $id) 
	{
        return $this->db->delete($this->table_name, array($column => $id));
    }
}