<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Payable_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->table_name = "payables";
	}

	
	public function insert_product($table, $data) {
		$this->db->insert($table,$data);
		return $this->db->insert_id();
	}
	public function select_products($table) {
		$this->db->order_by("payable_id", "DESC");
		$this->db->where('status',0);
		$query = $this->db->get($table);
		return $query->result();
	}
	public function get_product_by_column($table,$column,$value) {
		return $this->db->get_where($table,array($column=>$value))->row();
	}
	public function product_update($table,$data,$column_name,$column_data) {
		$this->db->where($column_name,$column_data);
		$this->db->update($table,$data);
		return $this->db->affected_rows();
	}
	public function update_by($column, $row_id, $data) {
		$this->db->where($column, $row_id);
		return $this->db->update('payables', $data);
	}
	public function select_payable_amount($table){
		$this->db->select_sum('amount');
	    $query = $this->db->get($table);
    	return $query->result_array();
	}
	
}	