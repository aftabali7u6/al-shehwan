<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Sales_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->table_name = "Sales";
	}
	public function insert_sale($table, $data) {
		$this->db->insert($table,$data);
		return $this->db->insert_id();
	}
	public function select_sale($table) {
		return $this->db->order_by('sale_id','DESC')->get($table)->result();
	}
	public function get_sale_by_column($table,$column,$value) {
		return $this->db->get_where($table,array($column=>$value))->row();
	}
	public function sale_update($table,$data,$column_name,$column_data) {
		$this->db->where($column_name,$column_data);
		$this->db->update($table,$data);
		return $this->db->affected_rows();
	}
	public function update_by($column, $row_id, $data) {
		$this->db->where($column, $row_id);
		return $this->db->update('Sales', $data);
	}
	public function delete_by($column, $id) 
	{
        return $this->db->delete($this->table_name, array($column => $id));
    }


    public function show_sale()
    {
    	// $this->db->
     return	$this->db->select("*,(select sum(total_amount) FROM sales where sales.invo_id=invoice.invo_id) as total_amount
     	,(select sum(qty) FROM sales where sales.invo_id=invoice.invo_id) as qty
		,(select sum(total_exec_vat) FROM sales where sales.invo_id=invoice.invo_id) as total_exec_vat 
		,(select sum(vat_sar) FROM sales where sales.invo_id=invoice.invo_id) as vat_sar")
		->from('invoice')->where('invoice.type','Sale')
		->join('users', 'users.user_id=invoice.user_id')->where('users.type','Client')->order_by('invoice.created_at','desc')->get()->result();
		// invo_status
    }

    function delete_advance($table,$column_name,$column_data)
	{
		$this->db->where($column_name,$column_data);
		$this->db->delete($table);
		return $this->db->affected_rows();
	}
    
    //sales ajax cliet get 
	function client_info($table,$column_name,$column_data)
	{
		$this->db->select('vat_no as id,vat_no as label,vat_no as value');
		 $this->db->like($column_name, $column_data,'after');
		 $this->db->where('type','client');
            return $this->db->get($table)->result();
	}
	function client_info_get($table,$column_name,$column_data)
	{
		$this->db->where($column_name,$column_data);
            return $this->db->get($table)->row();        
	}
	function select_invo_number()
	{
		$this->db->select('invo_id');
		$this->db->order_by('invo_id', 'DESC');
		$this->db->limit(1);
		return $this->db->from('invoice')->get()->row();
	}
	public function insert_product($table, $data) {
		$this->db->insert($table,$data);
		return $this->db->insert_id();
	}
	function get_product_by_column($table,$column,$value)
	{
		return $this->db->get_where($table,array($column=>$value))->row();
	}
	function select_products($table)
	{
		return $this->db->get($table)->result();
	}
	function product_update($table,$data,$column_name,$column_data)
	{
		$this->db->where($column_name,$column_data);
		$this->db->update($table,$data);
		return $this->db->affected_rows();
	}

// function client_info_get($table,$column_name,$column_data)
// 	{
// 		$this->db->where($column_name,$column_data);
//             return $this->db->get($table)->row();
             
// 	}
// 	function get_product_by_column($table,$column,$value)
// {
// 	return $this->db->get_where($table,array($column=>$value))->row();
// }
	
}	