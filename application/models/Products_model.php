<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Products_model extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	
function insert_product($table, $data){

		$this->db->insert($table,$data);
		return $this->db->insert_id();
	}
function select_products($table)
	{
		return $this->db->get($table)->result();
	}
	

function get_product_by_column($table,$column,$value)
{
	return $this->db->get_where($table,array($column=>$value))->row();
}

function product_update($table,$data,$column_name,$column_data)
	{
		$this->db->where($column_name,$column_data);
		$this->db->update($table,$data);
		return $this->db->affected_rows();
	}

	function delete_products($table,$column_name,$column_data)
	{
		$this->db->where($column_name,$column_data);
		$this->db->delete($table);
		return $this->db->affected_rows();
	}

	function select_all_puchases($table,$column_name,$column_data)
	{
		$invoices=$this->db->select('*')
		->where($column_name,$column_data)
		->from($table)
		->get()->result();
		return $invoices;
             
	}
	//ajax vendor get
	function vendor_info($table,$column_name,$column_data)
	{
		$this->db->select('vat_no as id,vat_no as label,vat_no as value');
		 $this->db->like($column_name, $column_data,'after');
		 $this->db->where('type','vendor');
            return $this->db->get($table)->result();
	}
	function vendor_info_get($table,$column_name,$column_data)
	{
		$this->db->where($column_name,$column_data);
            return $this->db->get($table)->row();
             
	}

	
}