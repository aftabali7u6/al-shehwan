
<html>
<head>
	<style type="text/css">
		/* reset */

		*
		{
			border: 0;
			box-sizing: content-box;
			color: inherit;
			font-family: inherit;
			font-size: inherit;
			font-style: inherit;
			font-weight: inherit;
			line-height: inherit;
			list-style: none;
			margin: 0;
			padding: 0;
			text-decoration: none;
			vertical-align: top;
		}

		/* content editable */

		*[contenteditable] { border-radius: 0.25em; min-width: 1em; outline: 0; }

		*[contenteditable] { cursor: pointer; }

		*[contenteditable]:hover, *[contenteditable]:focus, td:hover *[contenteditable], td:focus *[contenteditable], img.hover { background: #DEF; box-shadow: 0 0 1em 0.5em #DEF; }

		span[contenteditable] { display: inline-block; }

		/* heading */

		h1 { font: bold 100% sans-serif; letter-spacing: 0.5em; text-align: center; text-transform: uppercase; }

		/* table */

		table { font-size: 75%; table-layout: fixed; width: 100%; }
		table { border-collapse: separate; border-spacing: 2px; }
		th, td { border-width: 1px; padding: 0.5em; position: relative; text-align: left; }
		th, td { border-radius: 0.25em; border-style: solid; }
		th { background: #EEE; border-color: #BBB; }
		td { border-color: #DDD; }

		/* page */

		html { font: 16px/1 'Open Sans', sans-serif; overflow: auto; padding: 0.5in; }
		html { background: #999; cursor: default; }

		body { box-sizing: border-box; height: 11in; margin: 0 auto; overflow: hidden; padding: 0.5in; width: 8.5in; }
		body { background: #FFF; border-radius: 1px; box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5); }

		/* header */

		header { margin: 0 0 3em; }
		header:after { clear: both; content: ""; display: table; }

		header h1 { background: #000; border-radius: 0.25em; color: #FFF; margin: 0 0 1em; padding: 0.5em 0; }
		header address { float: left; font-size: 75%; font-style: normal; line-height: 1.25; margin: 0 1em 1em 0; }
		header address p { margin: 0 0 0.25em; }
		header span, header img { display: block; float: right; }
		header span { margin: 0 0 1em 1em; max-height: 25%; max-width: 60%; position: relative; }
		header img { max-height: 100%; max-width: 100%; }
		header input { cursor: pointer; -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)"; height: 100%; left: 0; opacity: 0; position: absolute; top: 0; width: 100%; }

		/* article */

		article, article address, table.meta, table.inventory { margin: 0 0 3em; }
		article:after { clear: both; content: ""; display: table; }
		article h1 { clip: rect(0 0 0 0); position: absolute; }

		article address { float: left; font-size: 125%; font-weight: bold; }

		/* table meta & balance */

		table.meta, table.balance { float: right; width: 36%; }
		table.meta:after, table.balance:after { clear: both; content: ""; display: table; }

		/* table meta */

		table.meta th { width: 40%; }
		table.meta td { width: 60%; }

		/* table items */

		table.inventory { clear: both; width: 100%; }
		table.inventory th { font-weight: bold; text-align: center; }

		table.inventory td:nth-child(1) { width: 26%; }
		table.inventory td:nth-child(2) { width: 38%; }
		table.inventory td:nth-child(3) { text-align: right; width: 12%; }
		table.inventory td:nth-child(4) { text-align: right; width: 12%; }
		table.inventory td:nth-child(5) { text-align: right; width: 12%; }

		/* table balance */

		table.balance th, table.balance td { width: 50%; }
		table.balance td { text-align: right; }

		/* aside */

		aside h1 { border: none; border-width: 0 0 1px; margin: 0 0 1em; }
		aside h1 { border-color: #999; border-bottom-style: solid; }

		/* javascript */

		.add, .cut
		{
			border-width: 1px;
			display: block;
			font-size: .8rem;
			padding: 0.25em 0.5em;	
			float: left;
			text-align: center;
			width: 0.6em;
		}

		.add, .cut
		{
			background: #9AF;
			box-shadow: 0 1px 2px rgba(0,0,0,0.2);
			background-image: -moz-linear-gradient(#00ADEE 5%, #0078A5 100%);
			background-image: -webkit-linear-gradient(#00ADEE 5%, #0078A5 100%);
			border-radius: 0.5em;
			border-color: #0076A3;
			color: #FFF;
			cursor: pointer;
			font-weight: bold;
			text-shadow: 0 -1px 2px rgba(0,0,0,0.333);
		}

		.add { margin: -2.5em 0 0; }

		.add:hover { background: #00ADEE; }

		.cut { opacity: 0; position: absolute; top: 0; left: -1.5em; }
		.cut { -webkit-transition: opacity 100ms ease-in; }

		tr:hover .cut { opacity: 1; }

		@media print {
			* { -webkit-print-color-adjust: exact; }
			html { background: none; padding: 0; }
			body { box-shadow: none; margin: 0; }
			span:empty { display: none; }
			.add, .cut { display: none; }
		}
		table.meta, table.balance {
    float: right;
    width: 36%;
   
}
	</style>
	<meta charset="utf-8">
	<title>Invoice</title>
	<link rel="stylesheet" href="style.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<script src="script.js"></script>
	<script src="invoice.js"></script>
	

</head>
<body id="print_specific">
	<header>
		<h1>Purchase Invoice</h1>
		<address>
			<p style="font-size: 25px">Al-shahwan<br>Trading Est.</p>
		</address>
		
	</header>
	<article>
		<h1>Recipient</h1>
		<table class="meta table bordered">
			<tr>
				<th><span >Invoice #</span></th>
				<td><span ><?php echo $invoice_data->invoice_no?></span></td>
			</tr>
			<tr>
				<th><span >Invoice Date #</span></th>
				<td><span ><?php echo formated_date($invoice_data->invoice_date,'d-m-Y')?></span></td>
			</tr>
			<tr>
				<th><span >Sale order No</span></th>
				<td><span ><?php echo $invoice_data->sale_ord_no?></span></td>
			</tr>
			<tr>
				<th><span >Sale order Date</span></th>
				<td><span ><?php echo formated_date($invoice_data->sale_ord_date,'d-m-Y')?></span></td>
			</tr>
			<tr>
				<th><span >Delivery Not No</span></th>
				<td><span ><?php echo $invoice_data->deliv_note_no?></span></td>
			</tr>
			<tr>
				<th><span >Cust Ticket No</span></th>
				<td><span><?php echo $invoice_data->cust_tckt_no?></span></td>
			</tr>
			<tr>
				<th><span >Cust Ticket Date</span></th>
				<td><span id="prefix" ></span><span></span></td>
			</tr>
		</table>
		<table class="meta new" style="float: left;">
			<tr>
				<th><span >Name</span></th>
				<td><span ><?php echo $user_data->company_name?></span></td>
			</tr>
			<tr>
				<th><span >Contact</span></th>
		<td><span><?php echo $user_data->phone?></span></td>
			</tr>
			<tr>
				<th><span >Address</span></th>
				<td><span ><?php echo $user_data->address?></span></td>
			</tr>
			<tr>
				<th><span >VAT Registeration</span></th>
		<td><span><?php echo $user_data->vat_no?></span></td>
			</tr>
			
		</table>
		<table class="inventory">
			<thead>
				<tr>
					<th><span >Item</span></th>
					<th><span >Goods Description</span></th>
					<th><span >Quantity in MT</span></th>
					<th><span >Rate</span></th>
					<th><span >Discount</span></th>
					<th><span >Total Exclusive VAT Amount</span></th>
					<th><span >VAT %</span></th>
					<th><span >VAT SAR</span></th>
					<th><span >Total Amount</span></th>
				</tr>
			</thead>
			<tbody>
				<?php $total=0?>
				<?php foreach ($purchase_data as $index=> $value) 
				{
					$data=product_data($value['product_id']	);
                       $total=$value['total_amount']+$total;
					?>

					<tr>
						<td><span ><?php echo ++$index?></span></td>
						<td><span ><?php echo $data->description?></span></td>
						<td><span ><?php echo $value['qty']?></span></td>
						<td><span ><?php echo $value['rate']?></span></td>
						<td><span ><?php echo $value['discount']?></span></td>
						<td><span ><?php echo round($value['total_exec_vat'],2)?></span></td>
						<td><span > <?php echo $value['vat_percent']?></span></td>
						<td><span ><?php echo round($value['vat_sar'],2)?></span></td>
						<td><span><?php echo round($value['total_amount'],2)?></span></td>
					</tr>
					<?php
				} 
				?>
				<tr>
						<td>3</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>4</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>5</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>

			</tbody>
		</table>
		<table class="balance">
			<tr>
				<th><span >Total</span></th>
				<td><span><?php echo round($total,2)?></span></td>
			</tr>
			<tr>
				<th><span >Amount Paid</span></th>
				<td><span ><?php echo round($invoice_data->cash,2)?></span></td>
			</tr>
			<tr>
				<th><span >Balance Due</span></th>
				<td><span><?php echo round($total-$invoice_data->cash,2)?></span></td>
			</tr>
		</table>
	</article>
	<aside>

		<h1><span >Additional Notes</span></h1>
		<div >
			<p>VAT 5% is charged on each product purchase.</p>
		</div>
	</aside>
	<br><br><br><br>

</body>
<div class="reset-button">
			<input type="submit" name="submit" value="Click for print" class="btn btn-primary" id="printMe" style="font-size: 18px;background-color: green;border-radius: 6px;min-height: 32px;color: white;width: 132px">
		</div>
</html>

<script type="text/javascript">
	$('#printMe').click(function(){
		$("#printMe").css("display", "none");
		window.print();
		$("#printMe").show();
	
	});
</script>