 <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-group"></i>
               </div>
               <div class="header-title">
                  <h1>Advance Amount Transactions Page</h1>
                  <small>Advance List</small>
                   <?php if($this->session->flashdata('adv_msg')): ?>
              <span id="adv_msg" style="color: green; text-align: right; float: right;color: #004085;background-color: #cce5ff;border-color: #b8daff;"><?php echo $this->session->flashdata('adv_msg');?></span>
           <?php endif;?>
           <?php if($this->session->flashdata('adv_msg_error')): ?>
              <span id="adv_msg_error" style="color: green; text-align: right; float: right;color: #004085;background-color: #cce5ff;border-color: #b8daff;"><?php echo $this->session->flashdata('adv_msg_error');?></span>
           <?php endif;?>
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                          <div class="btn-group buttonexport">
                              <a href="#">
                                 <h4>Advance Details</h4>
                              </a>
                           </div> 
                        </div>
                        <div class="panel-body">
                        <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->
                           <div class="btn-group">
                              <div class="buttonexport" id="buttonlist"> 
                                 <a class="btn btn-add" href="#" data-toggle="modal" data-target="#addcustom"> <i class="fa fa-plus"></i> Add new Advance
                                 </a>  
                              </div>
                             
                           </div>
                           <!-- ./Plugin content:powerpoint,txt,pdf,png,word,xl -->
                           <div class="table-responsive">
                              <table id="dataTableExample1" class="table table-bordered table-striped table-hover">
                                 <thead>
                                    <tr>
                                       <th>Date</th>
                                       <th>Company Name</th>
                                       <th>VAT No.</th>
                                       <th>Advance Amount</th>
                                       <th>Advance Description</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                     <?php if(isset($advance_amounts)):
                                        foreach ($advance_amounts as $index => $adv_amt):?>
                                    <tr>
                                    <tr class="receive<?php echo $adv_amt->adv_id;?> ">
                                       <td class="adv_date"><?php echo formated_date($adv_amt->advance_date,'d-m-Y');?></td>
                                       <td><?php echo $adv_amt->company_name;?></td>
                                       <td><?php echo $adv_amt->vat_no;?></td>
                                       <td class="adv_amt"><?php echo round($adv_amt->advance_amount,2);?></td>
                                       <td class="adv_descrp"><?php echo $adv_amt->description;?></td>
                                       <td>
                                          <button type="button" class="btn btn-add btn-sm" data-toggle="modal" data-target="#customer1" onclick="adv_adit(<?php echo $adv_amt->adv_id; ?>)"><i class="fa fa-pencil"></i></button>
                                          <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#customer2" onclick="adv_delete(<?php echo $adv_amt->adv_id; ?>)"><i class="fa fa-trash-o"></i> </button>
                                       </td>
                                    </tr>
                                     <? endforeach; ?>
                                     <? endif;?>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- ADD Modal1 -->
               <div class="modal fade" id="addcustom" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog">
                     <div class="modal-content">
                        <div class="modal-header modal-header-primary">
                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                           <h3><i class="fa fa-user m-r-5"></i> Client Advance Amount Transaction</h3>
                        </div>
                        <div class="modal-body">
                           <div class="row">
                              <div class="col-md-12">
                                    <?php echo form_open("Advance_payments_exe", array('name'=>'advance_amount',
                                          'id'=>'advance_amount','class' => 'form-horizontal'));?>
                                    <fieldset>
                                       <!-- Text input-->
                                    <div class="form-group col-md-6">
                                    <label>VAT Number</label>
                                    <input type="number" id="vat_num" name="vat_num" class="form-control" autocomplete="on" placeholder="Enter VAT Number" required >
                                 </div>
                                 <div class="form-group col-md-6">
                                    <label>Company Name</label>
                                    <input type="text" align="right" class="form-control" required name="comp_name" id="comp_name" style="text-align: right;"
                                    placeholder=" ...الرجاء إدخال اسم الشركة " required>
                                 </div>
                                 <div class="form-group col-md-6">
                                    <label>Contact Number</label>
                                    <input type="number" class="form-control" id="contact" name="contact" placeholder="Contact Number" >
                                 </div>
                                 <div class="form-group col-md-6">
                                    <label>Address</label>
                                    <input type="text" class="form-control" id="address" name="address" placeholder="Enter Address">

                                 </div>
                                 <div class="col-md-12"><hr></div>
                                       <div class="col-md-6 form-group">
                                          <label class="control-label">Advance Amount:</label>
                                          <input type="number" step="0.0001" placeholder="Enter Amount" class="form-control" required name="adv_amt" required id="adv_amt" >
                                       </div>
                                       <div class="col-md-6 form-group">
                                          <label class="control-label">Date:</label>
                                          <input  type="text" required name="invo_date" id="invo_date" required class="form-control adv_data" placeholder="Enter Date...">
                                       </div>
                                        <div class="col-md-6 form-group">
                                          <label class="control-label">Description:</label>
                                          <textarea placeholder="details" class="form-control" name="advance_desc" id="advance_desc" rows="4"></textarea>
                                       </div>
                                       <div class="col-md-12 form-group user-form-group">
                                          <div class="pull-right">
                                             <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                                             <button type="submit" class="btn btn-add btn-sm">Save</button>
                                          </div>
                                       </div>
                                    </fieldset>
                                 <?php echo form_close();?>
                              </div>
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                        </div>
                     </div>
                     <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
               </div>
               <!-- /.modal -->
               <!-- Modal -->    
               <!-- customer Modal1 -->
               <div class="modal fade" id="customer1" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog">
                     <div class="modal-content">
                        <div class="modal-header modal-header-primary">
                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                           <h3><i class="fa fa-user m-r-5"></i> Update Advance Transaction</h3>
                        </div>
                        <div class="modal-body">
                           <div class="row">
                              <div class="col-md-12">
                                <!--  <form class="form-horizontal"> -->
                                      <?php echo form_open_multipart("update_advance_amt", array('name'=>'update_advance_amount',
                                          'id'=>'update_advance_amount','class' => 'form-horizontal'));?>
                                    <fieldset>
                                       <!-- Text input-->
                                       <input type="hidden" name="advance_id" id="advance_id">
                                       <div class="col-md-6 form-group">
                                          <label class="control-label">Advance Amount:</label>
                                          <input type="number" step="0.0001" required id="update_amt" name="update_amt" placeholder="Customer Name" class="form-control">
                                       </div>
                                       <div class="col-md-6 form-group">
                                          <label class="control-label">Date:</label>
                                          <input  type="text" name="update_date" id="update_date" required class="form-control adv_data" placeholder="Enter Date...">
                                       </div>
                                       <!-- Text input-->
                                       <div class="col-md-6 form-group">
                                          <label class="control-label">Description:</label>
                                          <textarea placeholder="details" class="form-control" name="adv_desc" id="adv_desc" rows="4"></textarea>
                                       </div>
                                       <div class="col-md-12 form-group user-form-group">
                                          <div class="pull-right">
                                             <button type="button" class="btn btn-danger btn-sm">Cancel</button>
                                             <button type="submit" class="btn btn-add btn-sm">Save</button>
                                          </div>
                                       </div>
                                    </fieldset>
                                 <!-- </form> -->
                                 <?php echo form_close(); ?>
                              </div>
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                        </div>
                     </div>
                     <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
               </div>
               <!-- /.modal -->
               <!-- Modal -->    
               <!-- Customer Modal2 -->
               <div class="modal fade" id="customer2" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog">
                     <div class="modal-content">
                        <div class="modal-header modal-header-primary">
                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                           <h3><i class="fa fa-user m-r-5"></i> Delete Advance Transaction</h3>
                        </div>
                        <div class="modal-body">
                           <div class="row">
                              <div class="col-md-12">
                                 <?php echo form_open_multipart("delete_advance_amt", array('name'=>'delete_advance_amount',
                                          'id'=>'delete_advance_amount','class' => 'form-horizontal'));?>
                                    <fieldset>
                                       <div class="col-md-12 form-group user-form-group">
                                          <label class="control-label">Delete Advance</label>
                                          <input type="hidden" id="del_id" name="del_id">
                                          <div class="pull-right">
                                             <button type="button" class="btn btn-danger btn-sm">NO</button>
                                             <button type="submit" class="btn btn-add btn-sm">YES</button>
                                          </div>
                                       </div>
                                    </fieldset>
                                 <?php echo form_close(); ?>
                              </div>
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                        </div>
                     </div>
                     <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
               </div>
               <!-- /.modal -->
            </section>
            <!-- /.content -->
         </div>

         <style type="text/css">
            
            .ui-autocomplete{
               z-index: 99999999;
            }
         </style>
         <script type="text/javascript">
               function vat_fun(vat_no)
      {
         $.ajax({
            type: 'POST',
            url: '<?php echo base_url('adv_client_info_get'); ?>',
            data: {vat_no:vat_no},
            dataType: 'json',
            success: function(response){

              $('#comp_name').val(response.company_name);
              $('#contact').val(response.phone);
              $('#address').val(response.address);

           }
        });
         
      }
      $(function(){
        $( "#vat_num" ).autocomplete({
         source: "<? echo base_url('client_info');?>",
         minLength: 2,
         select: function( event, ui ) {
            vat_fun(ui.item.id);
        // console.log( "Selected: " + ui.item.value + " aka " + ui.item.id );

     }
  });
     });

      $(document).ready(function(){
    setTimeout(function(){ $("#adv_msg").fadeOut(); }, 4000);
    setTimeout(function(){ $("#adv_msg_error").fadeOut(); }, 4000);
});
   function adv_adit(adv_id)
   {
     var advance_dte=$('.receive'+adv_id).find('.adv_date').first().html();
  var advance_amnt=$('.receive'+adv_id).find('.adv_amt').first().html();
  var advance_descrption=$('.receive'+adv_id).find('.adv_descrp').first().html();

      $("#advance_id").val(adv_id);
      $("#update_amt").val(advance_amnt);
      $("#update_date").val(advance_dte);
      $("#adv_desc").val(advance_descrption);
   }

   function adv_delete(adv_id)
   {
      $("#del_id").val(adv_id);

   }
 $('#adv_data').datetimepicker({
 timepicker:false,
 format:'d-m-Y'
});
</script>