 <style type="text/css">
 .underline{text-decoration: underline;}
</style>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-users"></i>
      </div>
      <div class="header-title">
         <h1>New Sale Invoice</h1>
         <small>Sale list</small>
          <?php if($this->session->flashdata('pur_msg')): ?>
              <span id="pur_msg" style="color: green; text-align: right; float: right;color: #004085;background-color: #cce5ff;border-color: #b8daff;"><?php echo $this->session->flashdata('pur_msg');?></span>
           <?php endif;?>
           <?php if($this->session->flashdata('pur_msg_error')): ?>
              <span id="pur_msg_error" style="color: green; text-align: right; float: right;color: #004085;background-color: #cce5ff;border-color: #b8daff;"><?php echo $this->session->flashdata('pur_msg_error');?></span>
           <?php endif;?>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-12">
            <div class="panel panel-bd lobidrag">
               <div class="panel-heading">
                  <div class="btn-group" id="buttonlist"> 
                     <a class="btn btn-add " href="<? echo base_url('sale_list');?>"> 
                        <i class="fa fa-list"></i>  Sale Invoice List </a>  
                  </div>
               </div>
               <div class="panel-body">
                  <!-- Tab panels -->
                  <div class="tab-content col-sm-12">
                     <div class="tab-pane fade in active" id="tab1">
                        <form action="javascript:;" method="POST" name="sale_list" id="sale_list" onsubmit="verify_amt()" >
                        <div class="panel-body">

                           <div class="table-responsive">
                              <table class="table table-bordered table-striped table-hover">
                                 <div class=" col-md-12">
                                    <h2 class="underline">Client Information</h2>
                                 </div>
                                 <div class="form-group col-md-3">
                                    <label>VAT Number</label>
                                    <input type="number" id="vat_num" name="vat_num" class="form-control" placeholder="Enter VAT Number" required>
                                 </div>
                                 <div class="form-group col-md-3">
                                    <label>Company Name</label>
                                    <input type="text" align="right" class="form-control" required name="comp_name" id="comp_name" style="text-align: right;"
                                    placeholder=" ...الرجاء إدخال اسم الشركة " required>
                                 </div>
                                 <div class="form-group col-md-3">
                                    <label>Contact Number</label>
                                    <input type="number" class="form-control" id="contact" name="contact" placeholder="Contact Number">
                                 </div>
                                 <div class="form-group col-md-3">
                                    <label>Address</label>
                                    <input type="text" class="form-control" id="address" name="address" placeholder="Enter Address">

                                 </div>
                                 
                              </table>
                           </div>

                        </div>

                        <div class="panel-body">

                           <div class="table-responsive">
                              <table class="table table-bordered table-striped table-hover">
                                 <div class=" col-md-12">
                                    <h2 class="underline">Invoice Details</h2>
                                 </div>
                                 <div class="form-group col-md-5">
                                    <label>Invoice Number</label>
                                    <input type="number" class="form-control" value="<?php echo '00'.$invoice_number; ?>" required name="invo_num" id="invo_num" placeholder="Invoice Number">
                                 </div>
                                 <div class="form-group col-md-5">
                                    <label>Invoice Date</label>
                                    <input  type="text" name="invo_date" id="invo_date" required class="form-control" placeholder="Enter Date...">
                                 </div>
                                 <!-- <div class="form-group col-md-2">
                                    <label>Sales Order Number</label>
                                    <input type="text" class="form-control" name="sale_ord_num" id="sale_ord_num" placeholder="Sales Order Number" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
                                 </div>
                                 <div class="form-group col-md-2">
                                    <label>Sales Order Date</label>
                                    <input  type="text" class="form-control datetimepicker" name="sale_ord_date" id="sale_ord_date" placeholder="Enter Date...">
                                    
                                 </div>
                                 <div class="form-group col-md-2">
                                    <label>Delivery Note No.</label>
                                    <input type="text" class="form-control" name="deliv_note_num" id="deliv_note_num" placeholder="Enter Note Number" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
                                 </div>
                                 <div class="form-group col-md-2">
                                    <label>Cust. Ticket Noumber</label>
                                    <input type="text" class="form-control" name="cust_tck_num" id="cust_tck_num" placeholder="Cust. Ticket No." oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
                                 </div> -->
                                 
                              </table>
                           </div>

                        </div>
                        <div class="panel-body">

                           <div class="table-responsive">
                              <table class="table table-bordered table-striped table-hover">
                                 <div class=" col-md-12">
                                    <h2 class="underline">Product Details</h2>
                                 </div>
                                 <div  class="row count_form" id="clone_row" >
                                    <div class="form-group col-md-3">
                                       <label>Select Product</label>
                                       <select class="form-control" name="product[]" id="product" required>
                                          <option disabled selected>Please Select Product</option>
                                          <? foreach ($product_list as $product) {?>
                                             <option value="<?php echo $product->pro_id; ?>"><? echo $product->name;?></option>
                                          <? } ?>
                                       </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                       <label>Select Weight Unit</label>
                                       <select class="form-control" name="weight[]" id="weight" required>
                                          <option disabled selected>Please Select Product</option>
                                          <option value="1">MT</option>
                                          <option value="2">KG</option>
                                          <option value="3">TON</option>
                                       </select>
                                    </div>
                                    <div class="form-group col-md-2">
                                       <label>Quantity</label>
                                       <input type="number" step="0.0001" class="form-control quantity" name="quantity[]" id="quantity" required placeholder="Enter Quantity">
                                    </div>
                                    <div class="form-group col-md-2">
                                       <label>Rate</label>
                                       <input type="number" step="0.0001" class="form-control rate" name="rate[]" id="rate" placeholder="Enter Rate" required>
                                    </div>
                                    <div class="form-group col-md-2">
                                       <label>Discount</label>
                                       <input type="number" step="0.0001" class="form-control discount" name="discount[]" id="discount" placeholder="Enter Discount">
                                    </div>
                                    
                                    <div class="form-group col-md-3">
                                       <label>Total Exclusive VAT Amount</label>
                                       <input type="text" class="form-control total_exc_vat" name="total_exc_vat[]" id="total_exc_vat" readonly="">
                                    </div>
                                    <div class="form-group col-md-3">
                                       <label>VAT %</label>
                                       <select class="form-control vat_perc" required name="vat_perc[]" id="vat_perc">
                                          <option disabled selected>Select VAT %</option>
                                          <option value="5">5</option>
                                          <option value="5.5">5.5</option>
                                          <option value="6">6</option>
                                          <option value="7">7</option>
                                       </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                       <label>VAT SAR Amount</label>
                                       <input type="text" readonly="" class="form-control vat_sar" name="vat_sar[]" id="vat_sar">
                                    </div>
                                    <div class="form-group col-md-3">
                                       <label>Total Amount</label>
                                       <input type="text" class="form-control total_amt" name="total_amt[]"  readonly="">
                                    </div>
                                    <div id="append_total" class="append_total"></div>
                                    <div class="form-group col-md-3">
                                       <div type="button" id="add_row" class="btn btn-warning"><span class="glyphicon glyphicon-plus"></span></div>
                                       <div type="submit" id="remove_row" class="btn btn-danger"><span class="glyphicon glyphicon-minus"></span></div>
                                    </div>
                                 </div>
                                 <div id="append_data" class="append_data"></div>
                                 <div class="row display_row"  style="display: none;">
                                    <div class="form-group col-md-9"></div>
                                  <div class="form-group col-md-3" >
                                     <label>Grand Total</label>
                                       <input type="number" step="0.0001" class="form-control grand_total" name="grand_total" id="grand_total" value="0" readonly>
                                  </div>
                                  </div>
                              </table>
                           </div>
                        </div>
                         <div class="panel-body" id="advance_panel" style="display: none;">

                           <div class="table-responsive">
                              <table class="table table-bordered table-striped table-hover">
                                 <div class=" col-md-12">
                                    <h2 class="underline">Advance Amount</h2>
                                 </div>
                                 <div class="form-group col-md-6">
                                    <label id="lbl_cash">Advance Available</label>
                                    <input type="text" class="form-control advanceAvail" readonly name="adv_avl" id="adv_avl">
                                 </div>
                                 <div class="form-group col-md-6">
                                    <label id="lbl_credit">Remaining Amount</label>
                                    <input type="text" class="form-control" readonly name="rem_adv" id="rem_adv">
                                 </div>
                              </table>
                           </div>
                        </div>

                        <div class="panel-body paymentStatus">

                           <div class="table-responsive">
                              <table class="table table-bordered table-striped table-hover">
                                 <div class=" col-md-12">
                                    <h2 class="underline">Payment Details</h2>
                                 </div>
                                 <div class="form-group col-md-4">
                                    <label>Payment Method</label>
                                    <select class="form-control" required name="payment_method" id="payment_method">
                                       <option disabled selected>Select Payment Method</option>
                                       <option value="cash">Cash</option>
                                       <option value="credit">Credit</option>
                                       <option value="both">Both</option>
                                    </select>
                                 </div>
                                 <div class="form-group col-md-4">
                                    <label id="lbl_cash">Cash</label>
                                    <input type="number" step="0.0001" class="form-control" name="cash" id="cash" placeholder="Enter cash">
                                 </div>
                                 <div class="form-group col-md-4">
                                    <label id="lbl_credit">Credit</label>
                                    <input type="text" class="form-control" name="credit" id="credit" placeholder="Enter credit">
                                 </div>
                              </table>
                           </div>
                        </div>

                         
                        <div class="panel-body">

                           <div class="table-responsive">
                              <table class="table table-bordered table-striped table-hover">
                                 <div class=" col-md-12">
                                    <h2></h2>
                                 </div>
                                 
                                 <div class="form-group col-md-8">
                                    <label>Product Description</label>
                                    <textarea class="form-control" name="desc" id="desc" rows="4" placeholder="Enter Product Description..."></textarea>
                                 </div>

                                 <div class="reset-button col-md-12">
                                    <button type="button" class="btn btn-warning" onclick="reset_fun()">Reset</button>
                                    <button type="submit" class="btn btn-success">Save</button>
                                 </div>
                              </table>
                           </div>
                        </div>

                        <?php echo form_close();?>
                     </div>

                  </div>
               </div>

               <!-- Form -->

            </div>
         </div>
      </div>
   </section>
   <!-- /.content -->
</div>

<!-- ////////////////////////////////////// print sale invoice////////////////////////// -->
 <div class="content-wrapper" style="display: none;">
     
            <!-- Content Header (Page header) -->
           
            <!-- Main content -->
            <div id="print_data"  >
            <section class="content" style="background-color: white;">
    <style>
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 21px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
       /* line-height: inherit;*/
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        /*padding-bottom: 20px;*/
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        /*line-height: 45px;*/
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 0px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 0px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    </style>
    <div class="invoice-box" style="background-color: white;">

        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td  style="font-size: 13px;color: blue;font-weight: bold;">
                                Ziyad Bin Ahmad Bin Ibrahim Al Shahwan Trading Est.<br>
                                Wholesale of building materials and scrap <br>
                                C.R.:20501241159 :س  .ت 
                            </td>

                            <td dir="rtl" lang="ar" style="font-size: 13px;color: blue;font-weight: bold;">
                                 مؤسسة زياد بن احمد بن ابراهيم الشهوان التجارية   <br>
                                <p dir="rtl" lang="ar"> البيع بالجملة لمواد البناء والخردوات المعدنية </p> <br>
                              V.A.T- No: 300409886200003 : الرقيم زريباتا
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td style="padding: 0px !important;">
                                <hr style="border:2px solid blue;padding: 0px;">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr >
              <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                               <pre style="text-align: center;background-color: grey;border:1px solid black;font-weight: bold;border-radius: 0px;color: blue;">  VAT INVOICE 
                                 <p dir="rtl" lang="ar">فاتورة ضريبة القيمة المضافة</p></pre>
                           </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr >
                <td colspan="3">
                    <table>
                        <tr>
                            <td  style="font-size: 13px;color: blue;font-weight: bold;">
                                VENDORS DETAILS:<br>
                                BILL TO:<br>
                                INVOICE DATE:<br>
                                INVOICE NUMBER:<br>
                                ADDRESS:<br>
                                V.A.T- No:
                            <td class="clientDetail" style="font-size: 13px;color: blue;font-weight: bold;text-align: center;">
                              
                            </td>
                            <td  style="font-size: 13px;color: blue;font-weight: bold;float: right;">
                                تفاصيل البائعين:<br>
                                فاتورة الى:  <br>
                                تاريخ الفاتورة: <br>
                                رقم الفاتورة: <br> 
                                عنوان: <br>
                                ضريبة القيمة المضافة لا:
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="information">
                <td colspan="2  ">
                    <table style="border:1px solid blue;color: blue;">
                        <tr style="border:1px solid blue;text-align: center;">
                            <th style="border:1px solid blue;text-align: center;">Sr. #</th>
                            <th style="border:1px solid blue;text-align: center;">Description   <span style="">التفصيل  </span></th>
                            <th style="border:1px solid blue;text-align: center;">Weight  <span style=" ">وزن </span> <hr style="border:1px solid blue;">KG<span style=" ">كلو</span></th>
                            <th style="border:1px solid blue;text-align: center;">Price<br><br>
                            السعر  </th>
                            <th style="border:1px solid blue;text-align: center;">Total  مجموع   </th>
                        </tr>
                        <tbody id="tableContent">
                        
                        
                        </tbody>
                    </table>
                </td>
            </tr> 
            <br>
            <br>
            <tr >
                <td style="color: blue;font-weight: bold;">
                  <br>
                  <br>
                  <br>
                    Manager Signature / توقيع المدير  
                </td>
                
                <td style="color: blue;font-weight: bold;">
                  <br>
                  <br>
                  <br>
                   Customer Signature / توقيع العملاء  
                </td>
            </tr>
            
            <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                <hr style="border:2px solid blue;">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="font-size: 13px !important;color: blue;">
                <td colspan="3">
                    <table>
                        <tr >
                           الجوال: 0507975931 - 0549388266 - هاتف: 013 8281169 - صندوق البريد 003983 - الرمز البريدي
                            32441 - الدمام - شارع الملك سعود - منطقة الفيحاء  
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="font-size: 12px !important;color: blue;">
                <td colspan="3">
                    <table>
                        <tr >
                           Mobile:0507975931-0549388266-Telephone:013 8281169-P.O.Box 003983-Zip 32441-Dammam-King Saud Street-Fayhaa Area
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                <hr style="border:15px solid blue;">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
     </section>
               </div>
            <!-- /.content -->
         </div>
         <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
         <script type="text/javascript">
            function printData()
            {
               var divToPrint=document.getElementById("print_data");
               newWin= window.open("");
               newWin.document.write(divToPrint.outerHTML);
               newWin.print();
               newWin.close();
               $("#print_data").css('display','none');
            }
            $('#click_print').click(function(){
              $("#print_data").css('display','block');
               printData();
                 // $("#print_data").print();
            });
         </script>
<!-- ///////////////////////////////////////// print form end here/////////////////////////// -->
  <script type="text/javascript">

      


      function vat_fun(vat_no)
      {
         $.ajax({
            type: 'POST',
            url: '<?php echo base_url('client_info_get'); ?>',
            data: {vat_no:vat_no},
            dataType: 'json',
            success: function(response){
               if(response[1] != false)
               {
                  $("#advance_panel").show();
                   $("#adv_avl").val(response[1].advance_amount);
               }
              $('#comp_name').val(response[0].company_name);
              $('#contact').val(response[0].phone);
              $('#address').val(response[0].address);

           }
        });
         
      }
      $(function(){
         $('.advanceAvail').val(0);
        $( "#vat_num" ).autocomplete({
         source: "<? echo base_url('client_info');?>",
         minLength: 2,
         select: function( event, ui ) {
            
               vat_fun(ui.item.id);
            
            
            

     }
  });
     });

      function reset_fun()
      {
         $('#vat_num').val('');
         $('#comp_name').val('');
         $('#contact').val('');
         $('#address').val('');
         $('#invo_num').val('');
         $('#invo_date').val('');
         $('#product').val('');
         $('#weight').val('');
         $('#quantity').val('');
         $('#rate').val('');
         $('#discount').val('');
         $('#total_exc_vat').val('');
         $('#vat_perc').val('');
         $('#vat_sar').val('');
         $('.total_amt').val('');
         $('#desc').val('');
          $("#rem_adv").val('');


      }
      $('.rate').keyup(function(){
         var rate = $(this).val();
         var quantity = $(this).parent().parent('.count_form').find('.quantity').val();
         var discount = $(this).parent().parent('.count_form').find('.discount').val();
         $(this).parent().parent('.count_form').find('.total_exc_vat').val(rate*quantity-discount);
         var vat_perc = $(this).parent().parent('.count_form').find('.vat_perc').val();
         var total_exc_vat = $(this).parent().parent('.count_form').find('.total_exc_vat').val();
         $(this).parent().parent('.count_form').find('.vat_sar').val(total_exc_vat*vat_perc/100);
         var vat_sar = $(this).parent().parent('.count_form').find('.vat_sar').val();
         var total= parseFloat(vat_sar)+parseFloat(total_exc_vat);
         $(this).parent().parent('.count_form').find('.total_amt').val(total.toFixed(2));

         grand_total();
         checkAmount();
          $("#rem_adv").val('');




      });
      $('.quantity').keyup(function(){
         var quantity = $(this).val();
         var rate = $(this).parent().parent('.count_form').find('.rate').val();
         var discount = $(this).parent().parent('.count_form').find('.discount').val();
         
         $(this).parent().parent('.count_form').find('.total_exc_vat').val(rate*quantity-discount);
         var vat_perc = $(this).parent().parent('.count_form').find('.vat_perc').val();
         var total_exc_vat = $(this).parent().parent('.count_form').find('.total_exc_vat').val();
         $(this).parent().parent('.count_form').find('.vat_sar').val(total_exc_vat*vat_perc/100);
         var vat_sar = $(this).parent().parent('.count_form').find('.vat_sar').val();
         var total= parseFloat(vat_sar)+parseFloat(total_exc_vat);
         $(this).parent().parent('.count_form').find('.total_amt').val(total.toFixed(2));

          grand_total();
          checkAmount();
          $("#rem_adv").val('');

      });

      $('.discount').keyup(function(){
         var disc = $(this).val();
         var rate = $(this).parent().parent('.count_form').find('.rate').val();
         var quantity = $(this).parent().parent('.count_form').find('.quantity').val();
         $(this).parent().parent('.count_form').find('.total_exc_vat').val(rate*quantity-disc);
         var vat_perc = $(this).parent().parent('.count_form').find('.vat_perc').val();
         var total_exc_vat = $(this).parent().parent('.count_form').find('.total_exc_vat').val();
         $(this).parent().parent('.count_form').find('.vat_sar').val(total_exc_vat*vat_perc/100);
         var vat_sar = $(this).parent().parent('.count_form').find('.vat_sar').val();
         //$('.total_amt').val(parseInt(vat_sar)+parseInt(total_exc_vat));
         var total= parseFloat(vat_sar)+parseFloat(total_exc_vat);
         $(this).parent().parent('.count_form').find('.total_amt').val(total.toFixed(2));

         grand_total();
         checkAmount();
         $("#rem_adv").val('');

      });
      $('.vat_perc').change(function()
      {


               var vat_perc = $(this).val();
               var total_exc_vat = $(this).parent().parent('.count_form').find('.total_exc_vat').val();
               $(this).parent().parent('.count_form').find('.vat_sar').val(total_exc_vat*vat_perc/100);
               var vat_sar = $(this).parent().parent('.count_form').find('.vat_sar').val();
               if(total_exc_vat!=''){
                  var total= parseFloat(total_exc_vat)+parseFloat(vat_sar);
                  $(this).parent().parent('.count_form').find('.total_amt').val(total.toFixed(2));
                  grand_total();
               }

               ///advance amount calculations
               var gtotal = $("#grand_total").val();
                  var advanceAmt=parseFloat($('.advanceAvail').val());
                  if(advanceAmt < gtotal)
                  {
                     var rem_pay_amt = advanceAmt - gtotal;
                     $("#rem_adv").val(rem_pay_amt.toFixed(2));
                  }
                  else
                  {
                     var rem_adv_amt = advanceAmt - gtotal;
                     $("#rem_adv").val(rem_adv_amt.toFixed(2));
                  } 
               
            });
      ////// payment method dropdown

$(document).ready(function(){
   var count_form =$("body").find('.count_form');
    var count_form_length = count_form.length-1;
    $("#add_row").on('click',function(){
      var total = $(this).parent().parent().find('.total_amt').val();
       var sub = parseFloat(total)+parseFloat(total);

      //$('#grand_total').val(sub);
     
      $('.display_row').css('display','block');
     $("#clone_row").first().clone(true).find("input").val("").end().appendTo(".append_data");
     
     // $('.append_total').append("<div class='form-group col-md-6'><label>Grand Total Amount</label>"+
     //  "<input type='text' class='form-control' name='grand_total' id='grand_total'"+
     //  "readonly></div>");
     grand_total();
   
     count_form_length++;
 });

    $("#remove_row").on('click',function(){
       if (count_form_length>0){ 

        var name=$(this).parent().parent('.count_form').remove();
        count_form_length--;
        grand_total();
    }
    if(count_form_length==0){
      $('.display_row').css('display','none');
    }

});
//   $("#add_row").click(function(){
//     $("#clone_data").clone().appendTo("#append_data");
//   });
});


function grand_total(){
   var grand_total = 0;
   $('.total_amt').each(function(index){
      if($(this).val()!=null && $(this).val()!='')
      grand_total += parseFloat($(this).val());
   });

   $('#grand_total').val(grand_total.toFixed(2));
}

function checkAmount(){
   var grandTotal=parseFloat($('#grand_total').val());
   var adv=parseFloat($('.advanceAvail').val());
   if(adv>grandTotal){
      $(".paymentStatus").css("display", "none");
      $(".paymentStatus").attr("disabled",true);
   }else{
      $(".paymentStatus").css("display", "block");
      $(".paymentStatus").attr("disabled",false);

   }


}



$(document).ready(function(){
   $("#credit").attr('disabled', true);
         $("#cash").attr('disabled', true);
    setTimeout(function(){ $("#pur_msg").fadeOut(); }, 4000);
    setTimeout(function(){ $("#pur_msg_error").fadeOut(); }, 4000);
});

///////payment method //////
 $('#payment_method').change(function(){

      
var pm_value = $('#payment_method').val();
   if(pm_value == 'cash')
      {
              $("#cash").attr('disabled', false);
             $("#credit").attr('disabled', true);
      }
      else if(pm_value == 'credit')
      {
         $("#credit").attr('disabled', false);
         $("#cash").attr('disabled', true);
      }
      else
      {
      $("#cash").attr('disabled', false);
      $("#credit").attr('disabled', false);
      }
  
});




function verify_amt()
{
   var pm_value = $('#payment_method').val();
   var count_form =$("body").find('.count_form');
   var row_length = count_form.length;
   var advanceAmount=parseFloat($('.advanceAvail').val());


   if(row_length > 1)
   {
      var grand_total = parseFloat($("#grand_total").val());
      if(advanceAmount > grand_total)
      {
         saleInvoiceAdd();
      return true;
      }
      else
      {
         if(pm_value == 'cash')
         {
            
            var cash = parseFloat($("#cash").val())+advanceAmount;

            if((cash) != grand_total)
            {
               alert("Please enter the cash valid amount ...!");
               return false;
            }
         }
         else if((pm_value) == 'credit')
         {
            
            var credit = parseFloat($("#credit").val())+advanceAmount;
            if((credit) != grand_total)
            {
               alert("Please enter the credit valid amount ...!");
               return false;
            }
         }
         else
         {
            var cash = parseFloat($("#cash").val());
            var credit = parseFloat($("#credit").val());
            var total_amt_entered = cash+credit+advanceAmount;
             if(total_amt_entered != (grand_total))
            {
               alert("Please enter the valid amount in cash and credit ...!");
               return false;
            }
         }
      }
// grand total 
         
   }
   else
   {
      var grand_total = parseFloat($("#grand_total").val());

      if(advanceAmount > grand_total)
      {
      saleInvoiceAdd();
      return true;
      }
      else
      {
/// total amount 
        
         if(pm_value == 'cash')
         {
            var cash = parseFloat($("#cash").val())+advanceAmount;
            if(cash != grand_total)
            {
               alert("Please enter the cash valid amount ...!");
               return false;
            }
         }
         else if(pm_value == 'credit')
         {
            var credit = parseFloat($("#credit").val())+advanceAmount;
            if(credit != grand_total)
            {
               alert("Please enter the credit valid amount ...!");
               return false;
            }
         }
         else
         {
            var cash = parseFloat($("#cash").val());
            var credit = parseFloat($("#credit").val());
            var total_amt_entered = parseFloat(cash+credit)+advanceAmount;
             if(total_amt_entered > grand_total)
            {
               alert("Please enter the valid amount in cash and credit ...!");
               return false;
            }
         }
      }
   }

         saleInvoiceAdd();
      return true;
}

         function saleInvoiceAdd()
         {
            $.ajax({
               type: 'POST',
               url: '<?php echo base_url('new_sale_exe'); ?>',
               data: $('#sale_list').serialize(),
               dataType: 'json',
               success: function(response){
                  if(response.flag){
                     $('#tableContent').html('');
                     $('#srTotal').html('');
                     var dataContentHtml=``;
                     var qtyTotal=0;
                     var totalProPrice=0;
                     var vat_sar_total=0;
                     var incr = 1;
                     response.data.forEach(function(item){ 
                          qtyTotal=parseFloat(qtyTotal)+parseFloat(item.qty);
                          totalProPrice=parseFloat(item.total_amount)+parseFloat(totalProPrice);
                          vat_sar_total+=parseFloat(item.vat_sar)+parseFloat(vat_sar_total);
                          var total_amount_display = parseFloat(item.total_amount);
                          dataContentHtml+=` <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;">${incr}</td>
                          <td style="border:1px solid blue; text-align: left;">${item.name}</td>
                          <td style="border:1px solid blue;">${item.qty}</td>
                          <td style="border:1px solid blue;">${item.rate}</td>
                          <td style="border:1px solid blue;">${total_amount_display.toFixed(2)}</td>
                        </tr>`;
                        incr++;
                      }); 
                      dataContentHtml+=` <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                        </tr>`;
                        dataContentHtml+=` <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                        </tr>`;
                        dataContentHtml+=` <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                        </tr>`;
                         dataContentHtml+=` <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                        </tr>`;
                         dataContentHtml+=` <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                        </tr>`;
                         dataContentHtml+=` <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                        </tr>`;
                     
                     dataContentHtml+=` <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;text-align: center;color: blue;"  colspan="2">Total  <span>مجموع  </span></td>
                          <td style="border:1px solid blue;text-align: center;">${qtyTotal}</td>
                          <td style="border:1px solid blue;text-align: center;"></td>
                          <td style="border:1px solid blue;text-align: center;">${totalProPrice.toFixed(2)}</td>
                        </tr>
                        <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;text-align: center;color: blue;" colspan="2">VAT % 5<span>ضريبة القيمة المضافة </span> </td>
                          <td style="border:1px solid blue;text-align: center;color: blue;" colspan="3">${vat_sar_total.toFixed(2)}</td>
                        </tr>
                        <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;text-align: center;color: blue;" colspan="2">Grand Total <span>ضالمجموع الكلي  </span> </td>
                          <td style="border:1px solid blue;text-align: center;color: blue;" colspan="3">${(vat_sar_total+totalProPrice).toFixed(2)}</td>
                        </tr>`;
                        $('#tableContent').html(dataContentHtml);
                         $('.clientDetail').html('');
                         var record=response.data[0];
                       var clientDetail = `<br> ${record.company_name} <br> 
                        ${record.invoice_date}<br> ${record.invoice_no}<br> 
                        ا${record.address} <br> ${record.vat_no}`; 
                        $('.clientDetail').html(clientDetail);
                        // $('#srTotal').html(srTotal);
                        $("#sale_list")[0].reset();
                     printData();
                     window.location.href = '<?php echo base_url('new_sale_view'); ?>';

                  }else{
                     swal("Oops...", "Something is wrong!", "error");
                  }
                  
                  
               }
            });
         }
$('#invo_date').datetimepicker({
 timepicker:false,
 format:'d-m-Y'
});
</script>    
