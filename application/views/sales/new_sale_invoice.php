 <style type="text/css">
 .underline{text-decoration: underline;}
</style>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-users"></i>
      </div>
      <div class="header-title">
         <h1>New Sale Invoice</h1>
         <small>Sale list</small>
          <?php if($this->session->flashdata('pur_msg')): ?>
              <span id="pur_msg" style="color: green; text-align: right; float: right;color: #004085;background-color: #cce5ff;border-color: #b8daff;"><?php echo $this->session->flashdata('pur_msg');?></span>
           <?php endif;?>
           <?php if($this->session->flashdata('pur_msg_error')): ?>
              <span id="pur_msg_error" style="color: green; text-align: right; float: right;color: #004085;background-color: #cce5ff;border-color: #b8daff;"><?php echo $this->session->flashdata('pur_msg_error');?></span>
           <?php endif;?>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-12">
            <div class="panel panel-bd lobidrag">
               <div class="panel-heading">
                  <div class="btn-group" id="buttonlist"> 
                     <a class="btn btn-add " href="<? echo base_url('sale_list');?>"> 
                        <i class="fa fa-list"></i>  Sale Invoice List </a>  
                  </div>
               </div>
               <div class="panel-body">
                  <!-- Tab panels -->
                  <div class="tab-content col-sm-12">
                     <div class="tab-pane fade in active" id="tab1">
                        <form action="javascript:;" method="POST" name="sale_list" id="sale_list" onsubmit="verify_amt()" >
                        <div class="panel-body">

                           <div class="table-responsive">
                              <table class="table table-bordered table-striped table-hover">
                                 <div class=" col-md-12">
                                    <h2 class="underline">Client Information</h2>
                                 </div>
                                 <div class="form-group col-md-3">
                                    <label>VAT Number</label>
                                    <input type="number" id="vat_num" name="vat_num" class="form-control" placeholder="Enter VAT Number" required>
                                 </div>
                                 <div class="form-group col-md-3">
                                    <label>Company Name</label>
                                    <input type="text" class="form-control" required name="comp_name" id="comp_name" maxlength="40" style="text-align: right;"
                                    placeholder=" ...الرجاء إدخال اسم الشركة " required>
                                 </div>
                                 <div class="form-group col-md-3">
                                    <label>Contact Number</label>
                                    <input type="number" class="form-control" id="contact" name="contact" placeholder="Contact Number">
                                 </div>
                                 <div class="form-group col-md-3">
                                    <label>Address</label>
                                    <input type="text" maxlength="40" class="form-control" id="address" name="address" placeholder="...عنوان الشركة" style="text-align: right;">

                                 </div>
                                 
                              </table>
                           </div>

                        </div>

                        <div class="panel-body">

                           <div class="table-responsive">
                              <table class="table table-bordered table-striped table-hover">
                                 <div class=" col-md-12">
                                    <h2 class="underline">Invoice Details</h2>
                                 </div>
                                 <div class="form-group col-md-5">
                                    <label>Invoice Number</label>
                                    <input type="number" class="form-control" required name="invo_num" id="invo_num" placeholder="Invoice Number">
                                 </div>
                                 <div class="form-group col-md-5">
                                    <label>Invoice Date</label>
                                    <input type="text" name="invo_date" placeholder="DD-MM-YYYY" id="invo_date" required class="form-control">
                                 </div>
                                 <!-- <div class="form-group col-md-2">
                                    <label>Sales Order Number</label>
                                    <input type="text" class="form-control" name="sale_ord_num" id="sale_ord_num" placeholder="Sales Order Number" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
                                 </div>
                                 <div class="form-group col-md-2">
                                    <label>Sales Order Date</label>
                                    <input  type="text" class="form-control datetimepicker" name="sale_ord_date" id="sale_ord_date" placeholder="Enter Date...">
                                    
                                 </div>
                                 <div class="form-group col-md-2">
                                    <label>Delivery Note No.</label>
                                    <input type="text" class="form-control" name="deliv_note_num" id="deliv_note_num" placeholder="Enter Note Number" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
                                 </div>
                                 <div class="form-group col-md-2">
                                    <label>Cust. Ticket Noumber</label>
                                    <input type="text" class="form-control" name="cust_tck_num" id="cust_tck_num" placeholder="Cust. Ticket No." oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
                                 </div> -->
                                 
                              </table>
                           </div>

                        </div>
                        <div class="panel-body">

                           <div class="table-responsive">
                              <table class="table table-bordered table-striped table-hover">
                                 <div class=" col-md-12">
                                    <h2 class="underline">Product Details</h2>
                                 </div>
                                 <div  class="row count_form" id="clone_row" >
                                    <div class="form-group col-md-3">
                                       <label>Select Product</label>
                                       <select class="form-control" name="product[]" id="product" required>
                                          <option disabled selected>Please Select Product</option>
                                          <? foreach ($product_list as $product) {?>
                                             <option value="<?php echo $product->pro_id; ?>"><? echo $product->name;?></option>
                                          <? } ?>
                                       </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                       <label>Select Weight Unit</label>
                                       <select class="form-control" name="weight[]" id="weight" required>
                                          <option disabled selected>Please Select Product</option>
                                          <option value="1">MT</option>
                                          <option value="2">KG</option>
                                          <option value="3">TON</option>
                                       </select>
                                    </div>
                                    <div class="form-group col-md-2">
                                       <label>Quantity</label>
                                       <input type="number" step="0.0001" class="form-control quantity" name="quantity[]" id="quantity" required placeholder="Enter Quantity">
                                    </div>
                                    <div class="form-group col-md-2">
                                       <label>Rate</label>
                                       <input type="number" step="0.0001" class="form-control rate" name="rate[]" id="rate" placeholder="Enter Rate" required>
                                    </div>
                                    <div class="form-group col-md-2">
                                       <label>Discount</label>
                                       <input type="number" step="0.0001" class="form-control discount" name="discount[]" id="discount" placeholder="Enter Discount">
                                    </div>
                                    
                                    <div class="form-group col-md-3">
                                       <label>Total Exclusive VAT Amount</label>
                                       <input type="text" class="form-control total_exc_vat" name="total_exc_vat[]" id="total_exc_vat" readonly="">
                                    </div>
                                    <div class="form-group col-md-3">
                                       <label>VAT %</label>
                                       <select class="form-control vat_perc" required name="vat_perc[]" id="vat_perc">
                                          <option disabled selected>Select VAT %</option>
                                          <option value="5">5</option>
                                          <option value="5.5">5.5</option>
                                          <option value="6">6</option>
                                          <option value="7">7</option>
                                       </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                       <label>VAT SAR Amount</label>
                                       <input type="text" readonly="" class="form-control vat_sar" name="vat_sar[]" id="vat_sar">
                                    </div>
                                    <div class="form-group col-md-3">
                                       <label>Total Amount</label>
                                       <input type="text" class="form-control total_amt" name="total_amt[]"  readonly="">
                                    </div>
                                    <div id="append_total" class="append_total"></div>
                                    <div class="form-group col-md-3">
                                       <div type="button" id="add_row" class="btn btn-warning"><span class="glyphicon glyphicon-plus"></span></div>
                                       <div type="submit" id="remove_row" class="btn btn-danger"><span class="glyphicon glyphicon-minus"></span></div>
                                    </div>
                                 </div>
                                 <div id="append_data" class="append_data"></div>
                                 <div class="row display_row"  style="display: none;">
                                    <div class="form-group col-md-9"></div>
                                  <div class="form-group col-md-3" >
                                     <label>Grand Total</label>
                                       <input type="number" step="0.0001" class="form-control grand_total" name="grand_total" id="grand_total" value="0" readonly>
                                  </div>
                                  </div>
                              </table>
                           </div>
                        </div>
                         <div class="panel-body" id="advance_panel" style="display: none;">

                           <div class="table-responsive">
                              <table class="table table-bordered table-striped table-hover">
                                 <div class=" col-md-12">
                                    <h2 class="underline">Advance Amount</h2>
                                 </div>
                                 <div class="form-group col-md-6">
                                    <label id="lbl_cash">Advance Available</label>
                                    <input type="text" class="form-control advanceAvail" readonly name="adv_avl" id="adv_avl">
                                 </div>
                                 <div class="form-group col-md-6">
                                    <label id="lbl_credit">Remaining Amount</label>
                                    <input type="text" class="form-control" readonly name="rem_adv" id="rem_adv">
                                 </div>
                              </table>
                           </div>
                        </div>

                        <div class="panel-body paymentStatus">

                           <div class="table-responsive">
                              <table class="table table-bordered table-striped table-hover">
                                 <div class=" col-md-12">
                                    <h2 class="underline">Payment Details</h2>
                                 </div>
                                 <div class="form-group col-md-4">
                                    <label>Payment Method</label>
                                    <select class="form-control" required name="payment_method" id="payment_method">
                                       <option disabled selected>Select Payment Method</option>
                                       <option value="cash">Cash</option>
                                       <option value="credit">Credit</option>
                                       <option value="both">Both</option>
                                    </select>
                                 </div>
                                 <div class="form-group col-md-4">
                                    <label id="lbl_cash">Cash</label>
                                    <input type="number" step="0.0001" class="form-control" name="cash" id="cash" placeholder="Enter cash">
                                 </div>
                                 <div class="form-group col-md-4">
                                    <label id="lbl_credit">Credit</label>
                                    <input type="number" step="0.0001" class="form-control" name="credit" id="credit" placeholder="Enter credit">
                                 </div>
                              </table>
                           </div>
                        </div>

                         
                        <div class="panel-body">

                           <div class="table-responsive">
                              <table class="table table-bordered table-striped table-hover">
                                 <div class=" col-md-12">
                                    <h2></h2>
                                 </div>
                                 
                                 <div class="form-group col-md-8">
                                    <label>Product Description</label>
                                    <textarea class="form-control" name="desc" id="desc" rows="4" placeholder="Enter Product Description..."></textarea>
                                 </div>

                                 <div class="reset-button col-md-12">
                                    <button type="button" class="btn btn-warning" onclick="reset_fun()">Reset</button>
                                    <button type="submit" class="btn btn-success">Save</button>
                                 </div>
                              </table>
                           </div>
                        </div>

                        <?php echo form_close();?>
                     </div>

                  </div>
               </div>

               <!-- Form -->

            </div>
         </div>
      </div>
   </section>
   <!-- /.content -->
</div>

<!-- ////////////////////////////////////// print sale invoice////////////////////////// -->
   <div class="content-wrapper" style="display: none;">
     <!-- //////////////////////////  here is invoice code////////////////////// -->
     <div id="print_data"  >

  
  <style>
   @media print {
.header, .hide { visibility: hidden }
}

@media print {
    #Header, #Footer { display: none !important; }
  html, body {
    width: 230mm;
    height: 300mm;
  }
  /* ... the rest of the rules ... */
}
li{
lis
}

after::.box1{
   border-bottom: 2px solid #000;
}
.content-foot{
   width: 65%;
   margin: 0 auto;
}
.content-fot{
   width: 65%;
   margin: 0 auto;
}
.content-ft{
   width: 100%;
   margin: 0 auto;
   height: 40px!important;
   background-color: #2e5db3!important;
   margin-bottom: 10px;
}
.box-text{
       font-size: 16px;

    display: inline-block;
}
.td-box-file{
           width: 94%;
    margin: 0 auto;
}
.box21 {
   
    height: 40px;
    padding: 7px;
    background: #e2e8f0;
    width: 100%;
    font-weight: 600;
    color: #404eac!important;
    font-size: 16px;
    text-align: center;
    border: 1px solid #00e!important;
    background-image: #ccc;
}
caption{
   font-size:28px;
   margin-bottom:15px;
}
table{
    border: 1px solid #333;
    border-collapse: collapse;
    margin: 0 auto;
     width: 90%;
   }
    .box_de{
       width: 100%!important;
    }
.td-box{
   width: 45%;
}
.td-1{
   height: 20px;
}
.clr_blue{color: #00e!important;}
span{
   /*color: #00e!important;*/
}
.{
       line-height: 1.5;
    margin-top: 0;
    margin-bottom: 10px;
}
td, tr, th{
     border-collapse: collapse;
    padding: 12px;
    height: 38px;
    border: 1px solid #00e!important;
    width: 185px;
}

th{
       color: #0000ee!important;
    font-weight: 100;
    background-color: #ffffff;
    border-collapse: collapse;
}

li{
   list-style-type: none;
   color: #6d6dbf!important;
}
.fontheading{      
    font-size: 18px;
    color: #5456bf!important;
}
.fontheadin{   
 font-size: 16px;
  color: #5456bf!important;
}
.last-line{
       height: 40px;
    border-bottom: 1px solid #bbb;
    width: 60%;
    margin: 0 auto;
}
.ziyad{
    font-size: 16px;
    letter-spacing: -0.8px;
    font-weight: bold;
    color: #5456bf!important;
}

.ziarat{
       font-size: 20px;
    font-weight: bold;
    color: #5456bf!important;
}
.customer{
   font-size: 18px;
    color: #0000dd!important;
    color: #2e5db3!important;
}
.customer1{
   font-size: 16px;
    color: #0000dd!important;
    color: #2e5db3!important;
}

.border-1 {
    border: 2px solid #00e!important;
    width: 90%;
    padding: 4px;
    margin: 0 auto;
}
  </style>
<section>
   <div class="col-md-8">
     <div class="box-shedow">
         <div class="col-md-12">
            <div class="row">
               <div class="col-md-12">
                  <div class="row" style="margin-top: 16px;">
                     <div class="col-md-12" style="text-align: center;">
                        <div class="row">
                           <div class="col-md-6 col-sm-6 col-xs-6  ziyad">Ziyad Bin Ahmed Bin Ibrahim Al Shahwan Trading Est
                           </div>
                           <div class="ziarat col-md-6 col-sm-6 col-xs-6">مؤسسة زياد بن أحمد  بن  إبراهيم  الشهوان للتجارة 
                           </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                           <div class="box">
                              <p class="fontheading">Wholesale Of Building Meterial And Scrap</p>
                              <p class="fontheadin">C.R.:2050124159: <span>س.ت    </span> </p>
                           </div> 
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                           <div class="box">
                              <p class="fontheading">البيع بالجملة لموادالنباءوالخردوات المعدنية  </p>
                              <p class="fontheadin">V.A.T- No: 300409886200003  :الرقم ضر یبة</p> 
                           </div>
                        </div>

                     </div>
                  </div>
               </div>
            </div>
         <div class="">
            <div style="width: 100%; margin: 0 auto;">
               <div style="border-bottom:2px solid #00d; margin-bottom: 2px; ">
               </div>
               <div style="border-bottom:2px solid #00d; margin-bottom: 10px;">
               </div>
            </div>
         </div>
         <div class="col-md-12">
            <div class=" td-box-file">
               <div class="box21">VAT INVOICE &nbsp;  &nbsp;  &nbsp;  &nbsp;فاتورة ضريبة القيمة المضافة
               </div>   
            </div>
         </div>
         <div class="box-shedow">
            <table>
               <caption>
                  <div class="row">       
                     <div class="box-text col-md-4 col-xs-4 col-sm-4">
                        <ul style="text-align: left; margin-left: -38px;">
                           <li style="text-transform: uppercase;font-size: 16px; font-weight: bold;">Vendors details
                           </li>
                           <li>BILL TO:</li> 
                           <li>INVOICE DATE:</li>
                           <li>INVOICE NUMBER:</li>
                           <li>ADDRESS:</li>
                           <li>V.A.T- No:</li>
                        </ul>
                     </div>
                        <div class="box-text col-md-4 col-xs-4 col-sm-4">
                        <ul class="clientDetail">
                          <!--  <li class="td-1"></li>
                           <li>17-Aliril-2019</li> 
                           <li>002</li>
                           <li class="td-1"></li>
                           <li>الدمام</li> 
                           <li>300527238500003</li> -->
                        </ul>
                     </div>
                        <div class="box-text col-md-4 col-xs-4 col-sm-4">
                        <ul style="text-align: right;">
                           <li dir="rtl">تفاصيل العورد</li>
                           <li dir="rtl">تفاتورة الى:</li> 
                           <li dir="rtl">تاريخ</li> 
                           <li dir="rtl"> رقم الفاتورة:</li>
                           <li dir="rtl"> العنوان:</li>
                           <li>رقم الضريبة المضافة العميل:</li>
                        </ul>
                     </div>
                  </div>
               </caption>
            </table>
            <div class="border-1">
               <table class="box_de" style="border: 1px solid #00e;">
                  <thead>
                     <tr>
                        <th rowspan="2">Sr #</th>
                        <th rowspan="2" class="td-box">Description <span class="pull-right clr_blue">التفاصيل</span> </th>
                        <th>Weight &nbsp;الوزن  </th>
                           <th rowspan="2"> Price &nbsp;السعر</th>
                           <th rowspan="2"> Total&nbsp;   المجموع </th>
                     </tr>
                     <tr>
                        <th>KG  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;كلو</th>
                     </tr>

                  </thead>
                  <tbody id="tableContent">
                     
                  </tbody>
               </table>
            </div>
            <br>
         </div>
         <br>
         <div class="col-md-12">
               <div class="col-md-12"> 
                  <div class="col-md-6 col-sm-6 col-xs-6">
                     <br><br>
                     <p class="customer">Manager Signature&nbsp;/ <span class="clr_blue">توقيع المدير</span></p>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6">
                     <p class="customer pull-right">
                     <br><br>
                     Customer Signature&nbsp;/<span class="clr_blue"> توقيع العميل    </span>
                     </p>
                  </div>
               </div>
         </div>
         <div class="clearfix"></div>
         <div class="col-md-12">
            <div class="row">
            <br>
               <div class="footer-content">
                  <div style="width: 100%; margin: 0 auto;">
                     <div style="border-bottom:2px solid #00d; margin-bottom: 2px; "></div>
                        <div style="border-bottom:2px solid #00d; margin-bottom: 10px;">
                        </div>
                           <img src="<?php echo base_url('assets/invoices/img.png'); ?>" width="100%;">
                           <div class="clearfix"></div>
                        <div class="content-ft">
                        </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

</section>
   </div>
   </div>
   <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/earlyaccess/amiri.css">
  <link rel="stylesheet" type="text/css" href="https://lexique-arabe.org">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
         <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
         <script type="text/javascript" src="https://www.jqueryscript.net/demo/jQuery-Plugin-To-Print-Any-Part-Of-Your-Page-Print/jQuery.print.js"></script>

         <script type="text/javascript" src="<?php echo base_url('assets/printThis.js'); ?>"></script>
         <script type="text/javascript">
            function printData()
            {
               //$.print("#print_data");
               $('#print_data').printThis({
                      pageTitle: "",              // add title to print page
                   header: null,               // prefix to html
                   footer: null
               });

               // var divToPrint=document.getElementById("print_data");
               // newWin= window.open("");
               // newWin.document.write(divToPrint.outerHTML);
               // newWin.print();
               // newWin.close();
               // $("#print_data").css('display','none');
            }
            // $('#click_print').click(function(){
            //   $("#print_data").css('display','block');
            //    printData();
            //      // $("#print_data").print();
            // });
         </script>
<!-- ///////////////////////////////////////// print form end here/////////////////////////// -->
  <script type="text/javascript">

      


      function vat_fun(vat_no)
      {
         $.ajax({
            type: 'POST',
            url: '<?php echo base_url('client_info_get'); ?>',
            data: {vat_no:vat_no},
            dataType: 'json',
            success: function(response){
               if(response[1] != false)
               {
                  $("#advance_panel").show();
                   $("#adv_avl").val(response[1].advance_amount);
               }
              $('#comp_name').val(response[0].company_name);
              $('#contact').val(response[0].phone);
              $('#address').val(response[0].address);

           }
        });
         
      }
      $(function(){
         $('.advanceAvail').val(0);
        $( "#vat_num" ).autocomplete({
         source: "<? echo base_url('client_info');?>",
         minLength: 2,
         select: function( event, ui ) {
            
               vat_fun(ui.item.id);
            
            
            

     }
  });
     });

      function reset_fun()
      {
         $('#vat_num').val('');
         $('#comp_name').val('');
         $('#contact').val('');
         $('#address').val('');
         $('#invo_num').val('');
         $('#invo_date').val('');
         $('#product').val('');
         $('#weight').val('');
         $('#quantity').val('');
         $('#rate').val('');
         $('#discount').val('');
         $('#total_exc_vat').val('');
         $('#vat_perc').val('');
         $('#vat_sar').val('');
         $('.total_amt').val('');
         $('#desc').val('');
          $("#rem_adv").val('');


      }
      $('.rate').keyup(function(){
         var rate = $(this).val();
         var quantity = $(this).parent().parent('.count_form').find('.quantity').val();
         var discount = $(this).parent().parent('.count_form').find('.discount').val();
         $(this).parent().parent('.count_form').find('.total_exc_vat').val((rate*quantity-discount).toFixed(3));
         var vat_perc = $(this).parent().parent('.count_form').find('.vat_perc').val();
         var total_exc_vat = $(this).parent().parent('.count_form').find('.total_exc_vat').val();
         $(this).parent().parent('.count_form').find('.vat_sar').val((total_exc_vat*vat_perc/100).toFixed(3));
         var vat_sar = $(this).parent().parent('.count_form').find('.vat_sar').val();
         var total= parseFloat(vat_sar)+parseFloat(total_exc_vat);
         $(this).parent().parent('.count_form').find('.total_amt').val(total.toFixed(2));

         grand_total();
         checkAmount();
          $("#rem_adv").val('');




      });
      $('.quantity').keyup(function(){
         var quantity = $(this).val();
         var rate = $(this).parent().parent('.count_form').find('.rate').val();
         var discount = $(this).parent().parent('.count_form').find('.discount').val();
         
         $(this).parent().parent('.count_form').find('.total_exc_vat').val((rate*quantity-discount).toFixed(3));
         var vat_perc = $(this).parent().parent('.count_form').find('.vat_perc').val();
         var total_exc_vat = $(this).parent().parent('.count_form').find('.total_exc_vat').val();
         $(this).parent().parent('.count_form').find('.vat_sar').val((total_exc_vat*vat_perc/100).toFixed(3));
         var vat_sar = $(this).parent().parent('.count_form').find('.vat_sar').val();
         var total= parseFloat(vat_sar)+parseFloat(total_exc_vat);
         $(this).parent().parent('.count_form').find('.total_amt').val(total.toFixed(2));

          grand_total();
          checkAmount();
          $("#rem_adv").val('');

      });

      $('.discount').keyup(function(){
         var disc = $(this).val();
         var rate = $(this).parent().parent('.count_form').find('.rate').val();
         var quantity = $(this).parent().parent('.count_form').find('.quantity').val();
         $(this).parent().parent('.count_form').find('.total_exc_vat').val((rate*quantity-disc).toFixed(3));
         var vat_perc = $(this).parent().parent('.count_form').find('.vat_perc').val();
         var total_exc_vat = $(this).parent().parent('.count_form').find('.total_exc_vat').val();
         $(this).parent().parent('.count_form').find('.vat_sar').val((total_exc_vat*vat_perc/100).toFixed(3));
         var vat_sar = $(this).parent().parent('.count_form').find('.vat_sar').val();
         //$('.total_amt').val(parseInt(vat_sar)+parseInt(total_exc_vat));
         var total= parseFloat(vat_sar)+parseFloat(total_exc_vat);
         $(this).parent().parent('.count_form').find('.total_amt').val(total.toFixed(2));

         grand_total();
         checkAmount();
         $("#rem_adv").val('');

      });
      $('.vat_perc').change(function()
      {


               var vat_perc = $(this).val();
               var total_exc_vat = $(this).parent().parent('.count_form').find('.total_exc_vat').val();
               $(this).parent().parent('.count_form').find('.vat_sar').val((total_exc_vat*vat_perc/100).toFixed(3));
               var vat_sar = $(this).parent().parent('.count_form').find('.vat_sar').val();
               if(total_exc_vat!=''){
                  var total= parseFloat(total_exc_vat)+parseFloat(vat_sar);
                  $(this).parent().parent('.count_form').find('.total_amt').val(total.toFixed(2));
                  grand_total();
               }

               ///advance amount calculations
               var gtotal = parseFloat($("#grand_total").val());
                  var advanceAmt=parseFloat($('.advanceAvail').val());
                  if(advanceAmt < gtotal)
                  {
                     var rem_pay_amt = advanceAmt - gtotal;
                     $("#rem_adv").val(rem_pay_amt.toFixed(3));
                  }
                  else
                  {
                     var rem_adv_amt = advanceAmt - gtotal;
                     $("#rem_adv").val(rem_adv_amt.toFixed(3));
                  } 
               
            });
      ////// payment method dropdown

$(document).ready(function(){
   var count_form =$("body").find('.count_form');
    var count_form_length = count_form.length-1;
    $("#add_row").on('click',function(){
      var total = $(this).parent().parent().find('.total_amt').val();
       var sub = parseFloat(total)+parseFloat(total);

      //$('#grand_total').val(sub);
     
      $('.display_row').css('display','block');
     $("#clone_row").first().clone(true).find("input").val("").end().appendTo(".append_data");
     
     // $('.append_total').append("<div class='form-group col-md-6'><label>Grand Total Amount</label>"+
     //  "<input type='text' class='form-control' name='grand_total' id='grand_total'"+
     //  "readonly></div>");
     grand_total();
   
     count_form_length++;
 });

    $("#remove_row").on('click',function(){
       if (count_form_length>0){ 

        var name=$(this).parent().parent('.count_form').remove();
        count_form_length--;
        grand_total();
    }
    if(count_form_length==0){
      $('.display_row').css('display','none');
    }

});
//   $("#add_row").click(function(){
//     $("#clone_data").clone().appendTo("#append_data");
//   });
});


function grand_total(){
   var grand_total = 0;
   $('.total_amt').each(function(index){
      if($(this).val()!=null && $(this).val()!='')
      grand_total += parseFloat($(this).val());
   });

   $('#grand_total').val(grand_total.toFixed(3));
} 

function checkAmount(){
   var grandTotal=parseFloat($('#grand_total').val());
   var adv=parseFloat($('.advanceAvail').val());
   if(adv>grandTotal){
      $(".paymentStatus").css("display", "none");
      $(".paymentStatus").attr("disabled",true);
   }else{
      $(".paymentStatus").css("display", "block");
      $(".paymentStatus").attr("disabled",false);

   }


}



$(document).ready(function(){
   $("#credit").attr('disabled', true);
         $("#cash").attr('disabled', true);
    setTimeout(function(){ $("#pur_msg").fadeOut(); }, 4000);
    setTimeout(function(){ $("#pur_msg_error").fadeOut(); }, 4000);
});

///////payment method //////
 $('#payment_method').change(function(){

      
var pm_value = $('#payment_method').val();
   if(pm_value == 'cash')
      {
              $("#cash").attr('disabled', false);
             $("#credit").attr('disabled', true);
             $("#credit").val('');
      }
      else if(pm_value == 'credit')
      {
         $("#credit").attr('disabled', false);
         $("#cash").attr('disabled', true);
         $("#cash").val('');
      }
      else
      {
      $("#cash").attr('disabled', false);
      $("#credit").attr('disabled', false);
      }
  
});




function verify_amt()
{
   var grand_total = parseFloat($("#grand_total").val());
   
   var pm_value = $('#payment_method').val();
   var count_form =$("body").find('.count_form');
   var row_length = count_form.length;
   var advanceAmount=parseFloat($('.advanceAvail').val());
   //  var credit = parseFloat($("#credit").val());
   // if(advanceAmount === undefined || advanceAmount === null)
   // {
   //    console.log('value is Nan');
   //    credit = credit + advanceAmount;
   // }

   var credit = parseFloat($("#credit").val())+advanceAmount;
// console.log(credit,'credit',grand_total,'total');
   if(row_length > 1)
   {
      var grand_total = parseFloat($("#grand_total").val());
      if(advanceAmount > grand_total)
      {
         saleInvoiceAdd();
      return true;
      }
      else
      {
         if(pm_value == 'cash')
         {
            
            var cash = parseFloat($("#cash").val())+advanceAmount;

            if((cash) != grand_total)
            {
               alert("Please enter the cash valid amount ...!");
               return false;
            }
         }
         else if((pm_value) == 'credit')
         {
            
            var credit = parseFloat($("#credit").val())+advanceAmount;
            if((credit) != grand_total)
            {
               console.log(credit,grand_total);
               alert("Please enter the credit valid amount ...!");
               return false;
            }
         }
         else
         {
            var cash = parseFloat($("#cash").val());
            var credit = parseFloat($("#credit").val());
            var total_amt_entered = cash+credit+advanceAmount;
             if(total_amt_entered != (grand_total))
            {
               alert("Please enter the valid amount in cash and credit ...!");
               return false;
            }
         }
      }
// grand total 
         
   }
   else
   {
      var grand_total = parseFloat($("#grand_total").val());

      if(advanceAmount > grand_total)
      {
      saleInvoiceAdd();
      return true;
      }
      else
      {
/// total amount 
        
         if(pm_value == 'cash')
         {
            var cash = parseFloat($("#cash").val())+advanceAmount;
            if(cash != grand_total)
            {
               alert("Please enter the cash valid amount ...!");
               return false;
            }
         }
         else if(pm_value == 'credit')
         {
            var credit = parseFloat($("#credit").val())+advanceAmount;
            if(credit != grand_total)
            {
               // console.log(credit,grand_total);
               alert("Please enter the credit valid amount ...!");
               return false;
            }
         }
         else
         {
            var cash = parseFloat($("#cash").val());
            var credit = parseFloat($("#credit").val());
            var total_amt_entered = parseFloat(cash+credit)+advanceAmount;
             if(total_amt_entered > grand_total)
            {
               alert("Please enter the valid amount in cash and credit ...!");
               return false;
            }
         }
      }
   }

         saleInvoiceAdd();
      return true;
}

         function saleInvoiceAdd()
         {
            $.ajax({
               type: 'POST',
               url: '<?php echo base_url('new_sale_exe'); ?>',
               data: $('#sale_list').serialize(),
               dataType: 'json',
               success: function(response){
                  // console.log(response);return false;
                  if(response.flag){
                      $('#tableContent').html('');
                     $('#srTotal').html('');
                     var dataContentHtml=``;
                     var qtyTotal=0;
                     var totalProPrice=0;
                     var vat_sar_total=0;
                     var incr = 1;
                     response.data.forEach(function(item){ 
                          qtyTotal=parseFloat(qtyTotal)+parseFloat(item.qty);
                          totalProPrice=parseFloat(item.total_amount)+parseFloat(totalProPrice);
                          vat_sar_total+=parseFloat(item.vat_sar)+parseFloat(vat_sar_total);
                          total_amount_display=parseFloat(item.qty)*parseFloat(item.rate);
                          dataContentHtml+=` <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;">${incr}</td>
                          <td style="border:1px solid blue; text-align: left;">${item.name}</td>
                          <td style="border:1px solid blue;">${item.qty}</td>
                          <td style="border:1px solid blue;">${item.rate}</td>
                          <td style="border:1px solid blue;">${total_amount_display.toFixed(2)}</td>
                        </tr>`;
                        incr++;
                      }); 
                      dataContentHtml+=` <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                        </tr>`;
                         dataContentHtml+=` <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                        </tr>`;
                         dataContentHtml+=` <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                        </tr>`;
                         dataContentHtml+=` <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                        </tr>`;
                         dataContentHtml+=` <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                        </tr>`;
                         dataContentHtml+=` <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                          <td style="border:1px solid blue;padding-bottom:20px;"></td>
                        </tr>`;
                       dataContentHtml+=` <tr style="border:1px solid blue;">
                         <td colspan="2" class="customer1" style="text-align: center;">Total &nbsp;&nbsp;&nbsp; <span class="clr_blue">المجموع</span></td>
                          <td style="border:1px solid blue;text-align: center;">${qtyTotal}</td>
                          <td style="border:1px solid blue;text-align: center;"></td>
                          <td style="border:1px solid blue;text-align: center;">${total_amount_display.toFixed(2)}</td>
                        </tr>
                        <tr style="border:1px solid blue;">
                          <td colspan="2" class="customer" style="text-align: center;">VAT % 5 &nbsp;&nbsp;&nbsp;&nbsp;  5  <span class="clr_blue">٪ ضريبة القيمة المضافة</span></td>
                          <td style="border:1px solid blue;text-align: right;color: blue;" colspan="3">${vat_sar_total.toFixed(2)}</td>
                        </tr>
                        <tr style="border:1px solid blue;">
                         <td colspan="2" class="customer" style="text-align: center;"> Grand Total 
                         &nbsp;&nbsp;&nbsp;&nbsp;<span class="clr_blue">  المجموع الاجما لى  </span></td>
                          <td style="border:1px solid blue;text-align:right;color: blue;" colspan="3">${(vat_sar_total+total_amount_display).toFixed(2)}</td>
                        </tr>`;
                        $('#tableContent').html(dataContentHtml);
                         $('.clientDetail').html('');
                         var record=response.data[0];
                         var current_datetime = new Date(record.invoice_date)
                        var formatted_date = current_datetime.getDate() + "-" +
                        (current_datetime.getMonth() + 1) + "-" + current_datetime.getFullYear()
                       var clientDetail = `<li class="td-1"></li><li class="td-1" style="color:#000!important;">${record.company_name}</li>
                           <li style="color:#000!important;">${formatted_date}</li> 
                           <li style="color:#000!important;">${record.invoice_no}</li>
                           <li style="color:#000!important;">ا${record.address}</li> 
                           <li style="color:#000!important;">${record.vat_no}</li>
                      
                        `; 
                        $('.clientDetail').html(clientDetail);
                        // $('#srTotal').html(srTotal);
                        $("#sale_list")[0].reset();
                     printData();
                     // window.location.href = '<?php echo base_url('new_sale_view'); ?>';

                  }else{
                     swal("Oops...", "Something is wrong!", "error");
                  }
                  
                  
               }
            });
         }

         $('#invo_date').datetimepicker({
 timepicker:false,
 format:'d-m-Y'
});

</script>    
