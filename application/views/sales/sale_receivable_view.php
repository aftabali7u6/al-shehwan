 <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-file-text-o"></i>
      </div>
      <div class="header-title">
         <h1>Sale Receivable</h1>
         <small>Sale Receivable Amounts</small>
         <?php if(!empty($this->session->flashdata('update_msg'))): ?>
          <span id="updatemsg" style="color: green; text-align: right; float: right;"><? echo $this->session->flashdata('update_msg');?></span>
       <?php endif;?>
    </div>
 </section>
 <!-- Main content -->
 <section class="content">
   <div class="row">
      <div class="col-sm-12">
         <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
               <div class="btn-group" id="buttonlist"> 
                     <a class="btn btn-add " href="<? echo base_url('sale_list');?>"> 
                        <i class="fa fa-list"></i>  Sale Invoice List </a>  
                  </div>
            </div>
            
            <div class="panel-body">
               <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->

               <!-- ./Plugin content:powerpoint,txt,pdf,png,word,xl -->
               <div class="table-responsive">
                  <table id="example" class="table table-striped table-bordered" style="width:100%">
                     <!-- <table id="dataTableExample1" class="table table-bordered table-striped table-hover"> -->
                        <thead>
                           <tr class="info">
                              <th>Invoice Date</th>
                              <th>Invoice No.</th>
                              <th>Company Name</th>
                              <th>VAT #</th>
                              <th>Cash</th>
                              <th>Amount Payable</th>
                              <th>Total</th>
                              <!-- <th>Status</th> -->
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                          <?php if(isset($receivables)):
                              foreach ($receivables as $index => $receivable):
                             // echo $Receivable[$i]->credit;exit;
                             if(!empty($receivable->credit)): ?>
                             <? $total_amount =$receivable->cash+$receivable->credit; ?>
                             <tr class="payble<?php echo $receivable->invo_id;?> ">
                             <td><? echo formated_date($receivable->invoice_date,'d-m-Y');?></td>
                             <td><? echo $receivable->invoice_no;?></td>
                              <td><? echo $receivable->company_name;?></td>
                              <td><? echo $receivable->vat_no;?></td>
                              <td class="invo_cash"><? echo round($receivable->cash,2);?></td>
                              <td class="invo_credit"><? echo round($receivable->credit,2);?></td>
                              <td><? echo round($total_amount,2);?></td>
                              <td>
                             <button type="button" onclick="receive_amount(<? echo $receivable->invo_id;?>)" id="btn_action" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#customer1">Unpaid</button>
                           </td>
                            
                              </tr>
                             <? endif;?>
                          <? endforeach; ?>
                            <? endif;?>

                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- quote Modal1 -->
         <div class="modal fade" id="customer1" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header modal-header-primary">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                     <h3><i class="fa fa-user m-r-5"></i> Update Receivable Amount </h3>
                  </div>
                  <div class="modal-body">
                     <div class="row">
                        <div class="col-md-12">
                           <?php echo form_open("", array('name' => 'update_receivable', 'id' =>'update_receivable'));?>
                           <fieldset>
                              <!-- Text input-->

                              <div class="col-md-4 form-group">
                                 <label class="control-label" class="form-control">Amount Receivable:</label>
                                 <!-- <input type="text" id="name" name="name"  placeholder="Name" class="form-control"> -->
                              </div>
                              <div class="col-md-8 form-group">
                                 
                                 <input type="number" step="0.0001" id="receivable_amt" name="receivable_amt" placeholder="Payable Amount..." readonly class="form-control">
                              </div>
                              <div class="col-md-4 form-group">
                                 <label class="control-label" class="form-control">Enter Amount To Receive:</label>
                                 <!-- <input type="text" id="contact" name="contact" placeholder="contact" class="form-control"> -->
                              </div>
                              <div class="col-md-8 form-group">
                                 <!-- <label class="control-label">Address</label> -->
                                 <input type="number" step="0.0001" id="received_amt" name="Payable Amount..." placeholder="address" class="form-control">
                              </div>
                              <input type="hidden" name="invoice_cash" id="invoice_cash">
                              <input type="hidden" name="invoice_id" id="invoice_id">
                              <div class="col-md-12 form-group user-form-group">
                                 <div class="pull-right">
                                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                                    <a href="javascript:;" class="btn btn-add btn-sm" 
                                    onclick="update_receivecall()">Save</a>
                                 </div>
                              </div>
                           </fieldset>
                           <? echo form_close(); ?>
                        </div>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-danger pull-left" >Close</button>
                  </div>
               </div>
               <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
         </div>
      </section>
      <!-- /.content -->
   </div>

  <script type="text/javascript">

 function receive_amount(invo_id)
 {
  var invo_cash=$('.payble'+invo_id).find('.invo_cash').first().html();
  var credit=$('.payble'+invo_id).find('.invo_credit').first().html();
  $("#receivable_amt").val(credit);
  $("#received_amt").val(credit);
  $("#invoice_cash").val(invo_cash);
  $("#invoice_id").val(invo_id);
 }
 function update_receivecall()
 {
    var receivable_amount = $("#receivable_amt").val();
    var received_amount = $("#received_amt").val();
    if(parseInt(received_amount) > parseInt(receivable_amount))
    {
      alert("You have entered invalid amount!");
    }else{
      var amt_cash = $("#invoice_cash").val();
      var invo_id = $("#invoice_id").val();
      $.ajax({
        type: 'POST',
        url: '<?php echo base_url('update_receive_ajax'); ?>',
        dataType: 'json',
        data:{receivable_amount:receivable_amount,received_amount:received_amount,amt_cash:amt_cash,invo_id:invo_id},
            success: function(response){
               $('.payble'+invo_id).find('.invo_cash').first().html(response.cash);
               $('.payble'+invo_id).find('.invo_credit').first().html(response.credit);
               $('#customer1').modal('hide');
               if(response.credit == 0)
               {
                $('.payble'+invo_id).find('#btn_action').first().html("Paid");
                $('.payble'+invo_id).find('#btn_action').removeClass('btn-danger').addClass('btn-success');
               }
            }
    });
    }
    
 }
</script>