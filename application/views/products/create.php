 <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-suitcase"></i>
               </div>
               <div class="header-title">
                  <h1>Add New Product</h1>
                  <small>New Product Details</small>
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="panel lobidisable panel-bd">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonexport">
                              <a href="#">
                                 <h4>New Product</h4>
                              </a>
                           </div>
                        </div>
                        <div class="panel-body">
                           <?php echo form_open("add_product_exe", array('name' => 'add_product',
                            'id' => 'add_product', 'class' => 'col-sm-6' )); ?>
                           <!-- <form class="col-sm-6"> -->
                              <div class="form-group">
                                 <label>Product Name</label>
                                 <input type="text" class="form-control" placeholder="Product Name"
                                 name="pname" id="pname" required>
                                 
                              </div>
                              <div class="form-group">
                                 <label>Status</label>
                                 <select class="form-control" name="status">
                                    <option value="1">Active</option>
                                    <option value="0">Deactive</option>
                                 </select>
                              </div>
                             
                              
                              <div class="form-group">
                                 <label>Notes</label><br>
                                 <textarea name="description" id="description" rows="3" class="form-control"></textarea>
                              </div>
                              <div class="form-group">
                                 <input type="submit" value="Save" name="save_as_draft" class="btn btn-warning">
                                 <?php if($this->session->flashdata('pro_msg')): ?>
                                <span style="color: red;"><?php echo $this->session->flashdata('pro_msg'); ?></span>
                            <?php endif; ?>
                            <?php if(!empty($this->session->flashdata('pro_success'))): ?>
                                <span style="color: green;"><?php echo $this->session->flashdata('pro_success'); ?></span>
                             <?php endif;?>
                              </div>
                              <?php echo form_close();?>
                           <!-- </form> -->
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <!-- /.content -->
         </div>