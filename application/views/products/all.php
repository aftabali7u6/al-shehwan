 <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-file-text-o"></i>
               </div>
               <div class="header-title">
                  <h1>Products</h1>
                  <small>Products List</small>
                  <?php if(!empty($this->session->flashdata('update_msg'))): ?>
                                <span id="updatemsg" style="color: green; text-align: right; float: right;"><? echo $this->session->flashdata('update_msg');?></span>
                             <?php endif;?>
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonexport">
                              <a href="#">
                                 <h4>Products</h4>
                              </a>
                           </div>
                        </div>
                        <div class="panel-body">
                        <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->
                         
                           <!-- ./Plugin content:powerpoint,txt,pdf,png,word,xl -->
                           <div class="table-responsive">
                              <table id="dataTableExample1" class="table table-bordered table-striped table-hover">
                                 <thead>
                                    <tr class="info">
                                       <th>Sr. No</th>
                                       <th>Product Name</th>
                                       <th>Description</th>
                                       <th>Created by</th>
                                       <th>Uptated by</th>
                                       <th>Created at</th>
                                       <th>Updated at</th>
                                       <th>Status</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php $sr_no = 1;?>
                                    <?php foreach($products as $pro){ ?>
                                     <tr>
                                       
                                       <td><?php echo $sr_no;?></td>
                                       <?php $sr_no++; ?>
                                       <td><?php echo $pro->name;?></td>
                                       <td><?php echo $pro->description;?></td>
                                       <td><?php echo $pro->created_by;?></td>
                                       <td><?php echo $pro->updated_by;?></td>
                                       <td><?php echo $pro->created_at;?></td>
                                       <td><?php echo $pro->updated_at;?></td>
                                       <?php if($pro->status == '1'): ?>
                                       <td><span class="label label-success">Active</span></td>
                                       <?php else: ?>
                                          <td><span class="label label-danger">Deactive</span></td>
                                       <?php endif; ?> 
                                       <input type="hidden" value="<? echo $pro->pro_id; ?>" id="edit_id">
                                       <td>
                                          <button type="button" class="btn btn-add btn-sm" data-toggle="modal" data-target="#customer1" onclick="edit_pro(<?php echo $pro->pro_id;?>);"><i class="fa fa-pencil"></i></button>
                                          <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#customer2" 
                                          onclick="del_pro(<?php echo $pro->pro_id;?>)"><i class="fa fa-trash-o"></i> </button>
                                       </td>
                                       <?php } ?>
                                    </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- quote Modal1 -->
               <div class="modal fade" id="customer1" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog">
                     <div class="modal-content">
                        <div class="modal-header modal-header-primary">
                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                           <h3><i class="fa fa-user m-r-5"></i> Update Quotes</h3>
                        </div>
                        <div class="modal-body">
                           <div class="row">
                              <div class="col-md-12">
                                 <? echo form_open("update_product", array('name' => 'update_pro', 'id' =>
                                 'update_pro'));?>
                                    <fieldset>
                                       <!-- Text input-->
                                       <div class="col-md-6 form-group">
                                          <label class="control-label">Product name</label>
                                          <input type="text" id="pro_name" name="pro_name" placeholder="Product Name" class="form-control" required>
                                       </div>
                                       <!-- Text input-->
                                       <div class="col-md-6 form-group">
                                          <label>Status</label>
                                             <select class="form-control" id="state" name="state">
                                                <option value="1">Active</option>
                                                <option value="0">Deactive</option>
                                             </select>
                                       </div>
                                       <!-- Text input-->
                                       <div class="col-md-12 form-group">
                                           <label>Description</label><br>
                                             <textarea name="description" id="description" rows="3" class="form-control" required></textarea>
                                       </div>
                                       <input type="hidden" name="update_id" id="update_id">
                                       <div class="col-md-12 form-group user-form-group">
                                          <div class="pull-right">
                                             <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                                             <button type="submit" class="btn btn-add btn-sm">Save</button>
                                          </div>
                                       </div>
                                    </fieldset>
                                 <? echo form_close(); ?>
                              </div>
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                        </div>
                     </div>
                     <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
               </div>
               <!-- /.modal -->
               <!-- Modal -->   
               <!-- quote delete Modal2 -->
               <div class="modal fade" id="customer2" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog">
                     <div class="modal-content">
                        <div class="modal-header modal-header-primary">
                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                           <h3><i class="fa fa-user m-r-5"></i> Delete Product</h3>
                        </div>
                        <div class="modal-body">
                           <div class="row">
                              <div class="col-md-12">
                                 <? echo form_open("delete_product", array('name' => 'del_form',
                                 'id' => 'del_form', 'class' => 'form-horizontal'));?>
                                 <!-- <form class="form-horizontal"> -->
                                    <fieldset>
                                       <div class="col-md-12 form-group user-form-group">
                                          <label class="control-label">Delete Product</label>
                                          <input type="hidden" name="del" id="del">
                                          <div class="pull-right">
                                             <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">NO</button>
                                             <button type="submit" class="btn btn-add btn-sm">YES</button>
                                          </div>
                                       </div>
                                    </fieldset>
                                <? echo form_close();?>
                              </div>
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                        </div>
                     </div>
                     <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
               </div>
               <!-- /.modal -->
            </section>
            <!-- /.content -->
         </div>

         <script type="text/javascript">
           function edit_pro(pro_id)
           {
        
            $.ajax({
               type: 'POST',
               url: '<?php echo base_url('get_single_product_ajax'); ?>',
               data: {pro_id:pro_id},
               dataType: 'json',
               success: function(response){
                  if(response.flag){

                     $('#update_id').val(response.data.pro_id);
                     $('#');
                     $('#pro_name').val(response.data.name);
                     // console.log(response.data.status);
                   $('#state').val(response.data.status);
                   $('#description').val(response.data.description);

                  }
               }
            });
           }

           function del_pro(del_id)
           {
         $('#del').val(del_id);
           }
         </script>