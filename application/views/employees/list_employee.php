 <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-file-text-o"></i>
      </div>
      <div class="header-title">
         <h1>Employees</h1>
         <small>Employees List</small>
         <?php if(!empty($this->session->flashdata('update_msg'))): ?>
          <span id="updatemsg" style="color: green; text-align: right; float: right;"><? echo $this->session->flashdata('update_msg');?></span>
       <?php endif;?>
         <?php if(!empty($this->session->flashdata('delete_msg'))): ?>
          <span id="updatemsg" style="color: red; text-align: right; float: right;"><? echo $this->session->flashdata('delete_msg');?></span>
       <?php endif;?>
    </div>
 </section>
 <!-- Main content -->
 <section class="content">
   <div class="row">
      <div class="col-sm-12">
         <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
               <div class="btn-group" id="buttonexport">
                  <a href="<?php echo base_url('add_employee')?>">
                     <button class="btn btn-primary">Add Employee</button>

                  </a>
               </div>
            </div>
            <div class="panel-body">
               <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->

               <!-- ./Plugin content:powerpoint,txt,pdf,png,word,xl -->
               <div class="table-responsive">
                  <table id="example" class="table table-striped table-bordered" style="width:100%">
                     <!-- <table id="dataTableExample1" class="table table-bordered table-striped table-hover"> -->
                        <thead>
                           <tr class="info">
                              <th>Sr. No</th>
                              <th>Name</th>
                              <th>Joing Date</th>
                              <th>Contact</th>                             
                              <th>Salary</th>                 
                              <th>Image</th>
                              <th>Created by</th>
                              <th>Time</th>
                              <th>Status</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                          <?php if (!empty($employees) && $employees>0) {
                            $id = 1;
                            foreach($employees as $employee){ ?>
                             <tr>
                              <td><?php echo $id++; ?></td>
                              <td><?php echo $employee->name;?></td>
                              <td><?php echo $employee->date;?></td>
                              <td><?php echo $employee->contact;?></td>                             
                              <td><?php echo $employee->salary;?></td>                           
                              <td><img src="<?php echo base_url()?>images/<?php echo $employee->image; ?> " style="height: 60px;width: 60px"></td>
                              <td><?php echo $employee->created_by;?></td>
                              <td><?php echo $employee->created_at;?></td>
                              <td>
                               <?php if($employee->status==0) { ?>
                               <a href="javascript:;" style="padding: 7px" onclick="change_status(this,'<?php echo $employee->employee_id; ?>','<?php echo $employee->status; ?>')" class="btn btn-primary btn-xs  ">Pending</a>

                               <?php }else{ ?>
                               <span style="padding: 15px;height: 40px" class="btn btn-success btn-xs" >Employee</span>
                               <?php } ?>
                            </td> 
                            <input type="hidden" value="<? echo $employee->employee_id; ?>" id="edit_id">
                            <td>
                              <button type="button" class="btn btn-add btn-sm" data-toggle="modal" data-target="#customer1" onclick="update_employee1('<?php echo $employee->name;?>','<?php echo $employee->date;?>','<?php echo $employee->contact;?>','<?php echo $employee->salary;?>','<?php echo $employee->employee_id;?>');"><i class="fa fa-pencil"></i></button>
                               <a href="employee/Employees/delete/<?php echo $employee->employee_id;?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                 </td>
                              </tr>
                              <?php } } ?>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- quote Modal1 -->
         <div class="modal fade" id="customer1" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header modal-header-primary">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                     <h3><i class="fa fa-user m-r-5"></i> Update employee </h3>
                  </div>
                  <div class="modal-body">
                     <div class="row">
                        <div class="col-md-12">
                           <?php echo form_open("update_employee", array('name' => 'update_employee', 'id' =>'update_employee', 'enctype'=>'multipart/form-data'));?>
                           <fieldset>
                              <!-- Text input-->
                              <div class="col-md-6 form-group">
                                 <label class="control-label">name</label>
                                 <input type="text" id="name" name="name"  placeholder="Name" class="form-control">
                              </div>
                              <div class="col-md-6 form-group">
                                 <label class="control-label">Joining Date</label>
                                 <input type="date" id="date" name="date" placeholder="Date" class="form-control">
                              </div>
                              <div class="col-md-6 form-group">
                                 <label class="control-label">Contact</label>
                                 <input type="number" id="contact" name="contact" placeholder="contact" class="form-control">
                              </div>
                              <div class="col-md-6 form-group">
                                 <label class="control-label">Salary</label>
                                 <input type="number" id="salary" name="salary" placeholder="salary" class="form-control">
                              </div>
                              <div class="col-md-6 form-group">
                                 <label class="control-label">File</label>
                                
                                 <input type="file" id="photo" name="photo" placeholder="file" class="form-control">
                              </div>
                              <input type="hidden" name="update_id" id="update_id">
                              <div class="col-md-12 form-group user-form-group">
                                 <div class="pull-right">
                                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-add btn-sm">Save</button>
                                 </div>
                              </div>
                           </fieldset>
                           <? echo form_close(); ?>
                        </div>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                  </div>
               </div>
               <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
         </div>
      </section>
      <!-- /.content -->
   </div>

   <script type="text/javascript">
      function update_employee1($name,$date,$contact,$salary,$update_id,$photo) {
            // alert($name);
            $('#name').val($name);
            $('#date').val($date);
            $('#contact').val($contact);
            $('#salary').val($salary);
            $('#update_id').val($update_id);
            $('#photo').val($photo);
         }
      </script>
      <script type="text/javascript">
       function change_status(obj,id,status) {
        var res = confirm("Are You Sure to except this request ?");
        if(res)
        {
           var id=id;
           var status=status;
     // alert(status);
    $.ajax
    ({
      url:'<?php echo base_url('employee/Employees/change_status/');?>',
      method:'POST',
      dataType:'json',
      data:{id:id,status:status},
      success:function(data)
      {
       // console.log("check status",data.status);
      if(data.status==1) {
         $(obj).replaceWith('<span class="btn btn-success btn-xs" style="padding: 15px;height: 40px"> Employee </span>');

      } else {
       $(obj).replaceWith('<a href="javascript:;" style="padding: 7px" onclick="change_status(this,'+data.id+',0)" class="btn btn-danger btn-xs"> Pending </a>');
    }
 }
});
        }
     // alert(obj,id,status);
    
 }
</script>
<script type="text/javascript">
 function hide_records(obj,id,status){
  var con = confirm('Are You Sure To Delete This Record?');
  if (con) {
    var id=id;
     var status=status;
     // alert(id);
    $.ajax
    ({
      url:'<?php echo base_url('employee/Employees/hide_record/');?>',
      method:'POST',
      dataType:'json',
      data:{id:id,status:status},
      success:function(data)
      {
      // console.log("check status",data.status);
      if(data.status==1) {
         $(obj).replaceWith('<a href="javascript:;" style="padding: 10px" onclick="change_status(this,'+data.id+',0)" class="btn btn-danger btn-xs"> Delete </a>');

      } else {
       location.reload();
    }
 }
});
  }
     // alert(obj,id,status);
     
 }
</script>