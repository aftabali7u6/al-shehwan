 <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-suitcase"></i>
               </div>
               <div class="header-title">
                  <h1>Add New Employee</h1>
                  <small>New Employee Details</small>
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="panel lobidisable panel-bd">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonexport">
                              <a href="#">
                                 <h4>New Employee</h4>
                              </a>
                           </div>
                        </div>
                        <div class="panel-body">
                           <?php echo form_open("store_employee", array('name' => 'store_employee',
                            'id' => 'store_employee', 'class' => 'col-sm-12','enctype'=>'multipart/form-data' )); ?>
                           <!-- <form class="col-sm-6"> -->
                              <div class="form-group">
                                 <label>Name</label>
                                 <input type="text" class="form-control" placeholder="Enter Name"
                                 name="name" id="name">
                                 
                              </div>
                              <div class="form-group">
                                 <label>Date of joining</label>
                                 <input type="date" class="form-control" placeholder="Enter date"
                                 name="date" id="date">
                                 
                              </div>
                              <div class="form-group">
                                 <label>Contact</label>
                                 <input type="number" class="form-control" placeholder="Enter Contact Number"
                                 name="contact" id="contact">
                                 
                              </div>
                     
                              <div class="form-group">
                                 <label>salary</label>
                                 <input type="Number" class="form-control" placeholder="Enter salary"
                                 name="salary" id="salary">
                              </div>
                              
                              <div class="form-group">
                                 <label>Image</label>
                                 <input type="file" class="form-control" placeholder="File"
                                 name="photo" id="photo">
                              </div>
                              

                              <div class="form-group">
                                 <input type="submit" value="Save" name="save_as_draft" class="btn btn-warning">
                                 <a class="btn btn-primary" href="<?php echo base_url('employee_index'); ?>">Cancel</a>
                                 <?php if($this->session->flashdata('pro_msg')): ?>
                                <span style="color: red;"><?php echo $this->session->flashdata('pro_msg'); ?></span>
                            <?php endif; ?>
                            <?php if(!empty($this->session->flashdata('pro_success'))): ?>
                                <span style="color: green;"><?php echo $this->session->flashdata('pro_success'); ?></span>
                             <?php endif;?>
                              </div>
                              <?php echo form_close();?>
                           <!-- </form> -->
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <!-- /.content -->
         </div>