<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-dashboard"></i>
      </div>
      <div class="header-title">
         <h1>Admin Dashboard</h1>
         <small>Very detailed & featured admin.</small>
      </div>
   </section>
   <style >
   .img-car{
      width: 100%;
   }
</style>
<!-- Main content -->
<section class="content">
 <div class="row">
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3" id="payable">
                     <div id="cardbox1">
                        <div class="statistic-box">
                           <i class="fa fa-user-plus fa-3x"></i>
                           <div class="counter-number pull-right">
                              <span class="count-number">
                                 <?php echo $sumPurchase; ?>
                              </span> 
                              <span class="slight"><i class="fa fa-play fa-rotate-270"> </i>
                              </span>
                           </div>
                           <h3> Total Purchase</h3>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3" id="receive">
                     <div id="cardbox2">
                        <div class="statistic-box">
                           <i class="fa fa-user-secret fa-3x"></i>
                           <div class="counter-number pull-right">
                              <span class="count-number">
                                 <?php echo $sumSales; ?>
                              </span> 
                              <span class="slight"><i class="fa fa-play fa-rotate-270"> </i>
                              </span>
                           </div>
                           <h3>  Total Sale</h3>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3" id="expense">
                     <div id="cardbox3">
                        <div class="statistic-box">
                           <i class="fa fa-money fa-3x"></i>
                           <div class="counter-number pull-right">
                              <i></i><span class="count-number">
                                 <?php if (isset($expenses) && !empty($expenses)):
                                    echo $expenses[0]->total_expense;?>
                                    <?php else: 
                                       echo '0';
                                       ?>
                                 <?php endif; ?>
                              </span> 
                              <span class="slight"><i class="fa fa-play fa-rotate-270"> </i>
                              </span>
                           </div>
                           <h3>  
                              Expenses
                           </h3>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                     <div id="cardbox3">
                        <div class="statistic-box">
                           <i class="fa fa-money fa-3x"></i>
                           <div class="counter-number pull-right">
                              <?php $pro_or_loss = '';
                              ?>
                              <i></i><span class="count-number">
                                 <?php if(isset($expenses) && !empty($expenses))
                                 {
                                     $total_exp = $expenses[0]->total_expense; 
                                    $exp_purchase = $total_exp + $sumPurchase;
                                    if($exp_purchase > $sumSales){
                                       $pro_or_loss= "Loss";
                                       echo $exp_purchase - $sumSales;
                                    }
                                    elseif($exp_purchase < $sumSales)
                                    {
                                       $pro_or_loss= 'Profit';
                                       echo $sumSales - $exp_purchase;

                                    }
                                    else
                                    {
                                        echo '0';
                                       $pro_or_loss = 'Profit / Loss';
                                    }
                                 }
                                 else
                                 {
                                    if($sumPurchase > $sumSales){
                                       $pro_or_loss= "Loss";
                                       echo $sumPurchase - $sumSales;
                                    }
                                    elseif($sumPurchase < $sumSales)
                                    {
                                       $pro_or_loss = 'Profit';
                                       echo $sumSales - $sumPurchase;

                                    }
                                    else
                                    {
                                        echo '0';
                                       $pro_or_loss = 'Profit / Loss';
                                    }
                                 }?>
                                    <!-- <?php //echo $pro_or_loss_amt; ?> -->
                              </span> 
                              <span class="slight"><i class="fa fa-play fa-rotate-270"> </i>
                              </span>
                           </div>
                           <h3>  
                             <?php echo $pro_or_loss; ?>
                           </h3>
                        </div>
                     </div>
                  </div>
               </div>
               
               <!-- /// list payable code -->
   <div class="row payable_tbl">
      <div class="col-sm-12">
         <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
               <div class="btn-group" id="buttonexport">
               <h4>Purchase List</h4>
            </div>
            </div>
            <div class="panel-body">
                              <div class="table-responsive" id="">
                                 <table id="example2" class="table table-striped table-bordered">
                                    <thead>
                                       <tr class="info">
                                          <th>Dated</th>
                                          <th>Invoice No.</th>
                                          <th>Company Name</th>
                                          <th>VAT #</th>
                                          <th>Weight</th>
                                          <th>Amount</th>
                                          <th>Vat Amount</th>
                                          <th>Total</th>
                                          <th>Status</th>
                                       </tr>
                                    </thead>
                                    <tbody id="datatablebody">
                                       <?php if(isset($purchases) && !empty($purchases)):?>

                                       <?php foreach($purchases as $invoice){ ?>
                                         <tr>
                                          <td><?php echo $invoice->invoice_date; ?></td>
                                          <td><?php echo $invoice->invoice_no;?></td>
                                          <td><?php echo $invoice->company_name;?></td>
                                          <td><?php echo $invoice->vat_no;?></td>
                                          <td><?php echo $invoice->qty;?></td>
                                          <td><?php echo round($invoice->total_exec_vat,2);?></td>
                                          <td><?php echo round($invoice->vat_sar,2);?></td>
                                          <td><?php echo round($invoice->total_amount,2);?></td>
                                          <?php if($invoice->invo_status == 0): ?>
                                          <td><span class="label-danger label label-default" >Pending</span>
                                          </td>
                                          <?php else: ?>
                                          <td><span class="label-custom label label-default" >Completed</span>
                                          </td>
                                        <?php endif; ?>
                                       <?php } ?>
                                    <?php endif; ?>
                                 </tbody>
                              </table>
                           </div>
                        </div>
               </div>
            </div>
         </div>


         <!-- ///list payable code end -->


         <!-- /// list receiveable start  -->
      <div class="row receive_tbl" style="display: none;">
      <div class="col-sm-12">
         <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
               <div class="btn-group" id="buttonexport">
               <h4>Sale List</h4>
            </div>
            </div>
            <div class="panel-body">
                              <div class="table-responsive" id="">
                                 <table id="example" class="table table-striped table-bordered">
                                    <thead>
                                       <tr class="info">
                                          <th>Dated</th>
                                          <th>Invoice No.</th>
                                          <th>Company Name</th>
                                          <th>VAT #</th>
                                          <th>Amount</th>
                                          <th>Vat Amount</th>
                                          <th>Total</th>
                                          <th>Status</th>
                                       </tr>
                                    </thead>
                                    <tbody id="datatablebody">
                                       <? if(isset($sales) && !empty($sales)):?>

                                       <? foreach($sales as $invoice): ?>
                                         <tr>
                                          <td><? echo $invoice->invoice_date;?></td>
                                          <td><? echo $invoice->invoice_no;?></td>
                                          <td><? echo $invoice->company_name;?></td>
                                          <td><? echo $invoice->vat_no;?></td>
                                          <td><? echo round($invoice->total_exec_vat,2);?></td>
                                          <td><? echo round($invoice->vat_sar,2);?></td>
                                          <td><? echo round($invoice->total_amount,2);?></td>
                                          <?php if($invoice->invo_status == 0): ?>
                                          <td><span class="label-danger label label-default" >    Pending</span>
                                          </td>
                                          <?php else: ?>
                                          <td><span class="label-custom label label-default" >Completed</span>
                                          </td>
                                        <?php endif; ?>
                                       <? endforeach; ?>
                                    <? endif;?>
                                 </tbody>
                              </table>
                           </div>
                        </div>
               </div>
            </div>
         </div>


         <!-- /// expenses list start here -->

         <div class="row expense_tbl" style="display: none;">
      <div class="col-sm-12">
         <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
               <div class="btn-group" id="buttonexport">
               <h4>Expense List</h4>
            </div>
            </div>
            <div class="panel-body">
                              <div class="table-responsive" id="">
                                 <table id="example" class="table table-striped table-bordered">
                                    <thead>
                                       <tr class="info">
                                       <th>Sr. No</th>
                                       <th>Date</th>
                                       <th>Expense category</th>
                                       <th>Expense Amount</th>
                                       <th>Description</th>
                                       </tr>
                                    </thead>
                                    <tbody id="datatablebody">
                                       <? if(isset($expenses) && !empty($expenses)):?>
                                          <?php $sr_no='1'; ?>
                                       <? foreach($expenses as $expense): ?>
                                       <tr>
                                          <td><?php echo $sr_no;?></td>
                                       <?php $sr_no++; ?>
                                       <td><?php echo $expense->exp_date;?></td>
                                       <td><?php echo $expense->cat_name;?></td>
                                       <td><?php echo round($expense->amount,2);?></td>
                                       <td><?php echo $expense->exp_description;?></td>
                                       <? endforeach; ?>
                                    <? endif;?>
                                 </tbody>
                              </table>
                           </div>
                        </div>
               </div>
            </div>
         </div>
         <!-- ///expenses list end here -->

              
      </section>
            <!-- /.content -->
   </div>
<script type="text/javascript">
 
   $("#payable").click(function(){
      $(".payable_tbl").css('display','block');
      $(".receive_tbl").css('display','none');
      $(".expense_tbl").css('display','none');
   });
    $("#receive").click(function(){
      $(".payable_tbl").css('display','none');
      $(".expense_tbl").css('display','none');
      $(".receive_tbl").css('display','block');
   }); 
    $("#expense").click(function(){
      $(".expense_tbl").css('display','block');
      $(".payable_tbl").css('display','none');
      $(".receive_tbl").css('display','none');
   });
</script>
 