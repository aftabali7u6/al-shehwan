 <!-- /// css for image modal show  -->
 <style>
body {font-family: Arial, Helvetica, sans-serif;}

#myImg {
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.img_modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 0px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.show-model-content {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
}

/* Caption of Modal Image */
#caption {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
  text-align: center;
  color: #ccc;
  padding: 10px 0;
  height: 150px;
}

/* Add Animation */
.show-model-content, #caption {  
  -webkit-animation-name: zoom;
  -webkit-animation-duration: 0.6s;
  animation-name: zoom;
  animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
  from {-webkit-transform:scale(0)} 
  to {-webkit-transform:scale(1)}
}

@keyframes zoom {
  from {transform:scale(0)} 
  to {transform:scale(1)}
}

/* The Close Button */
.close {
  position: absolute;
  top: 15px;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.close:hover,
.close:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
  .show-model-content {
    width: 100%;
  }
}
</style>



 <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-file-text-o"></i>
      </div>
      <div class="header-title">
         <h1>Receivable</h1>
         <small>Receivable List</small>
        <?php if(!empty($this->session->flashdata('add_success'))): ?>
          <span id="updatemsg" style="color: green; text-align: right; float: right;"><? echo $this->session->flashdata('add_success');?></span>
       <?php endif;?>
        <?php if(!empty($this->session->flashdata('add_fail'))): ?>
          <span id="updatemsg" style="color: red; text-align: right; float: right;"><? echo $this->session->flashdata('add_fail');?></span>
       <?php endif;?>
    </div>
 </section>
 <!-- Main content -->
 <section class="content">
   <div class="row">
      <div class="col-sm-12">
         <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
               <div class="btn-group" id="buttonexport">
                  <!-- <a href="<?php echo base_url('add_receivable')?>">
                     <button class="btn btn-primary">Add Receivable</button>

                  </a> -->
                    <a href="javascript:;" id="btn_action" class="btn btn-add btn-sm" data-toggle="modal" data-target="#customer2">Add Receivable</a>
               </div>
            </div>
            <div class="panel-body">
               <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->

               <!-- ./Plugin content:powerpoint,txt,pdf,png,word,xl -->
               <div class="table-responsive">
                  <table id="example" class="table table-striped table-bordered" style="width:100%">
                     <!-- <table id="dataTableExample1" class="table table-bordered table-striped table-hover"> -->
                        <thead>
                           <tr class="info">
                              <th>Date</th>
                              <th>Name</th>
                              <th>Contact</th>
                              <th>Address</th>
                              <th>amount</th>
                              <th>Paid by</th>
                              <th>Image</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php if (!empty($receivables) && $receivables>0) {
                            foreach($receivables as $receivables){ ?>
                             <tr>
                               <td><?php echo formated_date($receivables->date,'d-m-Y');?></td>
                              <td><?php echo $receivables->name;?></td>
                              <td><?php echo $receivables->contact;?></td>
                              <td><?php echo $receivables->address;?></td>
                              <td><?php echo round($receivables->amount,2);?></td>
                              <td><?php echo $receivables->paid_by;?></td>
                              <td>
                                <a href="javascript:;" class="pop">
                                <img src="<?php echo base_url();?>assets/images/receivables/<?php echo $receivables->image;?>" alt="Receivable image.." class="image_show" style="height: 60px;width: 60px">
                                </a>
                              </td>
                                <td>
                                 <a href="javascript:;" onclick="received_amount(<?php echo $receivables->receivable_id;?>)" id="btn_action" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#customer1">Receiveables</a>
                              <?php } } ?>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
        <div class="img_modal fade" id="imagemodal" style="z-index: 9999;" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="show-model-content">
                 
                  <div class="modal-body" style="padding: 5px !important;">
                    <img src="" id="imagepreview" style="width: 468px; height: 470px;" >
                  </div>
                 
                </div>
              </div>
            </div>
         <!-- quote Modal1 -->
         <div class="modal fade" id="customer1" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header modal-header-primary">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                     <h3><i class="fa fa-user m-r-5"></i> Update Receivable</h3>
                  </div>
                  <div class="modal-body">
                     <div class="row">
                        <div class="col-md-12">
                           <?php echo form_open("update_receivable", array('name' => 'update_receivable', 'id' =>'update_receivable', 'enctype'=>'multipart/form-data'));?>
                           <fieldset>
                              <!-- Text input-->
                              <div class="col-md-6 form-group">
                                 <label class="control-label">name</label>
                                 <input type="text" id="name" name="name" required  placeholder="Name" class="form-control">
                              </div>
                              <div class="col-md-6 form-group">
                                 <label class="control-label">Contact</label>
                                 <input type="number" id="contact" name="contact" required placeholder="contact" class="form-control">
                              </div>
                              <div class="col-md-6 form-group">
                                 <label class="control-label">Address</label>
                                 <input type="text" id="address" name="address" required placeholder="address" class="form-control">
                              </div>
                               <div class="col-md-6 form-group">
                                 <label class="control-label">Paid By</label>
                                 <input type="text" id="paid_by" name="paid_by" required placeholder="Paid By" class="form-control">
                              </div>
                              <div class="col-md-6 form-group">
                                 <label class="control-label">Receivable Amount</label>
                                 <input type="number" step="0.0001" readonly id="receivable_am" name="receivable_am" placeholder="Receivable amount..." class="form-control">
                              </div>
                              <div class="col-md-6 form-group">
                                 <label class="control-label">Receiving Amount</label>
                                 <input type="number" step="0.0001" id="receiving_amt" name="receiving_amt" required placeholder="Receiving amount..." class="form-control">
                              </div>
                             <div class="col-md-6 form-group">
                                 <label>Date</label>
                                 <input type="date" class="form-control receive_date" required placeholder="Enter date"
                                 name="date" id="date">
                                 
                              </div>
                              <div class="col-md-6 form-group">
                                 <label class="control-label">File</label>
                                 <input type="file" id="photo" name="photo" placeholder="file" class="form-control">
                                  <input type="hidden" name="old_img" id="old_img">
                                
                                 <img src="" id="show_old_img" height="80px" width="80px" style="margin-top: 10px;" alt="Receivable Image...">
                              </div>
                              <input type="hidden" name="receivable_id" id="receivable_id">
                               <div class="col-md-6 form-group">
                                  <label class="control-label">Description:</label>
                                  <textarea placeholder="Description..." class="form-control" name="desc" id="desc" rows="4"></textarea>
                              </div>
                              <input type="hidden" name="update_id" id="update_id">
                              <div class="col-md-12 form-group user-form-group">
                                 <div class="pull-right">
                                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-add btn-sm" id="update_receive">Save</button>
                                 </div>
                              </div>
                           </fieldset>
                           <? echo form_close(); ?>
                        </div>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                  </div>
               </div>
               <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
         </div>

         <!-- //// add new receivable  -->
         <div class="modal fade" id="customer2" tabindex="-2" style="z-index: 999999;" role="dialog2" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header modal-header-primary">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                     <h3><i class="fa fa-user m-r-5"></i> Add Receivable </h3>
                  </div>
                  <div class="modal-body">
                     <div class="row">
                        <div class="col-md-12">
                           <?php echo form_open("store_receivable", array('name' => 'add_receivable', 'id' =>'add_receivable', 'enctype'=>'multipart/form-data'));?>
                           <fieldset>
                              <!-- Text input-->
                              <div class="col-md-6 form-group">
                                 <label class="control-label">Name</label>
                                 <input type="text" id="name" required name="name" required  placeholder="Name" class="form-control">
                              </div>
                              <div class="col-md-6 form-group">
                                 <label class="control-label">Contact</label>
                                 <input type="number" id="contact" name="contact" required placeholder="contact" class="form-control">
                              </div>
                              <div class="col-md-6 form-group">
                                 <label class="control-label">Address</label>
                                 <input type="text" id="address" name="address" required placeholder="address" class="form-control">
                              </div>
                              <div class="col-md-6 form-group">
                                 <label class="control-label">Amount</label>
                                 <input type="number" step="0.0001" id="amount" name="amount" required placeholder="amount" class="form-control">
                              </div>
                              <div class="col-md-6 form-group">
                                 <label class="control-label">Paid By</label>
                                 <input type="text" id="pai_by" name="pai_by" required placeholder="received from" class="form-control">
                              </div>
                              <div class="col-md-6 form-group">
                                 <label>Date</label>
                                 <input type="date" class="form-control receive_date" required placeholder="Enter date"
                                 name="date" id="date">
                                 
                              </div>
                              <div class="col-md-6 form-group">
                                 <label class="control-label">File</label>
                                 <input type="file" id="photo" name="photo" placeholder="file" class="form-control">   
                              </div>
                              <div class="col-md-6 form-group">
                                  <label class="control-label">Description:</label>
                                  <textarea placeholder="Description..." class="form-control" name="desc" id="desc" rows="4"></textarea>
                              </div>
                              <div class="col-md-12 form-group user-form-group">
                                 <div class="pull-right">
                                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-add btn-sm">Save</button>
                                 </div>
                              </div>
                           </fieldset>
                           <? echo form_close(); ?>
                        </div>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                  </div>
               </div>
               <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
         </div>
         <!-- /// end new receiveable -->
      </section>
      <!-- /.content -->
   </div>

   <script type="text/javascript">
 ///// received amount get ajax
  function received_amount(invo_id)
 {
  $.ajax({
    type: 'POST',
    url: '<?php echo base_url('get_personal_receivables');?>',
    dataType: 'json',
    data: {invo_id:invo_id},
    success: function(response){
      $("#name").val(response.name);
      $("#contact").val(response.contact);
      $("#address").val(response.address);
      $("#amount").val(response.amount);
      $("#paid_by").val(response.paid_by);
      var reciev_amt = parseFloat(response.amount);
      $("#receivable_am").val(reciev_amt);
      $("#old_img").val(response.image);
       $("#date").val(response.date);
      $("#show_old_img").attr('src',"<?php echo base_url('assets/images/receivables/')?>"+response.image);
      $("#receivable_id").val(response.receivable_id);
      $("#desc").val(response.description);

    }
  });
 }
 $(".pop").on("click", function() {
   $('#imagepreview').attr('src', $(this).find('img').attr('src')); // here asign the image to the modal when the user click the enlarge link
   $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
});

$("#update_receive").click(function(){
  var receivable_am = parseFloat($("#receivable_am").val());
  var receiving_amt = parseFloat($("#receiving_amt").val());
  if(receiving_amt > receivable_am)
  {
    alert("Pease enter valid amount !");
    return false;
  }
  return true;
});


 $(document).ready(function(){
    setTimeout(function(){ $("#add_success").fadeOut(); }, 4000);
    setTimeout(function(){ $("#add_fail").fadeOut(); }, 4000);
});

 $('.receive_date').datetimepicker({
 timepicker:false,
 format:'d-m-Y'
});
</script>