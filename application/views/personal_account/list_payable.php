
<style>
body {font-family: Arial, Helvetica, sans-serif;}

#myImg {
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.img_modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 0px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.show-model-content {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
}

/* Caption of Modal Image */
#caption {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
  text-align: center;
  color: #ccc;
  padding: 10px 0;
  height: 150px;
}

/* Add Animation */
.show-model-content, #caption {  
  -webkit-animation-name: zoom;
  -webkit-animation-duration: 0.6s;
  animation-name: zoom;
  animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
  from {-webkit-transform:scale(0)} 
  to {-webkit-transform:scale(1)}
}

@keyframes zoom {
  from {transform:scale(0)} 
  to {transform:scale(1)}
}

/* The Close Button */
.close {
  position: absolute;
  top: 15px;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.close:hover,
.close:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
  .show-model-content {
    width: 100%;
  }
}
</style>

<!-- //// end image modal /// -->



 <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-file-text-o"></i>
      </div>
      <div class="header-title">
         <h1>Payables</h1>
         <small>Payables List</small>
         <?php if(!empty($this->session->flashdata('add_success'))): ?>
          <span id="updatemsg" style="color: green; text-align: right; float: right;"><? echo $this->session->flashdata('add_success');?></span>
       <?php endif;?>
        <?php if(!empty($this->session->flashdata('add_fail'))): ?>
          <span id="updatemsg" style="color: red; text-align: right; float: right;"><? echo $this->session->flashdata('add_fail');?></span>
       <?php endif;?>
    </div>
 </section>
 <!-- Main content -->
 <section class="content">
   <div class="row">
      <div class="col-sm-12">
         <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
               <div class="btn-group" id="buttonexport">
                  <!-- <a href="<?php //echo base_url('add_payable')?>">
                     <button class="btn btn-primary">Add Payable</button>

                  </a> -->
                  <a href="javascript:;" id="btn_action" class="btn btn-add btn-sm" data-toggle="modal" data-target="#customer2">Add Payable</a>
               </div>
            </div>
            <div class="panel-body">
               <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->

               <!-- ./Plugin content:powerpoint,txt,pdf,png,word,xl -->
               <div class="table-responsive">
                  <table id="example" class="table table-striped table-bordered" style="width:100%">
                     <!-- <table id="dataTableExample1" class="table table-bordered table-striped table-hover"> -->
                        <thead>
                           <tr class="info">
                              <th>Date</th>
                              <th>Name</th>
                              <th>Contact</th>
                              <th>Address</th>
                              <th>Amount</th>
                              <th>Received From</th>
                              <th>Image</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                          <?php if (!empty($payables) && $payables>0) {
                            foreach($payables as $payable){ ?>
                             <tr>
                              <td><?php echo formated_date($payable->date,'d-m-Y');?></td>
                              <td><?php echo $payable->name;?></td>
                              <td><?php echo $payable->contact;?></td>
                              <td><?php echo $payable->address;?></td>
                              <td><?php echo round($payable->amount,2);?></td>
                              <td><?php echo $payable->received_from;?></td>
                              <td>
                                <a href="javascript:;" class="pop">
                                <img src="<?php echo base_url();?>/assets/images/payable/<?php echo $payable->image;?>" alt="payable image.." class="image_show" style="height: 60px;width: 60px">
                                </a>
                              </td>
                              <td>
                                 <a href="javascript:;" onclick="paid_amount(<?php echo $payable->payable_id;?>)" id="btn_action" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#customer1">Unpaid</a>
                              </td> 
                              </tr>
                              <?php } } ?>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <!-- //// IMAGE SHOW MODAL /// -->
                     <div class="img_modal fade" id="imagemodal" style="z-index: 9999;" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="show-model-content">
                 
                  <div class="modal-body" style="padding: 5px !important;">
                    <img src="" id="imagepreview" style="width: 468px; height: 470px;" >
                  </div>
                 
                </div>
              </div>
            </div>
            <!-- /// END IMAGE SHOW MODAL /// -->
         <!-- quote Modal1 -->
         <!--- Edit modal starts -->
         <div class="modal fade" id="customer1" tabindex="-2" style="z-index: 999999;" role="dialog2" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header modal-header-primary">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                     <h3><i class="fa fa-user m-r-5"></i> Update Payable </h3>
                  </div>
                  <div class="modal-body">
                     <div class="row">
                        <div class="col-md-12">
                           <?php echo form_open("upadate_payables_exe", array('name' => 'update_payable', 'id' =>'update_payable', 'enctype'=>'multipart/form-data'));?>
                           <fieldset>
                              <!-- Text input-->
                              <div class="col-md-6 form-group">
                                 <label class="control-label">Name</label>
                                 <input type="text" id="name" name="name" required placeholder="Name" class="form-control">
                              </div>
                              <div class="col-md-6 form-group">
                                 <label class="control-label">Contact</label>
                                 <input type="text" id="contact" name="contact" required placeholder="contact" class="form-control">
                              </div>
                              <div class="col-md-6 form-group">
                                 <label class="control-label">Address</label>
                                 <input type="text" id="address" name="address" required placeholder="address" class="form-control">
                              </div>
                               <div class="col-md-6 form-group">
                                 <label class="control-label">Received From</label>
                                 <input type="text" id="recivedfrom" name="recivedfrom" required placeholder="received from" class="form-control">
                              </div>
                              <div class="col-md-6 form-group">
                                 <label class="control-label">Payable Amount</label>
                                 <input type="number" step="0.0001" readonly id="payable_amt" name="payable_amt" placeholder="amount" class="form-control">
                              </div>
                              <div class="col-md-6 form-group">
                                 <label class="control-label">Pay Amount</label>
                                 <input type="number" step="0.0001" id="pay_amt" name="pay_amt" required placeholder="amount" class="form-control">
                              </div>
                              <div class="col-md-6 form-group">
                                 <label>Date</label>
                                 <input type="date" class="form-control pay_date" required placeholder="Enter date"
                                 name="date" id="date">
                                 
                              </div>
                              <div class="col-md-6 form-group">
                                 <label class="control-label">File</label>
                                 <input type="file" id="photo" name="photo" placeholder="file" class="form-control">
                                 <input type="hidden" name="old_img" id="old_img">
                                
                                 <img src="" id="show_old_img" height="80px" width="80px" style="margin-top: 10px;" alt="Payable Image...">
                               
                              </div>
                              <input type="hidden" name="payableId" id="payableId">
                              <div class="col-md-6 form-group">
                                  <label class="control-label">Description:</label>
                                  <textarea placeholder="Description..." class="form-control" name="desc" id="desc" rows="4"></textarea>
                              </div>
                              <div class="col-md-12 form-group user-form-group">
                                 <div class="pull-right">
                                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-add btn-sm" id="update_pay" >Update</button>
                                 </div>
                              </div>
                           </fieldset>
                           <? echo form_close(); ?>
                        </div>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                  </div>
               </div>
               <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
         </div>
         <!--- Edit modal end -->

         <!--- Add New Payable modal starts -->

          <div class="modal fade" id="customer2" tabindex="-2" style="z-index: 999999;" role="dialog2" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header modal-header-primary">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                     <h3><i class="fa fa-user m-r-5"></i> Add Payable </h3>
                  </div>
                  <div class="modal-body">
                     <div class="row">
                        <div class="col-md-12">
                           <?php echo form_open("store_payable", array('name' => 'add_payable', 'id' =>'add_payable', 'enctype'=>'multipart/form-data'));?>
                           <fieldset>
                              <!-- Text input-->
                              <div class="col-md-6 form-group">
                                 <label class="control-label">Name</label>
                                 <input type="text" id="name" required name="name" required  placeholder="Name" class="form-control">
                              </div>
                              <div class="col-md-6 form-group">
                                 <label class="control-label">Contact</label>
                                 <input type="number" id="contact" name="contact" required placeholder="contact" class="form-control">
                              </div>
                              <div class="col-md-6 form-group">
                                 <label class="control-label">Address</label>
                                 <input type="text" id="address" name="address" required placeholder="address" class="form-control">
                              </div>
                              <div class="col-md-6 form-group">
                                 <label class="control-label">Amount</label>
                                 <input type="number" step="0.0001" id="amount" name="amount" required placeholder="amount" class="form-control">
                              </div>
                              <div class="col-md-6 form-group">
                                 <label class="control-label">Received From</label>
                                 <input type="text" id="recivedfrom" name="recivedfrom" required placeholder="received from" class="form-control">
                              </div>
                              <div class="col-md-6 form-group">
                                 <label>Date</label>
                                 <input type="date" class="form-control pay_date" required placeholder="Enter date"
                                 name="date" id="date">
                                 
                              </div>
                              <div class="col-md-6 form-group">
                                 <label class="control-label">File</label>
                                 <input type="file" id="photo" name="photo" placeholder="file" class="form-control">   
                              </div>
                              <div class="col-md-6 form-group">
                                  <label class="control-label">Description:</label>
                                  <textarea placeholder="Description..." class="form-control" name="desc" id="desc" rows="4"></textarea>
                              </div>
                              <div class="col-md-12 form-group user-form-group">
                                 <div class="pull-right">
                                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-add btn-sm">Save</button>
                                 </div>
                              </div>
                           </fieldset>
                           <? echo form_close(); ?>
                        </div>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                  </div>
               </div>
               <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
         </div>
      </section>
      <!-- /.content -->
   </div>

   <script type="text/javascript">
    function paid_amount(invo_id)
 {
  $.ajax({
    type: 'POST',
    url: '<?php echo base_url('get_personal_payables');?>',
    dataType: 'json',
    data: {invo_id:invo_id},
    success: function(response){
      $("#name").val(response.name);
      $("#contact").val(response.contact);
      $("#address").val(response.address);
      var pable_amt = parseFloat(response.amount);
      $("#payable_amt").val(pable_amt);
      $("#recivedfrom").val(response.received_from);
      $("#old_img").val(response.image);
      $("#date").val(response.date);
      $("#show_old_img").attr('src',"<?php echo base_url('assets/images/payable/')?>"+response.image);
      $("#payableId").val(response.payable_id);
      $("#desc").val(response.description);

    }
  });
 }
$(".pop").on("click", function() {
   $('#imagepreview').attr('src', $(this).find('img').attr('src')); // here asign the image to the modal when the user click the enlarge link
   $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
});

$("#update_pay").click(function(){
  var payable_amt = parseFloat($("#payable_amt").val());
  var pay_amt = parseFloat($("#pay_amt").val());
  if(pay_amt > payable_amt)
  {
    alert("Pease enter valid amount !");
    return false;
  }
  return true;
});
$(document).ready(function(){
    setTimeout(function(){ $("#updatemsg").fadeOut(); }, 4000);
});
$('.pay_date').datetimepicker({
 timepicker:false,
 format:'d-m-Y'
});
</script>
