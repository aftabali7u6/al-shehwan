<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-dashboard"></i>
      </div>
      <div class="header-title">
         <h1>Admin Dashboard</h1>
         <small>Very detailed & featured admin.</small>
      </div>
   </section>
   <style >
   .img-car{
      width: 100%;
   }
</style>
<!-- Main content -->
<section class="content">
 <div class="row">
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4" id="payable">
                     <div id="cardbox1">
                        <div class="statistic-box">
                           <i class="fa fa-user-plus fa-3x"></i>
                           <div class="counter-number pull-right">
                              <span class="count-number">
                                 <?php echo $amount_payable[0]['amount']; ?>
                              </span> 
                              <span class="slight"><i class="fa fa-play fa-rotate-270"> </i>
                              </span>
                           </div>
                           <h3> Total Payables</h3>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4" id="receive">
                     <div id="cardbox2">
                        <div class="statistic-box">
                           <i class="fa fa-user-secret fa-3x"></i>
                           <div class="counter-number pull-right">
                              <span class="count-number">
                                 <?php echo $amount_receivable[0]['amount']; ?>
                              </span> 
                              <span class="slight"><i class="fa fa-play fa-rotate-270"> </i>
                              </span>
                           </div>
                           <h3>  Total Receivables</h3>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                     <div id="cardbox3">
                        <div class="statistic-box">
                           <i class="fa fa-money fa-3x"></i>
                           <div class="counter-number pull-right">
                              <i class="ti ti-money"></i><span class="count-number">
                                    <?php $amount_recev = $amount_receivable[0]['amount'];
                                     $amount_pay = $amount_payable[0]['amount'];
                                       if($amount_recev > $amount_pay){
                                             echo $amount_recev-$amount_pay;
                                       }
                                       else
                                       {
                                          echo $amount_pay-$amount_recev;
                                       }?>
                              </span> 
                              <span class="slight"><i class="fa fa-play fa-rotate-270"> </i>
                              </span>
                           </div>
                           <h3>  
                              <?php $amount_recev = $amount_receivable[0]['amount'];
                                     $amount_pay = $amount_payable[0]['amount'];
                                       if($amount_recev > $amount_pay){
                                             echo "Receivables";
                                       }
                                       else
                                       {
                                          echo "Payables";
                                       }?>
                                          
                                       </h3>
                        </div>
                     </div>
                  </div>
               </div>
               
               <!-- /// list payable code -->
   <div class="row payable_tbl">
      <div class="col-sm-12">
         <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
               <div class="btn-group" id="buttonexport">
               <h4>Payables List</h4>
            </div>
            </div>
            <div class="panel-body">
               <div class="table-responsive">
                  <table id="example" class="table table-striped table-bordered" style="width:100%">
                     <!-- <table id="dataTableExample1" class="table table-bordered table-striped table-hover"> -->
                        <thead>
                           <tr class="info">
                              <th>Date</th>
                              <th>Name</th>
                              <th>Contact</th>
                              <th>Address</th>
                              <th>Amount</th>
                              <th>Received From</th>
                              <th>Image</th>
                           </tr>
                        </thead>
                        <tbody>
                          <?php if (!empty($payables) && $payables>0) {
                            foreach($payables as $payable){ ?>
                             <tr>
                              <td><?php echo formated_date($payable->date,'d-m-Y');?></td>
                              <td><?php echo $payable->name;?></td>
                              <td><?php echo $payable->contact;?></td>
                              <td><?php echo $payable->address;?></td>
                              <td><?php echo round($payable->amount,2);?></td>
                              <td><?php echo round($payable->received_from,2);?></td>
                              <td>
                                <a href="javascript:;" class="pop">
                                <img src="<?php echo base_url();?>/assets/images/payable/<?php echo $payable->image;?>" alt="payable image.." class="image_show" style="height: 60px;width: 60px">
                                </a>
                              </td>
                              </tr>
                              <?php } } ?>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>


         <!-- ///list payable code end -->


         <!-- /// list receiveable start  -->
         <div class="row receive_tbl" style="display: none;">
      <div class="col-sm-12">
         <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
               <div class="btn-group" id="buttonexport">
               <h4>Receivable List</h4>
            </div>
            </div>
            <div class="panel-body">
               <div class="table-responsive">
                  <table id="example2" class="table table-striped table-bordered" style="width:100%">
                     <!-- <table id="dataTableExample1" class="table table-bordered table-striped table-hover"> -->
                        <thead>
                           <tr class="info">
                              <th>Date</th>
                              <th>Name</th>
                              <th>Contact</th>
                              <th>Address</th>
                              <th>Amount</th>
                              <th>Paid By</th>
                              <th>Image</th>
                           </tr>
                        </thead>
                        <tbody>
                          <?php if (!empty($receivables) && $receivables>0) {
                            foreach($receivables as $receivable){ ?>
                             <tr>
                              <td><?php echo formated_date($receivable->date,'d-m-Y');?></td>
                              <td><?php echo $receivable->name;?></td>
                              <td><?php echo $receivable->contact;?></td>
                              <td><?php echo $receivable->address;?></td>
                              <td><?php echo $receivable->amount;?></td>
                              <td><?php echo $receivable->paid_by;?></td>
                              <td>
                                <a href="javascript:;" class="pop">
                                <img src="<?php echo base_url();?>/assets/images/receivables/<?php echo $receivable->image;?>" alt="receivable image.." class="image_show" style="height: 60px;width: 60px">
                                </a>
                              </td>
                              </tr>
                              <?php } } ?>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
              
      </section>
            <!-- /.content -->
   </div>
<script type="text/javascript">
 
   $("#payable").click(function(){
      $(".payable_tbl").css('display','block');
      $(".receive_tbl").css('display','none');
   });
    $("#receive").click(function(){
      $(".payable_tbl").css('display','none');
      $(".receive_tbl").css('display','block');
   });
</script>
 