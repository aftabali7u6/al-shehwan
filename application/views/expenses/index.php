 <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-file-text-o"></i>
               </div>
               <div class="header-title">
                  <h1>Products</h1>
                  <small>Products List</small>
                  <?php if(!empty($this->session->flashdata('add_success'))): ?>
                    <span id="add_success" style="color: green; text-align: right; float: right;"><? echo $this->session->flashdata('add_success');?></span>
                 <?php endif;?>
                  <?php if(!empty($this->session->flashdata('add_fail'))): ?>
                    <span id="add_fail" style="color: red; text-align: right; float: right;"><? echo $this->session->flashdata('add_fail');?></span>
                 <?php endif;?>
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonexport">
                                 <button type="button" class="btn btn-add btn-sm" data-toggle="modal" data-target="#customer1" >Add Expense</button>
                           </div>
                        </div>
                        <div class="panel-body">
                        <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->
                         
                           <!-- ./Plugin content:powerpoint,txt,pdf,png,word,xl -->
                           <div class="table-responsive">
                              <table id="dataTableExample1" class="table table-bordered table-striped table-hover">
                                 <thead>
                                    <tr class="info">
                                       <th>Sr. No</th>
                                       <th>Date</th>
                                       <th>Expense category</th>
                                       <th>Expense Amount</th>
                                       <th>Description</th>
                                       <!-- <th>Created by</th> -->
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php $sr_no = 1;?>
                                    <?php  if(isset($expense)){foreach($expense as $pro){ ?>
                                     <tr>
                                       
                                       <td><?php echo $sr_no;?></td>
                                       <?php $sr_no++; ?>
                                       <td><?php echo $pro->exp_date;?></td>
                                       <td><?php echo $pro->cat_name;?></td>
                                       <td><?php echo $pro->amount;?></td>
                                       <td><?php echo $pro->exp_description;?></td>
                                     <!--   <td><?php //echo $pro->created_by;?></td> -->
                                       <!-- <td><?php //echo $pro->updated_by;?></td>
                                       <td><?php //echo $pro->created_at;?></td>
                                       <td><?php //echo $pro->updated_at;?></td>
                                       <?php //if($pro->status == '1'): ?>
                                       <td><span class="label label-success">Active</span>
                                       <?php //else: ?>
                                          <span class="label label-danger">Deactive</span>
                                       <?php //endif; ?> 
                                       <input type="hidden" value="<? //echo $pro->exp_id; ?>" id="edit_id">-->
                                       <td>
                                          <button type="button" class="btn btn-add btn-sm" data-toggle="modal" data-target="#customer1"><i class="fa fa-pencil"></i></button>
                                         <!--  <button type="button" class="btn btn-add btn-sm" data-toggle="modal" data-target="#customer1" onclick="edit_pro(<?php //echo $pro->pro_id;?>);"><i class="fa fa-pencil"></i></button> -->
                                          <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#customer2" ><i class="fa fa-trash-o"></i> </button>
                                          <!-- <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#customer2" 
                                          onclick="del_pro(<?php //echo $pro->pro_id;?>)"><i class="fa fa-trash-o"></i> </button> -->
                                       </td>
                                       <?php } } ?>
                                    </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- quote Modal1 -->
               <div class="modal fade" id="customer1" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog">
                     <div class="modal-content">
                        <div class="modal-header modal-header-primary">
                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                           <h3><i class="fa fa-user m-r-5"></i> Add New Expense</h3>
                        </div>
                        <div class="modal-body">
                           <div class="row">
                              <div class="col-md-12">
                                 <? echo form_open("new_expense_exe", array('name' => 'new_expense', 'id' =>
                                 'new_expense'));?>
                                    <fieldset>
                                       <!-- Text input-->
                                       <div class="col-md-6 form-group">
                                          <label class="control-label">Expense Category</label>
                                          <input type="text" id="cat_name" name="cat_name" placeholder="Expense Type" class="form-control" required>
                                          <input type="hidden" name="cat_exp_id" id="cat_exp_id">
                                       </div>
                                       <div class="form-group col-md-6">
                                          <label>Invoice Date</label>
                                          <input  type="date" name="exp_date" id="exp_date" required class="form-control" placeholder="Enter Date...">
                                        </div>
                                        <div class="form-group col-md-6">
                                          <label>Amount</label>
                                          <input type="number" step="0.0001" class="form-control" required name="amount" id="amount" placeholder="Enter expense amount...">
                                      </div>

                                       <!-- <div class="col-md-6 form-group">
                                          <label>Status</label>
                                             <select class="form-control" id="state" name="state">
                                                <option value="1">Active</option>
                                                <option value="0">Deactive</option>
                                             </select>
                                       </div> -->
                                       <!-- Text input-->
                                        <div class="form-group col-md-6">
                                          <label>Product Description</label>
                                          <textarea class="form-control" name="desc" id="desc" rows="4" placeholder="Enter Product Description..."></textarea>
                                       </div>
                                       <div class="col-md-12 form-group user-form-group">
                                          <div class="pull-right">
                                             <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                                             <button type="submit" class="btn btn-add btn-sm">Save</button>
                                          </div>
                                       </div>
                                    </fieldset>
                                 <? echo form_close(); ?>
                              </div>
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                        </div>
                     </div>
                     <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
               </div>
               <!-- /.modal -->
               <!-- Modal -->   
               <!-- quote delete Modal2 -->
               <div class="modal fade" id="customer2" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog">
                     <div class="modal-content">
                        <div class="modal-header modal-header-primary">
                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                           <h3><i class="fa fa-user m-r-5"></i> Delete Product</h3>
                        </div>
                        <div class="modal-body">
                           <div class="row">
                              <div class="col-md-12">
                                 <? echo form_open("delete_product", array('name' => 'del_form',
                                 'id' => 'del_form', 'class' => 'form-horizontal'));?>
                                 <!-- <form class="form-horizontal"> -->
                                    <fieldset>
                                       <div class="col-md-12 form-group user-form-group">
                                          <label class="control-label">Delete Product</label>
                                          <input type="hidden" name="del" id="del">
                                          <div class="pull-right">
                                             <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">NO</button>
                                             <button type="submit" class="btn btn-add btn-sm">YES</button>
                                          </div>
                                       </div>
                                    </fieldset>
                                <? echo form_close();?>
                              </div>
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                        </div>
                     </div>
                     <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
               </div>
               <!-- /.modal -->
            </section>
            <!-- /.content -->
         </div>

         <script type="text/javascript">        
      function cat_fun(cat_ex_id)
      {
        console.log('again calll');
         $.ajax({
            type: 'POST',
            url: '<?php echo base_url('cat_info_get'); ?>',
            data: {cat_ex_id:cat_ex_id},
            dataType: 'json',
            success: function(response){
              $("#cat_exp_id").val(response.cat_id);
           }
        });
         
      }
      $(function(){
        $( "#cat_name" ).autocomplete({
         source: "<?php echo base_url('cat_info');?>",
         minLength: 2,
         select: function( event, ui ) {
          console.log('payment id',ui.item.id);
            cat_fun(ui.item.id);
        // console.log( "Selected: " + ui.item.value + " aka " + ui.item.id );

          }
        });
     });





           function edit_pro(pro_id)
           {
        
            $.ajax({
               type: 'POST',
               url: '<?php echo base_url('get_single_product_ajax'); ?>',
               data: {pro_id:pro_id},
               dataType: 'json',
               success: function(response){
                  if(response.flag){

                     $('#update_id').val(response.data.pro_id);
                     $('#pro_name').val(response.data.name);
                     // console.log(response.data.status);
                   $('#state').val(response.data.status);
                   $('#description').val(response.data.description);

                  }
               }
            });
           }

           function del_pro(del_id)
           {
         $('#del').val(del_id);
           }

           //// msg ajax
           $(document).ready(function(){
    setTimeout(function(){ $("#add_success").fadeOut(); }, 4000);
    setTimeout(function(){ $("#add_fail").fadeOut(); }, 4000);
});


  </script>

         <style type="text/css">
           .ui-autocomplete{
            z-index: 9999 !important;
           }
         </style>