


<!-- //// end image modal /// -->



 <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-file-text-o"></i>
      </div>
      <div class="header-title">
         <h1>Qoutation</h1>
         <small>Qoutation List</small>
         <?php if(!empty($this->session->flashdata('add_success'))): ?>
          <span id="updatemsg" style="color: green; text-align: right; float: right;"><? echo $this->session->flashdata('add_success');?></span>
       <?php endif;?>
        <?php if(!empty($this->session->flashdata('add_fail'))): ?>
          <span id="updatemsg" style="color: red; text-align: right; float: right;"><? echo $this->session->flashdata('add_fail');?></span>
       <?php endif;?>
    </div>
 </section>
 <!-- Main content -->
 <section class="content">
   <div class="row">
      <div class="col-sm-12">
         <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
               <div class="btn-group" id="buttonexport">
                  <!-- <a href="<?php //echo base_url('add_payable')?>">
                     <button class="btn btn-primary">Add Payable</button>

                  </a> -->
                  <a href="<?php echo base_url('new_quotation'); ?>" id="btn_action" class="btn btn-add btn-sm">New Qoutation</a>
               </div>
            </div>
            <div class="panel-body">
               <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->

               <!-- ./Plugin content:powerpoint,txt,pdf,png,word,xl -->
              <div class="panel-body">
                              <div class="table-responsive" id="">
                                 <table id="example" class="table table-striped table-bordered">
                                    <thead>
                                       <tr class="info">
                                          <th>Dated</th>
                                          <th>Invoice No.</th>
                                          <th>Company Name</th>
                                          <th>VAT #</th>
                                          <th>Amount</th>
                                          <th>Vat Amount</th>
                                          <th>Total</th>
                                          <th>Status</th>
                                       </tr>
                                    </thead>
                                    <tbody id="datatablebody">
                                       <? if(isset($qoutes) && !empty($qoutes)):?>

                                       <? foreach($qoutes as $qoute): ?>
                                         <tr>
                                          <td><? echo $qoute->invoice_date;?></td>
                                          <td><? echo $qoute->invoice_no;?></td>
                                          <td style="text-align: center;"><? echo $qoute->company_name;?></td>
                                          <td><? echo $qoute->vat_no;?></td>
                                          <td><? echo $qoute->total_exec_vat;?></td>
                                          <td><? echo $qoute->vat_sar;?></td>
                                          <td><? echo $qoute->total_amount;?></td>
                                          <?php if($qoute->invo_status == 0): ?>
                                          <td><span class="label-danger label label-default" >    Pending</span>
                                          </td>
                                          <?php else: ?>
                                          <td><span class="label-custom label label-default" >Completed</span>
                                          </td>
                                        <?php endif; ?>
                                       <? endforeach; ?>
                                    <? endif;?>
                                 </tbody>
                              </table>
                           </div>
                        </div>
               </div>
            </div>
         </div>

         <!-- //// IMAGE SHOW MODAL /// -->
      </section>
   </div>

   <script type="text/javascript">
$(document).ready(function(){

    setTimeout(function(){ $("#updatemsg").fadeOut(); }, 4000);
    setTimeout(function(){ $("#add_fail").fadeOut(); }, 4000);
});

</script>
