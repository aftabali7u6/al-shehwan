 <div class="login-wrapper">
            
            <div class="container-center">
            <div class="login-area">
                <div class="panel panel-bd panel-custom">
                    <div class="panel-heading">
                        <div class="view-header">
                            <div class="header-icon">
                                <i class="pe-7s-unlock"></i>
                            </div>
                            <div class="header-title">
                                <h3>Login</h3>
                                <small><strong>Please enter your credentials to login.</strong></small>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <?php echo form_open('user/login', array('name' => 'loginForm', 'id' => 'loginForm')); ?>
                        <!-- <form action="http://crm.thememinister.com/crmAdmin/index.html" id="loginForm" novalidate> -->
                            <div class="form-group">
                                <label class="control-label" for="username">Username</label>
                                <input type="text" placeholder="Name" title="Please enter you username" required="" value="" name="name" id="name" class="form-control">
                                <span class="help-block small">Your unique username to app</span>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="password">Password</label>
                                <input type="password" title="Please enter your password" placeholder="******" required="" value="" name="password" id="password" class="form-control">
                                <span class="help-block small">Your strong password</span>
                            </div>
                            <div>
                                <button class="btn btn-add">Login</button>
                                &nbsp;&nbsp;
                                <?php if($this->session->flashdata('login_msg')): ?>
                                <span style="color: red;"><?php echo $this->session->flashdata('login_msg'); ?></span>
                            <?php endif; ?>
                            </div>
                        <?php echo form_close(); ?>
                        <!-- </form> -->
                        </div>
                        </div>
                </div>
            </div>
        </div>