 <div class="content-wrapper">
     
            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-suitcase"></i>
               </div>
               <div class="header-title">
                  <h1>Add New Employee</h1>
                  <small>New Employee Details</small>
               </div>
               <div class="btn btn-success" id="click_print">Print</div>
            </section>
            <!-- Main content -->
            <div id="print_data"  >
            <section class="content" style="background-color: white;">
    <style>
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 21px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
       /* line-height: inherit;*/
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        /*padding-bottom: 20px;*/
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        /*line-height: 45px;*/
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 0px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 0px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    </style>
    <div class="invoice-box" style="background-color: white;">

        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                             <td  style="font-size: 13px;color: blue;font-weight: bold;">
                                Ziyad Bin Ahmad Bin Ibrahim Al Shahwan Trading Est.<br>
                                Wholesale of building materials and scrap <br>
                                C.R.:20501241159 :س  .ت 
                            </td>

                            <td  style="font-size: 13px;color: blue;font-weight: bold;">
                                 مؤسسة زياد بن احمد بن ابراهيم الشهوان التجارية   <br>
                                االجملة من مواد البناء والخردة   <br>
                                V.A.T- No: 300409886200003 : الرقيم زريباتا
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td style="padding: 0px !important;">
                                <hr style="border:2px solid blue;padding: 0px;">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr >
              <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                             <pre style="text-align: center;background-color: lightgrey;border:1px solid black;font-weight: bold;border-radius: 0px;color: blue;">  VAT INVOICE 
                                 فاتورة ضريبة القيمة المضافة  </pre>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr >
                <td colspan="3">
                    <table>
                        <tr>
                            <td  style="font-size: 13px;color: blue;font-weight: bold;">
                                VENDORS DETAILS:<br>
                                BILL TO:<br>
                                INVOICE DATE:<br>
                                INVOICE NUMBER:<br>
                                ADDRESS:<br>
                                V.A.T- No:
                            <td style="font-size: 13px;color: blue;font-weight: bold;text-align: center;">
                              <br>
                              مسلمة حديد متمردة  <br>
                              20-April-2019<br>
                              003<br>
                              الدمام  <br>
                              3005706914000003
                            </td>
                            <td  style="font-size: 13px;color: blue;font-weight: bold;float: right;">
                                تفاصيل البائعين:<br>
                                فاتورة الى:  <br>
                                تاريخ الفاتورة: <br>
                                رقم الفاتورة: <br> 
                                عنوان: <br>
                                ضريبة القيمة المضافة لا:
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="information">
                <td colspan="2  ">
                    <table style="border:1px solid blue;color: blue;">
                        <tr style="border:1px solid blue;text-align: center;">
                            <th style="border:1px solid blue;text-align: center;">Sr. #</th>
                            <th style="border:1px solid blue;text-align: center;">Description   <span style="">التفصيل  </span></th>
                            <th style="border:1px solid blue;text-align: center;">Weight  <span style=" ">وزن </span> <hr style="border:1px solid blue;">KG<span style=" ">كلو</span></th>
                            <th style="border:1px solid blue;text-align: center;">Price<br><br>
                            السعر  </th>
                            <th style="border:1px solid blue;text-align: center;">Total  مجموع   </th>
                        </tr>
                        <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;">1</td>
                          <td style="border:1px solid blue; text-align: left;">1 <span style="text-align: right; padding-left: 300px;">sdjkf</span></td>
                          <td style="border:1px solid blue;">1</td>
                          <td style="border:1px solid blue;">1</td>
                          <td style="border:1px solid blue;">1</td>
                        </tr>
                        <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;">1</td>
                          <td style="border:1px solid blue;"></td>
                          <td style="border:1px solid blue;"></td>
                          <td style="border:1px solid blue;"></td>
                          <td style="border:1px solid blue;"></td>
                        </tr>
                        <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;">1</td>
                          <td style="border:1px solid blue;"></td>
                          <td style="border:1px solid blue;"></td>
                          <td style="border:1px solid blue;"></td>
                          <td style="border:1px solid blue;"></td>
                        </tr>
                        <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;">1</td>
                          <td style="border:1px solid blue;"></td>
                          <td style="border:1px solid blue;"></td>
                          <td style="border:1px solid blue;"></td>
                          <td style="border:1px solid blue;"></td>
                        </tr>
                        <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;">1</td>
                          <td style="border:1px solid blue;"></td>
                          <td style="border:1px solid blue;"></td>
                          <td style="border:1px solid blue;"></td>
                          <td style="border:1px solid blue;"></td>
                        </tr>
                        <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;">1</td>
                          <td style="border:1px solid blue;"></td>
                          <td style="border:1px solid blue;"></td>
                          <td style="border:1px solid blue;"></td>
                          <td style="border:1px solid blue;"></td>
                        </tr>
                        <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;">1</td>
                          <td style="border:1px solid blue;"></td>
                          <td style="border:1px solid blue;"></td>
                          <td style="border:1px so lid blue;"></td>
                          <td style="border:1px solid blue;"></td>
                        </tr>
                        <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;">1</td>
                          <td style="border:1px solid blue;"></td>
                          <td style="border:1px solid blue;"></td>
                          <td style="border:1px solid blue;"></td>
                          <td style="border:1px solid blue;"></td>
                        </tr>
                         <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;text-align: center;color: blue;"  colspan="2">Total  <span>مجموع  </span></td>
                          <td style="border:1px solid blue;text-align: center;">2700</td>
                          <td style="border:1px solid blue;text-align: center;"></td>
                          <td style="border:1px solid blue;text-align: center;">18,765</td>
                        </tr>
                        <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;text-align: center;color: blue;" colspan="2">VAT % 5 <span>5</span><span>ضريبة القيمة المضافة </span> </td>
                          <td style="border:1px solid blue;text-align: center;color: blue;" colspan="3">938.25</td>
                        </tr>
                        <tr style="border:1px solid blue;">
                          <td style="border:1px solid blue;text-align: center;color: blue;" colspan="2">Grand Total <span>ضالمجموع الكلي  </span> </td>
                          <td style="border:1px solid blue;text-align: center;color: blue;" colspan="3">19703.25</td>
                        </tr>
                    </table>
                </td>
            </tr> 
            <br>
            <br>
            <tr >
                <td style="color: blue;font-weight: bold;">
                  <br>
                  <br>
                  <br>
                    Manager Signature / توقيع المدير  
                </td>
                
                <td style="color: blue;font-weight: bold;">
                  <br>
                  <br>
                  <br>
                   Customer Signature / توقيع العملاء  
                </td>
            </tr>
            
            <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                <hr style="border:2px solid blue;">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="font-size: 13px !important;color: blue;">
                <td colspan="3">
                    <table>
                        <tr >
                           الجوال: 0507975931 - 0549388266 - هاتف: 013 8281169 - صندوق البريد 003983 - الرمز البريدي
                            32441 - الدمام - شارع الملك سعود - منطقة الفيحاء  
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="font-size: 12px !important;color: blue;">
                <td colspan="3">
                    <table>
                        <tr >
                           Mobile:0507975931-0549388266-Telephone:013 8281169-P.O.Box 003983-Zip 32441-Dammam-King Saud Street-Fayhaa Area
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                <hr style="border:15px solid blue;">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
     </section>
               </div>
            <!-- /.content -->
         </div>
         <script type="text/javascript">
            function printData()
            {
               var divToPrint=document.getElementById("print_data");
               newWin= window.open("");
               newWin.document.write(divToPrint.outerHTML);
               newWin.print();
               newWin.close();
               $("#print_data").css('display','none');
            }
            $('#click_print').click(function(){
              $("#print_data").css('display','block');
               printData();
                 // $("#print_data").print();
            });
         </script>