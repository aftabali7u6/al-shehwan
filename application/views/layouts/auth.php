<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from crm.thememinister.com/crmAdmin/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 07 Jan 2019 04:04:13 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Al Shahwan Trading Est.</title>

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/dist/img/ico/favicon.png" type="image/x-icon">
        <!-- Bootstrap -->
        <link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css"/>
        <!-- Bootstrap rtl -->
        <!--<link href="assets/bootstrap-rtl/bootstrap-rtl.min.css" rel="stylesheet" type="text/css"/>-->
        <!-- Pe-icon-7-stroke -->
        <link href="<?php echo base_url('assets/pe-icon-7-stroke/css/pe-icon-7-stroke.css');?>" rel="stylesheet" type="text/css"/>
        <!-- style css -->
        <link href="<?php echo base_url('assets/dist/css/stylecrm.css');?>" rel="stylesheet" type="text/css"/>
        <!-- Theme style rtl -->
        <!--<link href="assets/dist/css/stylecrm-rtl.css" rel="stylesheet" type="text/css"/>-->
    </head>
    <body>
		<!-- begin:: Page -->
	{content}
		<!-- end:: Page -->

		  <script src="<?php echo base_url('assets/plugins/jQuery/jquery-1.12.4.min.js');?>" type="text/javascript"></script>
        <!-- bootstrap js -->
        <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js');?>" type="text/javascript"></script>
    </body>

<!-- Mirrored from crm.thememinister.com/crmAdmin/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 07 Jan 2019 04:04:13 GMT -->
</html>