 <style type="text/css">
 .underline{text-decoration: underline;}
</style>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-users"></i>
      </div>
      <div class="header-title">
         <h1>New Purchase Invoice</h1>
         <small>Purchase Invoice</small>
          <?php if($this->session->flashdata('pur_msg')): ?>
              <span id="pur_msg" style="color: green; text-align: right; float: right;color: #004085;background-color: #cce5ff;border-color: #b8daff;"><?php echo $this->session->flashdata('pur_msg');?></span>
           <?php endif;?>
           <?php if($this->session->flashdata('pur_msg_error')): ?>
              <span id="pur_msg_error" style="color: green; text-align: right; float: right;color: #004085;background-color: #cce5ff;border-color: #b8daff;"><?php echo $this->session->flashdata('pur_msg_error');?></span>
           <?php endif;?>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-12">
            <div class="panel panel-bd lobidrag">
               <div class="panel-heading">
                  <div class="btn-group" id="buttonlist"> 
                     <a class="btn btn-add " href="<? echo base_url('purchase_list');?>"> 
                        <i class="fa fa-list"></i>  New Purchase List </a>  
                  </div>
               </div>
               <div class="panel-body">
                  <!-- Tab panels -->
                  <div class="tab-content col-sm-12">
                     <div class="tab-pane fade in active" id="tab1">
                        <?php echo form_open_multipart("new_purchase_exe", array('name'=>'purchase_list',
                        'id'=>'purchase_list','onsubmit'=>'return verify_amt();'));?>
                        <div class="panel-body">

                           <div class="table-responsive">
                              <table class="table table-bordered table-striped table-hover">
                                 <div class=" col-md-12">
                                    <h2 class="underline">Vendor Information</h2>
                                 </div>
                                 <div class="form-group col-md-3">
                                    <label>VAT Number</label>
                                    <input type="number" id="vat_num" name="vat_num" class="form-control" placeholder="Enter VAT Number" required>
                                 </div>
                                 <div class="form-group col-md-3">
                                    <label>Company Name</label>
                                    <input type="text" style="text-align: right;" class="form-control" maxlength="40" required name="comp_name" id="comp_name" placeholder=" ...الرجاء إدخال اسم الشركة " required>
                                 </div>
                                 <div class="form-group col-md-3">
                                    <label>Contact Number</label>
                                    <input type="number" class="form-control" id="contact" name="contact" placeholder="Contact Number">
                                 </div>
                                 <div class="form-group col-md-3">
                                    <label>Address</label>
                                    <input type="text" class="form-control" style="text-align: right;" id="address" maxlength="40" name="address" placeholder="...عنوان الشركة">

                                 </div>
                                 
                              </table>
                           </div>

                        </div>

                        <div class="panel-body">

                           <div class="table-responsive">
                              <table class="table table-bordered table-striped table-hover">
                                 <div class=" col-md-12">
                                    <h2 class="underline">Invoice Details</h2>
                                 </div>
                                 <div class="form-group col-md-4">
                                    <label>Invoice Number</label>
                                    <input type="number" class="form-control" required name="invo_num" id="invo_num" placeholder="Invoice Number">
                                 </div>
                                 <div class="form-group col-md-4">
                                    <label>Invoice Date</label>
                                    <input  type="text" name="invo_date" id="invo_date" required class="form-control pur_date" placeholder="DD-MM-YYYY">
                                 </div>
                                 <div class="form-group col-md-4">
                                    <label>Sales Order Number</label>
                                    <input type="number" class="form-control" name="sale_ord_num" id="sale_ord_num" placeholder="Sales Order Number">
                                 </div>
                                 <div class="form-group col-md-4">
                                    <label>Sales Order Date</label>
                                    <input  type="text" class="form-control pur_date" name="sale_ord_date" id="sale_ord_date" placeholder="DD-MM-YYYY">
                                    
                                 </div>
                                 <div class="form-group col-md-4">
                                    <label>Delivery Note No.</label>
                                    <input type="number" class="form-control" name="deliv_note_num" id="deliv_note_num" placeholder="Enter Note Number">
                                 </div>
                                 <div class="form-group col-md-4">
                                    <label>Cust. Ticket Noumber</label>
                                    <input type="number" class="form-control" name="cust_tck_num" id="cust_tck_num" placeholder="Cust. Ticket No.">
                                 </div>
                                 
                              </table>
                           </div>

                        </div>
                        <div class="panel-body">

                           <div class="table-responsive">
                              <table class="table table-bordered table-striped table-hover">
                                 <div class=" col-md-12">
                                    <h2 class="underline">Product Details</h2>
                                 </div>
                                 <div  class="row count_form" id="clone_row" >
                                    <div class="form-group col-md-3">
                                       <label>Select Product</label>
                                       <select class="form-control" name="product[]" id="product" required>
                                          <option disabled selected>Please Select Product</option>
                                          <?php foreach ($product_list as $product) {?>
                                             <option value="<? echo $product->pro_id; ?>"><?php echo $product->name;?></option>
                                          <?php } ?>
                                       </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                       <label>Select Weight Unit</label>
                                       <select class="form-control" name="weight[]" id="weight" required>
                                          <option disabled selected>Please Select Product</option>
                                          <option value="1">MT</option>
                                          <option value="2">KG</option>
                                          <option value="3">TON</option>
                                       </select>
                                    </div>
                                    <div class="form-group col-md-2">
                                       <label>Quantity</label>
                                       <input type="number" step="0.0001" class="form-control quantity" name="quantity[]" id="quantity" required placeholder="Enter Product Quantity">
                                    </div>
                                    <div class="form-group col-md-2">
                                       <label>Rate</label>
                                       <input type="number" step="0.0001" class="form-control rate" name="rate[]" id="rate" placeholder="Enter Rate" required>
                                    </div>
                                    <div class="form-group col-md-2">
                                       <label>Discount</label>
                                       <input type="number" step="0.0001" class="form-control discount" name="discount[]" id="discount" placeholder="Enter Discount">
                                    </div>
                                    
                                    <div class="form-group col-md-3">
                                       <label>Total Exclusive VAT Amount</label>
                                       <input type="text" class="form-control total_exc_vat" name="total_exc_vat[]" id="total_exc_vat" readonly="">
                                    </div>
                                    <div class="form-group col-md-3">
                                       <label>VAT %</label>
                                       <select class="form-control vat_perc" required name="vat_perc[]" id="vat_perc">
                                          <option disabled selected>Select VAT %</option>
                                          <option value="5">5</option>
                                          <option value="5.5">5.5</option>
                                          <option value="6">6</option>
                                          <option value="7">7</option>
                                       </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                       <label>VAT SAR Amount</label>
                                       <input type="text" readonly="" class="form-control vat_sar" name="vat_sar[]" id="vat_sar">
                                    </div>
                                    <div class="form-group col-md-3">
                                       <label>Total Amount</label>
                                       <input type="text" class="form-control total_amt" name="total_amt[]"  readonly="" id="total_amt">
                                    </div>
                                    <div id="append_total" class="append_total"></div>
                                    <div class="form-group col-md-3">
                                       <div type="button" id="add_row" class="btn btn-warning"><span class="glyphicon glyphicon-plus"></span></div>
                                       <div type="submit" id="remove_row" class="btn btn-danger"><span class="glyphicon glyphicon-minus"></span></div>
                                    </div>
                                 </div>
                                 <div id="append_data" class="append_data"></div>
                                 <div class="row display_row"  style="display: none;">
                                    <div class="form-group col-md-9"></div>
                                  <div class="form-group col-md-3" >
                                     <label>Grand Total</label>
                                       <input type="number" step="0.0001" class="form-control grand_total" name="grand_total[]" id="grand_total" value="0" readonly="">
                                  </div>
                                  </div>
                              </table>
                           </div>
                        </div>
                        <div class="panel-body">

                           <div class="table-responsive">
                              <table class="table table-bordered table-striped table-hover">
                                 <div class=" col-md-12">
                                    <h2 class="underline">Payment Details</h2>
                                 </div>
                                 <div class="form-group col-md-4">
                                    <label>Payment Method</label>
                                    <select class="form-control" required name="payment_method" id="payment_method">
                                       <option disabled selected>Select Payment Method</option>
                                       <option value="cash">Cash</option>
                                       <option value="credit">Credit</option>
                                       <option value="both">Both</option>
                                    </select>
                                 </div>
                                 <div class="form-group col-md-4">
                                    <label id="lbl_cash">Cash</label>
                                    <input type="number" step="0.0001" class="form-control" name="cash" id="cash" placeholder="Enter cash">
                                 </div>
                                 <div class="form-group col-md-4">
                                    <label id="lbl_credit">Credit</label>
                                    <input type="number" step="0.0001" class="form-control" name="credit" id="credit" placeholder="Enter credit">
                                 </div>
                              </table>
                           </div>
                        </div>

                        <div class="panel-body">

                           <div class="table-responsive">
                              <table class="table table-bordered table-striped table-hover">
                                 <div class=" col-md-12">
                                    <h2></h2>
                                 </div>
                                 <div class="form-group col-md-12">
                                    <label>Upload Invoice Image</label>
                                    <input type="file" name="invoice_img" id="invoice_img">
                                 </div>
                                 <div class="form-group col-md-8">
                                    <label>Product Description</label>
                                    <textarea class="form-control" name="desc" id="desc" rows="4" placeholder="Enter Product Description..."></textarea>
                                 </div>

                                 <div class="reset-button col-md-12">
                                    <button type="button" class="btn btn-warning" onclick="reset_fun()">Reset</button>
                                    <button type="submit" class="btn btn-success">Save</button>
                                 </div>
                              </table>
                           </div>
                        </div>

                        <?php echo form_close();?>
                     </div>

                  </div>
               </div>

               <!-- Form -->

            </div>
         </div>
      </div>
   </section>
   <!-- /.content -->
</div>
   <script type="text/javascript">

      


      function vat_fun(vat_no)
      {
         $.ajax({
            type: 'POST',
            url: '<?php echo base_url('vendor_info_get'); ?>',
            data: {vat_no:vat_no},
            dataType: 'json',
            success: function(response){

              $('#comp_name').val(response.company_name);
              $('#contact').val(response.phone);
              $('#address').val(response.address);

           }
        });
         
      }
      $(function(){
        $( "#vat_num" ).autocomplete({
         source: "<? echo base_url('vendor_info');?>",
         minLength: 2,
         select: function( event, ui ) {
            vat_fun(ui.item.id);
        // console.log( "Selected: " + ui.item.value + " aka " + ui.item.id );

     }
  });
     });

      function reset_fun()
      {
         $('#vat_num').val('');
         $('#comp_name').val('');
         $('#contact').val('');
         $('#address').val('');
         $('#invo_num').val('');
         $('#invo_date').val('');
         $('#sale_ord_date').val('');
         $('#deliv_note_num').val('');
         $('#cust_tck_num').val('');
         $('#product').val('');
         $('#weight').val('');
         $('#quantity').val('');
         $('#rate').val('');
         $('#discount').val('');
         $('#total_exc_vat').val('');
         $('#vat_perc').val('');
         $('#vat_sar').val('');
         $('.total_amt').val('');
         $('#img').val('');
         $('#desc').val('');

      }
      $('.rate').keyup(function(){
         var rate = $(this).val();
         var quantity = $(this).parent().parent('.count_form').find('.quantity').val();
         var discount = $(this).parent().parent('.count_form').find('.discount').val();
         $(this).parent().parent('.count_form').find('.total_exc_vat').val(rate*quantity-discount);
         var vat_perc = $(this).parent().parent('.count_form').find('.vat_perc').val();
         var total_exc_vat = $(this).parent().parent('.count_form').find('.total_exc_vat').val();
         $(this).parent().parent('.count_form').find('.vat_sar').val(total_exc_vat*vat_perc/100);
         var vat_sar = $(this).parent().parent('.count_form').find('.vat_sar').val();
         var total= parseFloat(vat_sar)+parseFloat(total_exc_vat)
         $(this).parent().parent('.count_form').find('.total_amt').val(total.toFixed(2));

         grand_total();

      });
      $('.quantity').keyup(function(){
         var quantity = $(this).val();
         var rate = $(this).parent().parent('.count_form').find('.rate').val();
         // console.log(rate);
         var discount = $(this).parent().parent('.count_form').find('.discount').val();
         console.log(discount);
         $(this).parent().parent('.count_form').find('.total_exc_vat').val(rate*quantity-discount);
      var vat_perc = $(this).parent().parent('.count_form').find('.vat_perc').val();
      var total_exc_vat = $(this).parent().parent('.count_form').find('.total_exc_vat').val();
      $(this).parent().parent('.count_form').find('.vat_sar').val(total_exc_vat*vat_perc/100);
         var vat_sar = $(this).parent().parent('.count_form').find('.vat_sar').val();
         var total= parseFloat(vat_sar)+parseFloat(total_exc_vat)
         $(this).parent().parent('.count_form').find('.total_amt').val(total.toFixed(2));

          grand_total();

      });

      $('.discount').keyup(function(){
         var disc = $(this).val();
         var rate = $(this).parent().parent('.count_form').find('.rate').val();
         var quantity = $(this).parent().parent('.count_form').find('.quantity').val();
         $(this).parent().parent('.count_form').find('.total_exc_vat').val(rate*quantity-disc);
         var vat_perc = $(this).parent().parent('.count_form').find('.vat_perc').val();
         var total_exc_vat = $(this).parent().parent('.count_form').find('.total_exc_vat').val();
         $(this).parent().parent('.count_form').find('.vat_sar').val(total_exc_vat*vat_perc/100);
         var vat_sar = $(this).parent().parent('.count_form').find('.vat_sar').val();
         //$('.total_amt').val(parseInt(vat_sar)+parseInt(total_exc_vat));
         var total= parseFloat(vat_sar)+parseFloat(total_exc_vat)
         $(this).parent().parent('.count_form').find('.total_amt').val(total.toFixed(2));

         grand_total();

      });
      $('.vat_perc').change(function(){
         var vat_perc = $(this).val();
         var total_exc_vat = $(this).parent().parent('.count_form').find('.total_exc_vat').val();
         $(this).parent().parent('.count_form').find('.vat_sar').val(total_exc_vat*vat_perc/100);
         var vat_sar = $(this).parent().parent('.count_form').find('.vat_sar').val();
         if(total_exc_vat!=''){
            var total= parseFloat(total_exc_vat)+parseFloat(vat_sar);
            $(this).parent().parent('.count_form').find('.total_amt').val(total.toFixed(2));
            grand_total();
         }


         
      });
 $('#payment_method').change(function(){

      
var pm_value = $('#payment_method').val();
   if(pm_value == 'cash')
      {
              $("#cash").attr('disabled', false);
             $("#credit").attr('disabled', true);
      }
      else if(pm_value == 'credit')
      {
         $("#credit").attr('disabled', false);
         $("#cash").attr('disabled', true);
      }
      else
      {
      $("#cash").attr('disabled', false);
      $("#credit").attr('disabled', false);
      }
  
});


$(document).ready(function(){
   var count_form =$("body").find('.count_form');
    var count_form_length = count_form.length-1;


         


    $("#add_row").on('click',function(){
      var total = $(this).parent().parent().find('.total_amt').val();
       var sub = parseFloat(total)+parseFloat(total);

      //$('#grand_total').val(sub);
     
      $('.display_row').css('display','block');
     $("#clone_row").first().clone(true).find("input").val("").end().appendTo(".append_data");
     
     // $('.append_total').append("<div class='form-group col-md-6'><label>Grand Total Amount</label>"+
     //  "<input type='text' class='form-control' name='grand_total' id='grand_total'"+
     //  "readonly></div>");
     grand_total();
   
     count_form_length++;
 });

    $("#remove_row").on('click',function(){
       if (count_form_length>0){ 

        var name=$(this).parent().parent('.count_form').remove();
        count_form_length--;
        grand_total();
    }
    if(count_form_length==0){
      $('.display_row').css('display','none');
    }

});
//   $("#add_row").click(function(){
//     $("#clone_data").clone().appendTo("#append_data");
//   });
});


function grand_total(){
   var grand_total = 0;
   $('.total_amt').each(function(index){
      if($(this).val()!=null && $(this).val()!='')
      grand_total += parseFloat($(this).val());
   });

   $('#grand_total').val(grand_total.toFixed(2));
}



$(document).ready(function(){
     $("#credit").attr('disabled', true);
         $("#cash").attr('disabled', true);
    setTimeout(function(){ $("#pur_msg").fadeOut(); }, 4000);
    setTimeout(function(){ $("#pur_msg_error").fadeOut(); }, 4000);
});

///////payment method //////
$('#payment_method').change(function(){
   $("#cash").val('');
   $("#credit").val('');
});

function verify_amt()
{
   var pm_value = $('#payment_method').val();
   var count_form =$("body").find('.count_form');
   var row_length = count_form.length;

   if(row_length > 1)
   {
// grand total 
      var grand_total = parseFloat($("#grand_total").val());

      if(pm_value == 'cash')
      {
         var cash = parseFloat($("#cash").val());
         if(cash != grand_total)
         {
            alert("Please enter the cash valid amount ...!");
            return false;
         }
      }
      else if(pm_value == 'credit')
      {
         var credit = parseFloat($("#credit").val());
         if(credit != grand_total)
         {
            alert("Please enter the credit valid amount ...!");
            return false;
         }
      }
      else
      {
         var cash = parseFloat($("#cash").val());
         var credit = parseFloat($("#credit").val());
         var total_amt_entered = cash+credit;
          if(total_amt_entered != grand_total)
         {
            alert("Please enter the valid amount in cash and credit ...!");
            return false;
         }
      }

   }
   else
   {
/// total amount 
      var total_amount = parseFloat($("#total_amt").val());
      if(pm_value == 'cash')
      {
         var cash = parseFloat($("#cash").val());
         if(cash != total_amount)
         {
            alert("Please enter the cash valid amount ...!");
            return false;
         }
      }
      else if(pm_value == 'credit')
      {
         var credit = parseFloat($("#credit").val());
         if(credit != total_amount)
         {
            alert("Please enter the credit valid amount ...!");
            return false;
         }
      }
      else
      {
         var cash = parseFloat($("#cash").val());
         var credit = parseFloat($("#credit").val());
         var total_amt_entered = cash+credit;

          if(total_amt_entered != total_amount)
         {
            alert("Please enter the valid amount in cash and credit ...!");
            return false;
         }
      }
   }


      return true;
}

$('.pur_date').datetimepicker({
 timepicker:false,
 format:'d-m-Y'
});
</script>    
