 <style>
body {font-family: Arial, Helvetica, sans-serif;}

#myImg {
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.img_modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 0px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.show-model-content {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
}

/* Caption of Modal Image */
#caption {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
  text-align: center;
  color: #ccc;
  padding: 10px 0;
  height: 150px;
}

/* Add Animation */
.show-model-content, #caption {  
  -webkit-animation-name: zoom;
  -webkit-animation-duration: 0.6s;
  animation-name: zoom;
  animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
  from {-webkit-transform:scale(0)} 
  to {-webkit-transform:scale(1)}
}

@keyframes zoom {
  from {transform:scale(0)} 
  to {transform:scale(1)}
}

/* The Close Button */
.close {
  position: absolute;
  top: 15px;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.close:hover,
.close:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
  .show-model-content {
    width: 100%;
  }
}
</style>


 <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-file-text-o"></i>
      </div>
      <div class="header-title">
         <h1>Purchase Register</h1>
         <small>Purchase List</small>
         <?php if(!empty($this->session->flashdata('update_msg_success'))): ?>
          <span id="updatemsg" style="color: green; text-align: right; float: right;"><? echo $this->session->flashdata('update_msg_success');?></span>
       <?php endif;?>
        <?php if(!empty($this->session->flashdata('update_msg_error'))): ?>
          <span id="updatemsg" style="color: red; text-align: right; float: right;"><? echo $this->session->flashdata('update_msg_error');?></span>
       <?php endif;?>
    </div>
 </section>
 <!-- Main content -->
 <section class="content">
   <div class="row">
      <div class="col-sm-12">
         <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
               <div class="btn-group" id="buttonexport">
                   <a class="btn btn-add " href="<? echo base_url('new_purchase');?>"> 
                      <i class="fa fa-list"></i>  New Purchase Invoice </a>
               </div>
            </div>
            <div class="panel-body">
               <?php echo form_open("purchase_print", array('name' => 'print_purchase', 'id' =>
               'print_purchase'));?>
                <div class="form-group col-md-3">
                  <input type="text" id="vat_num" name="vat_num" class="form-control" placeholder="Enter VAT Number" required oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" >
                </div>
               <div class="form-group col-md-3">
                  <input  type="text" name="start_date" id="invo_date" required class="form-control start_date pur_search_date" placeholder="From...">
               </div>
               <div class="form-group col-md-3">
                  <input  type="text" name="due_date" id="invo_date" required class="form-control due_date pur_search_date" placeholder="To...">
               </div>
               <div class="btn-group">
                  <a href="#" id='btn' value='Excel'  onclick="betweenPurchase(this)"> 
                           <img src="assets/dist/img/xls.png" width="24" alt="logo"> Print</a>
                  <!-- <button class="btn btn-exp btn-sm dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Purchase Register Action</button>
                  <ul class="dropdown-menu exp-drop" role="menu">

                     <li>
                        <a href="#" id='btn' value='Excel'  onclick="betweenPurchase(this)"> 
                           <img src="assets/dist/img/xls.png" width="24" alt="logo"> Print Excel</a>
                        </li> -->
                               <!--   <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'pdf',pdfFontSize:'7',escape:'false'});"> 
                                    <img src="assets/dist/img/pdf.png" width="24" alt="logo"> PDF</a>
                                 </li> -->
                                <!--  <li>
                                    <a href="#"> 
                                       <img src="assets/dist/img/txt.png" width="24" alt="logo"> Show</a>
                                    </li>
                                 </ul> -->
                              </div>
                              <? echo form_close();?>
                           </div>
                           <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->

                           <!-- ./Plugin content:powerpoint,txt,pdf,png,word,xl -->
                           <div class="panel-body">
                              <div class="table-responsive" id="">
                                 <table id="example" class="table table-striped table-bordered">
                                    <thead>
                                       <tr class="info">
                                          <th>Dated</th>
                                          <th>Invoice No.</th>
                                          <th>Company Name</th>
                                          <th>VAT #</th>
                                          <th>Quantity</th>
                                          <th>Amount</th>
                                          <th>Vat Amount</th>
                                          <th>Total</th>
                                          <th>Status</th>
                                          <th>Image</th>
                                          <th>Action</th>
                                       </tr>
                                    </thead>
                                    <tbody id="datatablebody">
                                       <?php if(isset($invoices) && !empty($invoices)):?>

                                       <?php foreach($invoices as $invoice){ ?>
                                         <tr>
                                          <td><?php echo formated_date($invoice->invoice_date,'d-m-Y'); ?></td>
                                          <td><?php echo $invoice->invoice_no;?></td>
                                          <td style="text-align: center;"><?php echo $invoice->company_name;?></td>
                                          <td><?php echo $invoice->vat_no;?></td>
                                          <td><?php echo $invoice->qty;?></td>
                                          <td><?php echo round($invoice->total_exec_vat,2);?></td>
                                          <td><?php echo round($invoice->vat_sar,2);?></td>
                                          <td><?php echo round($invoice->total_amount, 2);?></td>

                                          <?php if($invoice->invo_status == 0): ?>
                                          <td><span class="label-danger label label-default" >Pending</span>
                                            
                                          </td>
                                          <?php else: ?>
                                          <td><span class="label-custom label label-default" >Completed</span>
                                          </td>
                                        <?php endif; ?>
                                          <td>
                                          <a href="javascript:;" class="pop">
                                          <img src="<?php echo base_url();?>assets/images/purchase/<?php echo $invoice->img;?>" alt="payable image.." class="image_show" style="height: 60px;width: 60px">
                                          </a>
                                        </td>
                                          <td> 

                                             <a href="<?php echo base_url("edit_purchase?edit_id=$invoice->invo_id");?>" class="btn btn-add btn-sm"><i class="fa fa-pencil"></i></a>
                                             <a href="<?php echo base_url("invoice/Invoice/index/$invoice->invo_id");?>" class="btn btn-success btn-sm"><i class="fa fa-eye"></i></a>
                                             <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#customer2" 
                                             onclick="del_pro(<?php echo $invoice->invo_id;?>)"><i class="fa fa-trash-o"></i> </button>
                                          </td>
                                       <?php } ?>
                                    <?php endif; ?>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

               <!-- /// image modal show -->
                <div class="img_modal fade" id="imagemodal" style="z-index: 9999;" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="show-model-content">
                 
                  <div class="modal-body" style="padding: 5px !important;">
                    <img src="" id="imagepreview" style="width: 468px; height: 470px;" >
                    <div style="text-align: center; padding-top: 10px;">
                    <a  href="" download="purchase List" id="btn_download" class="btn btn-success btn-lg">Download</a>
                  </div>
                  </div>
                 
                </div>
              </div>
            </div>
            <!-- /// end image modal show -->
               <!-- quote Modal1 -->
               <div class="modal fade" id="customer1" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog">
                     <div class="modal-content">
                        <div class="modal-header modal-header-primary">
                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                           <h3><i class="fa fa-user m-r-5"></i> Update Quotes</h3>
                        </div>
                        <div class="modal-body">
                           <div class="row">
                              <div class="col-md-12">
                                 <? echo form_open("purchase_list_exe", array('name' => 'update_pro', 'id' =>
                                 'update_pro'));?>
                                 <fieldset>
                                    <!-- Text input-->
                                    <div class="col-md-6 form-group">
                                       <label class="control-label">Product name</label>
                                       <input type="text" id="pro_name" name="pro_name" placeholder="Product Name" class="form-control" required>
                                    </div>
                                    <!-- Text input-->
                                    <div class="col-md-6 form-group">
                                       <label>Status</label>
                                       <select class="form-control" id="state" name="state">
                                          <option value="1">Active</option>
                                          <option value="0">Deactive</option>
                                       </select>
                                    </div>
                                    <!-- Text input-->
                                    <div class="col-md-12 form-group">
                                      <label>Description</label><br>
                                      <textarea name="description" id="description" rows="3" class="form-control" required></textarea>
                                   </div>
                                   <input type="hidden" name="update_id" id="update_id">
                                   <div class="col-md-12 form-group user-form-group">
                                    <div class="pull-right">
                                       <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                                       <button type="submit" class="btn btn-add btn-sm">Save</button>
                                    </div>
                                 </div>
                              </fieldset>
                              <? echo form_close(); ?>
                           </div>
                        </div>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- Modal -->   
            <!-- quote delete Modal2 -->
            <div class="modal fade" id="customer2" tabindex="-1" role="dialog" aria-hidden="true">
               <div class="modal-dialog">
                  <div class="modal-content">
                     <div class="modal-header modal-header-primary">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3><i class="fa fa-user m-r-5"></i> Delete Product</h3>
                     </div>
                     <div class="modal-body">
                        <div class="row">
                           <div class="col-md-12">
                              <? echo form_open("delete_perchase", array('name' => 'del_form',
                              'id' => 'del_form', 'class' => 'form-horizontal'));?>
                              <!-- <form class="form-horizontal"> -->
                                 <fieldset>
                                    <div class="col-md-12 form-group user-form-group">
                                       <label class="control-label">Delete Product</label>
                                       <input type="hidden" name="del" id="del">
                                       <div class="pull-right">
                                          <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">NO</button>
                                          <button type="submit" class="btn btn-add btn-sm">YES</button>
                                       </div>
                                    </div>
                                 </fieldset>
                                 <? echo form_close();?>
                              </div>
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                        </div>
                     </div>
                     <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
               </div>
               <!-- /.modal -->
            </section>
            <!-- /.content -->
         </div>


         <!-- /////////////////////////////////// excel table start here //////////////////////////// -->
<div style="display: none;">
<div class="row" id="ExcelTable">
   <div class="table-responsive" >
<div class="col-md-12">
            <table  class="table table-bordered table-striped table-hover">
              <td colspan="8" style="text-align:center;"><h2>Purchase Register</h2></td>
               <thead>
                
                  <tr class="info">
                     <th>Dated</th>
                     <th>Invoice No.</th>
                     <th>Company Name</th>
                     <th>VAT #</th>
                     <th>Quantity</th>
                     <th>Amount</th>
                     <th>Vat Amount</th>
                     <th>Total</th>
                  </tr>
               </thead>
               <tbody id="invoiceExcel">

               </tbody>
            </table>
         </div>
         </div>
      </div>
      </div>

         <!-- //////////////////////////////////// end table here //////////////// -->

         <script type="text/javascript">
            var tableToExcel = (function() {
             var uri = 'data:application/vnd.ms-excel;base64,'
             , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
             , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
             , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
             return function(table, name) {
              if (!table.nodeType) table = document.getElementById(table)
                 var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
              window.location.href = uri + base64(format(template, ctx))
           }
        })()
     </script>
     <script type="text/javascript">
       function edit_pro(pro_id)
       {

         $.ajax({
            type: 'POST',
            url: '<?php //echo base_url('get_single_product_ajax'); ?>',
            data: {pro_id:pro_id},
            dataType: 'json',
            success: function(response){
               if(response.flag){

                  $('#update_id').val(response.data.pro_id);
                  $('#');
                  $('#pro_name').val(response.data.name);
                     // console.log(response.data.status);
                     $('#state').val(response.data.status);
                     $('#description').val(response.data.description);

                  }
               }
            });
      }

      function betweenPurchase(pro_id){
            // console.log('dd');
            var due_date=$('.due_date').val();
            var start_date=$('.start_date').val();
            var vat_no=$('#vat_num').val();
            $.ajax({
               type: 'POST',
               url: '<?php echo base_url('betweenPurchase'); ?>',
               data: {start_date:start_date,due_date:due_date,vat_no:vat_no},
               dataType: 'json',
               success: function(response){
                  if(response.flag){
                    var grand=0;
                     var vat_amt = 0;
                    var total_amt = 0;
                     var tableContent=``;
                     response.data.forEach(function(val,ind){
                      grand=((parseFloat(grand))+(parseFloat(val.total_amount)));
                      total_amt = ((parseFloat(total_amt))+(parseFloat(val.total_exec_vat)));
                      vat_amt = ((parseFloat(vat_amt))+(parseFloat(val.vat_sar)));
                      total_exec_vat= parseFloat(val.total_exec_vat);
                      totl_exe_vat = total_exec_vat.toFixed(2);
                      vat_sar= parseFloat(val.vat_sar);
                      vat_sr = vat_sar.toFixed(2);
                      total_amount= parseFloat(val.total_amount);
                      totl_amt = total_amount.toFixed(2);
                       var current_datetime = new Date(val.invoice_date)
                        var formatted_date = current_datetime.getDate() + "-" +
                        (current_datetime.getMonth() + 1) + "-" + current_datetime.getFullYear()
                      tableContent+=` <tr>
                      <td>${formatted_date}</td>
                      <td>${val.invoice_no}</td>
                      <td>${val.company_name}</td>
                      <td>${val.vat_no}</td>
                      <td>${val.qty}</td>
                      <td>${totl_exe_vat}</td>
                      <td>${vat_sr}</td>
                      <td>${totl_amt}</td>
                      </tr>`;
                   });
                    // var gtotal = parseFloat(grand);
                    //  var vat_amt_total = parseFloat(vat_amt);
                     tableContent+=`<tr><td></td><td colspan="4" style="text-align:center;"><strong>Total:</strong></td><td>${total_amt.toFixed(2)}</td><td>${vat_amt.toFixed(2)}</td>
                     <td><strong>${grand.toFixed(2)}</strong></td></tr>`;
                     $('#invoiceExcel').html(tableContent);
                     $('#datatablebody').html('');
                     $('#datatablebody').html(tableContent);
                     tableToExcel('ExcelTable', 'Bank outstanding Excel');
                  }
               }
            });
         }

         function del_pro(del_id)
         {
            $('#del').val(del_id);

         }

         /// vat no. ajax calls
         function vat_fun(vat_no)
      {
         $.ajax({
            type: 'POST',
            url: '<?php echo base_url('vendor_info_get'); ?>',
            data: {vat_no:vat_no},
            dataType: 'json',
            success: function(response){

              $('#comp_name').val(response.company_name);
              $('#contact').val(response.phone);
              $('#address').val(response.address);

           }
        });
         
      }
      $(function(){
        $("#vat_num" ).autocomplete({
         source: "<? echo base_url('vendor_info');?>",
         minLength: 2,
         select: function( event, ui ) {
            vat_fun(ui.item.id);
        // console.log( "Selected: " + ui.item.value + " aka " + ui.item.id );

     }
  });
     });

      /// image modal show 
      $(".pop").on("click", function() {
   $('#imagepreview').attr('src', $(this).find('img').attr('src'));
   $('#btn_download').attr("href", $(this).find('img').attr('src')) // here asign the image to the modal when the user click the enlarge link
   $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
});
      $(document).ready(function(){
    setTimeout(function(){ $("#update_msg_success").fadeOut(); }, 4000);
    setTimeout(function(){ $("#update_msg_error").fadeOut(); }, 4000);
});

 $('.pur_search_date').datetimepicker({
 timepicker:false,
 format:'d-m-Y'
});
    
      </script>