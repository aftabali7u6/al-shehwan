 <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-file-text-o"></i>
      </div>
      <div class="header-title">
         <h1>Purchase Payables</h1>
         <small>Purchase Due Amounts</small>
         <?php if(!empty($this->session->flashdata('update_msg'))): ?>
          <span id="updatemsg" style="color: green; text-align: right; float: right;"><? echo $this->session->flashdata('update_msg');?></span>
       <?php endif;?>
    </div>
 </section>
 <!-- Main content -->
 <section class="content">
   <div class="row">
      <div class="col-sm-12">
         <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
               <div class="btn-group" id="buttonlist"> 
                    <a class="btn btn-add " href="<? echo base_url('purchase_list');?>"> 
                        <i class="fa fa-list"></i>  New Purchase List </a> 
                      </div>   
            </div>
            <div class="panel-body">
               <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->

               <!-- ./Plugin content:powerpoint,txt,pdf,png,word,xl -->
               <div class="table-responsive">
                  <table id="example" class="table table-striped table-bordered" style="width:100%">
                     <!-- <table id="dataTableExample1" class="table table-bordered table-striped table-hover"> -->
                        <thead>
                           <tr class="info">
                              <th>Invoice Date</th>
                              <th>Invoice No.</th>
                              <th>Company Name</th>
                              <th>VAT #</th>
                              <th>Cash</th>
                              <th>Amount Payable</th>
                              <th>Total</th>
                              <!-- <th>Status</th> -->
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                          <?php if(isset($payables)):
                              foreach ($payables as $index => $payable):
                             // echo $payables[$i]->credit;exit;
                             if(!empty($payable->credit)): ?>
                             <? $total_amount =$payable->cash+$payable->credit; ?>
                             <tr class="payble<?php echo $payable->invo_id;?> ">
                             <td><? echo formated_date($payable->invoice_date,'d-m-Y');?></td>
                             <td><? echo $payable->invoice_no;?></td>
                              <td style="text-align: center;"><? echo $payable->company_name;?></td>
                              <td><? echo $payable->vat_no;?></td>
                              <td class="invo_cash"><? echo round($payable->cash,2);?></td>
                              <td class="invo_credit"><? echo round($payable->credit,2);?></td>
                              <td><? echo round($total_amount,2);?></td>
                              <td>
                             <button type="button" onclick="pay_amount(<? echo $payable->invo_id;?>)" id="btn_action" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#customer1">Unpaid</button>
                           </td>
                            
                              </tr>
                             <? endif;?>
                          <? endforeach; ?>
                            <? endif;?>

                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- quote Modal1 -->
         <div class="modal fade" id="customer1" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header modal-header-primary">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                     <h3><i class="fa fa-user m-r-5"></i> Update Payable Amount </h3>
                  </div>
                  <div class="modal-body">
                     <div class="row">
                        <div class="col-md-12">
                           <?php echo form_open("update_payable", array('name' => 'update_payable', 'id' =>'update_payable'));?>
                           <fieldset>
                              <!-- Text input-->

                              <div class="col-md-4 form-group">
                                 <label class="control-label" class="form-control">Amount Payable:</label>
                                 <!-- <input type="text" id="name" name="name"  placeholder="Name" class="form-control"> -->
                              </div>
                              <div class="col-md-8 form-group">
                                 
                                 <input type="text" id="payable_amt" name="payable_amt" placeholder="Payable Amount..." readonly class="form-control">
                              </div>
                              <div class="col-md-4 form-group">
                                 <label class="control-label" class="form-control">Enter Amount To Pay:</label>
                                 <!-- <input type="text" id="contact" name="contact" placeholder="contact" class="form-control"> -->
                              </div>
                              <div class="col-md-8 form-group">
                                 <!-- <label class="control-label">Address</label> -->
                                 <input type="number" step="0.0001" id="paid_amt" name="Payable Amount..." placeholder="address" class="form-control">
                              </div>
                              <input type="hidden" name="invoice_cash" id="invoice_cash">
                              <input type="hidden" name="invoice_id" id="invoice_id">
                              <div class="col-md-12 form-group user-form-group">
                                 <div class="pull-right">
                                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                                    <a href="javascript:;" class="btn btn-add btn-sm" 
                                    onclick="update_payableCall()">Save</a>
                                 </div>
                              </div>
                           </fieldset>
                           <? echo form_close(); ?>
                        </div>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" >Close</button>
                  </div>
               </div>
               <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
         </div>
      </section>
      <!-- /.content -->
   </div>

  <script type="text/javascript">

 function pay_amount(invo_id)
 {
  var invo_cash=$('.payble'+invo_id).find('.invo_cash').first().html();
  var credit=$('.payble'+invo_id).find('.invo_credit').first().html();
  $("#payable_amt").val(credit);
  $("#paid_amt").val(credit);
  $("#invoice_cash").val(invo_cash);
  $("#invoice_id").val(invo_id);
 }
 function update_payableCall()
 {
    var payable_amount = parseFloat($("#payable_amt").val());
    var paid_amount = parseFloat($("#paid_amt").val());
    if(paid_amount > payable_amount)
    {
      alert("You have entered invalid amount!");
    }else{
      var amt_cash = parseFloat($("#invoice_cash").val());
      var invo_id = parseFloat($("#invoice_id").val());
      $.ajax({
        type: 'POST',
        url: '<?php echo base_url('update_payable_ajax'); ?>',
        dataType: 'json',
        data:{payable_amount:payable_amount,paid_amount:paid_amount,amt_cash:amt_cash,invo_id:invo_id},
            success: function(response){
              var cash_amt = parseFloat(response.cash);
              var credit_amt = parseFloat(response.credit);
               $('.payble'+invo_id).find('.invo_cash').first().html(cash_amt.toFixed(2));
               $('.payble'+invo_id).find('.invo_credit').first().html(credit_amt.toFixed(2));
               $('#customer1').modal('hide');
                if(response.credit == 0)
               {
                $('.payble'+invo_id).find('#btn_action').first().html("Paid");
                $('.payble'+invo_id).find('#btn_action').removeClass('btn-danger').addClass('btn-success');
               }
            }
    });
    }
    
 }
</script>