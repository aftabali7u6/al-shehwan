<?php 

class Layout{

	public function render()
	{ 
		global $OUT;
		$CI = &get_instance();
		$output = $CI->output->get_output();
		if(!isset($CI->layout))
			$CI->layout = 'default';
		if($CI->layout != false)
		{
			if(!preg_match('/(.+).php$/', $CI->layout))
			{
				$CI->layout .= '.php';
			}

			$requested = APPPATH .'views/layouts/'.$CI->layout;
			$default = APPPATH .'views/layouts/default.php';

			if(file_exists($requested))
			{
				$layout = $CI->load->file($requested, true);
			}
			else
			{
				$layout = $CI->load->file($default, true);
			}

			$view = str_replace("{content}", $output, $layout);
			$view = str_replace("{title}", isset($CI->title)? $CI->title:'Application Name', $view);

			$scripts = "";
			$styles = "";
			$metas = "";


			if (isset($CI->meta) && count($CI->meta) > 0) { 
                // array('sport','criket','bedmend');   content="sport,criket,bedment"  
                $metas = implode(", ", $CI->meta);
            }

             if (isset($CI->scripts) && count($CI->scripts) > 0) {  // Массив со скриптами
                foreach ($CI->scripts as $script) {
                    $scripts .= "<script type='text/javascript' src='" . base_url() . "assets/js/" . $script . ".js'></script>";
                }
            }


             if (isset($CI->styles) && count($CI->styles) > 0) {   // Массив со стилями
                foreach ($CI->styles as $style) {
                    $styles .= "<link rel='stylesheet' type='text/css' href='" . base_url() . "assets/css/" . $style . ".css' />";
                }
            }


             if (isset($CI->parts) && count($CI->parts) > 0) {    // Массив с частями страницы
                foreach ($CI->parts as $name => $part) {
                    $view = str_replace("{" . $name . "}", $part, $view);
                }
            }

            $view = str_replace("{metas}", $metas, $view);
            $view = str_replace("{script}", $scripts, $view);
            $view = str_replace("{styles}", $styles, $view);



		}
		else
		{
			$view = $output;
		}

		$OUT->_display($view);

	}
}