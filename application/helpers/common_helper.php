<?php 

function check_user_session()
{
	$CI = &get_instance();
	if(!$CI->session->userdata('name'))
	{
		redirect('login/page');
	}
}

if (!function_exists('product_data'))
{
   function product_data($id)
   {
	$CI = &get_instance();
	$CI->load->model('Invoice_model');
    $array_data = $CI->Invoice_model->product_data('product',$id);
    return $array_data;
    
   }

}
function formated_date($mysql_date_time = '', $format = 'Y-m-d')
{
	
	if(!empty($mysql_date_time))
		return date($format, strtotime($mysql_date_time));
	else
		return date('Y-m-d H:i:s');
}

  
?>